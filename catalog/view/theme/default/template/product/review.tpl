<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<table class="table  table-bordered table-review">
  <tr>
    <td style="width: 50%;"><strong><?php echo $review['author']; ?></strong></td>

  </tr>
  <tr>
    <td colspan="2"><p class="review-text"><?php echo $review['text']; ?></p>
      <div class="text-left review-date"><?php echo $review['date_added']; ?></div>
   </td>
  </tr>
</table>
<?php } ?>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
