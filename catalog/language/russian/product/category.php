<?php
// Text
$_['text_refine']       = 'Уточнить поиск';
$_['text_product']      = 'Товары';
$_['text_error']        = 'Категория не найдена!';
$_['text_empty']        = 'В этой категории нет товаров.';
$_['text_quantity']     = 'Кол-во:';
$_['text_manufacturer'] = 'Производитель:';
$_['text_model']        = 'Код товара:';
$_['text_points']       = 'Бонусные Баллы:';
$_['text_price']        = 'Цена:';
$_['text_tax']          = 'Без налога:';
$_['text_compare']      = 'Сравнение товаров (%s)';
$_['text_sort']         = 'Сортировка ';
$_['text_default']      = 'По умолчанию';
$_['text_name_asc']     = ' по имени - от A до Я';
$_['text_name_desc']    = ' по имени  - от Я до A';
$_['text_price_asc']    = ' по цене - от меньшей';
$_['text_price_desc']   = ' по цене - от большей';
$_['text_rating_asc']   = ' по рейтингу  - от меньшего';
$_['text_rating_desc']  = ' по рейтингу  - от большего';
$_['text_model_asc']    = ' по Модели (A - Я)';
$_['text_model_desc']   = ' по Модели (Я - A)';
$_['text_limit']        = 'Показывать:';
