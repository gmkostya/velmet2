<div class="modal fade modal-quick-login-register" id="modal-quick-login-register" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content" id="quick-login-register">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span aria-hidden="true"
                                                                                                  class="icon-close"> </span>
                </button>
                <h3 class="modal-title"><?php echo $text_returning; ?></h3>
            </div>
            <div class="modal-body">

                <div class="form-login" id="quick-login">
                    <h3 class="box-heading-3"><?php echo $text_always_registered; ?></h3>
                    <p><?php echo $text_info_login; ?></p>

                    <div class="form-group required">
                        <input type="text" name="email" value="" id="input-emails" class="form-control"
                               placeholder="<?php echo $entry_email; ?>"/>
                    </div>
                    <div class="form-group required">
                        <input type="password" name="password" value="" id="input-passwords" class="form-control"
                               placeholder="<?php echo $entry_password; ?>"/>
                    </div>
                    <div class="form-group">
                        <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                    </div>

                    <button type="button" class="btn btn-primary loginaccount"
                            data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_login ?></button>

                </div>
                <div class="form-register" id="quick-register">
                    <h3 class="box-heading-3"><?php echo $text_new_customer; ?></h3>
                    <p><?php echo $text_customer_info; ?></p>
                    <div class="row">
                        <div class="col-sm-12 col-md-6  col-modal-left">
                            <div class="form-group required">
                                <input type="text" name="name" value="" id="input-name" class="form-control"
                                       placeholder="<?php echo $entry_name; ?>"/>
                            </div>
                            <div class="form-group required">

                                <input type="text" name="email" value="" id="input-email" class="form-control"
                                       placeholder="<?php echo $entry_email; ?>"/>
                            </div>
                            <div class="form-group required">
                                <input type="password" name="password" value="" id="input-password" class="form-control"
                                       placeholder="<?php echo $entry_password; ?>"/>
                            </div>
<span style="display: none">
                            <input class="styled" type="checkbox" name="fax" value="1" id="quick-fax"/>&nbsp;<label
            for="quick-fax"><?php echo $text_opt; ?></label><br><br></span>


                        </div>
                        <div class="col-sm-12 col-md-6 col-modal-right">
                            <div class="form-group required">
                                <input type="text" name="lastname" value="" id="input-lastname" class="form-control"
                                       placeholder="<?php echo $entry_lastname; ?>"/>
                            </div>
                            <div class="form-group required">
                                <input type="text" name="telephone" value="" id="input-telephone"
                                       class="form-control js-phone-mask"
                                       placeholder="<?php echo $entry_telephone; ?>"/>
                            </div>

                            <div class="form-group required">
                                <input type="password" name="password2" value="" id="input-password2"
                                       class="form-control" placeholder="<?php echo $entry_password2; ?>"/>
                            </div>


                        </div>
                        <div class="col-md-12">
                            <p class="text-attention"><?php echo $text_attention; ?></p>
                            <?php if ($text_agree and 0) { ?>
                                <input type="checkbox" name="agree" value="1" id="quick-agree"/>&nbsp;<label
                                    for="quick-agree"><?php echo $text_agree; ?></label><br><br>
                                <button type="button" class="btn btn-primary createaccount"
                                        data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_register; ?></button>
                            <?php } else { ?>
                                <button type="button" class="btn btn-primary createaccount"
                                        data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_register; ?></button>
                            <?php } ?>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <div class="curier-social-login">
                    <div class="text_social_login"><?php echo $text_social_login;?></div><div class="social_login"><?php echo $ulogin_form_marker;?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    $(document).delegate('.quick_login', 'click', function (e) {
        $('#modal-quick-login-register').modal('show');
    });
    $(document).delegate('.quick_register', 'click', function (e) {
        $('#modal-quick-login-register').modal('show');
    });
    $(document).delegate('.ulogin-button-facebook, .ulogin-button-googleplus', 'click', function (e) {
        setTimeout(function () {
            $('#modal-quick-login-register').modal('hide')
        },400)
    });

   //--></script>
<script type="text/javascript"><!--
    $('#quick-register input').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $('#quick-register .createaccount').trigger('click');
        }
    });
    $('#quick-register .createaccount').click(function () {
        $.ajax({
            url: 'index.php?route=common/quicksignup/register',
            type: 'post',
            data: $('#quick-register input[type=\'text\'], #quick-register input[type=\'password\'], #quick-register input[type=\'checkbox\']:checked'),
            dataType: 'json',
            beforeSend: function () {
                masked('#modal-quick-login-register .modal-content', true);
                $('#quick-register .createaccount').button('loading');
                $('#quick-register .alert-danger').remove();
                $('#quick-register .error').remove();
            },
            complete: function () {
                $('#quick-register .createaccount').button('reset');
            },
            success: function (json) {
                masked('#modal-quick-login-register .modal-content', false);
                $('#quick-register .form-group').removeClass('has-error');

                if (json['islogged']) {
                    window.location.href = "index.php?route=account/account";
                }
                if (json['error_name']) {
                    $('#quick-register #input-name').parent().addClass('has-error');
                    $('#quick-register #input-name').focus();
                    $('#quick-register #input-name').after('<span class="error">' + json['error_name'] + '</span>');
                }
                if (json['error_lastname']) {
                    $('#quick-register #input-lastname').parent().addClass('has-error');
                    $('#quick-register #input-lastname').focus();
                    $('#quick-register #input-lastname').after('<span class="error">' + json['error_lastname'] + '</span>');
                }
                if (json['error_email']) {
                    $('#quick-register #input-email').parent().addClass('has-error');
                    $('#quick-register #input-email').focus();
                    $('#quick-register #input-email').after('<span class="error">' + json['error_email'] + '</span>');

                }
                if (json['error_telephone']) {
                    $('#quick-register #input-telephone').parent().addClass('has-error');
                    $('#quick-register #input-telephone').focus();
                    $('#quick-register #input-telephone').after('<span class="error">' + json['error_telephone'] + '</span>');

                }
                if (json['error_password']) {
                    $('#quick-register #input-password').parent().addClass('has-error');
                    $('#quick-register #input-password').focus();
                    $('#quick-register #input-password').after('<span class="error">' + json['error_password'] + '</span>');

                }
                if (json['error_password2']) {
                    $('#quick-register #input-password2').parent().addClass('has-error');
                    $('#quick-register #input-password2').focus();
                    $('#quick-register #input-password2').after('<span class="error">' + json['error_password2'] + '</span>');

                }
                if (json['error']) {
                    //$('#quick-register h3').after('<div class="alert alert-danger" style="margin:5px 0;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['now_login']) {

                }
                if (json['success']) {
                    success = '<h3 class="box-heading-3">' + json['heading_title'] + '</h3>';
                    success += json['text_message'];
                    //success += '<div class="buttons"><div class="text-right"><a onclick="loacation();" class="btn btn-primary">'+ json['button_continue'] +'</a></div></div>';
                    $('#quick-register ').html(success);
                    setTimeout(function () {
                        window.location.href = "index.php?route=account/account";
                    }, 3000)
                }
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('#quick-login input').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $('#quick-login .loginaccount').trigger('click');
        }
    });
    $('#quick-login .loginaccount').click(function () {
        $.ajax({
            url: 'index.php?route=common/quicksignup/login',
            type: 'post',
            data: $('#quick-login input[type=\'text\'], #quick-login input[type=\'password\']'),
            dataType: 'json',
            beforeSend: function () {
                masked('#modal-quick-login-register .modal-content', true);
                $('#quick-login .loginaccount').button('loading');
                $('#modal-login .alert-danger').remove();
            },
            complete: function () {
                $('#quick-login .loginaccount').button('reset');
            },
            success: function (json) {
                masked('#modal-quick-login-register .modal-content', false);
                $('#modal-login .form-group').removeClass('has-error');
                if (json['islogged']) {
                    window.location.href = "index.php?route=account/account";
                }

                if (json['error']) {
                    $('#modal-login .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    $('#quick-login #input-emails').parent().addClass('has-error');
                    $('#quick-login #input-passwords').parent().addClass('has-error');
                    $('#quick-login #input-emails').focus();
                }
                if (json['success']) {
                    loacation();
                    $('#modal-login').modal('hide');
                }

            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    function loacation() {
        location.reload();
    }
    //--></script>
