<?php if ($article) { ?>
	<div class="bnews-list<?php if ($display_style) { ?> bnews-list-2<?php } ?> row">
		<?php foreach ($article as $articles) { ?>
		<div class="col-md-4 col-sm-2 col-xs-12 col-news-layout">
			<div class="artblock<?php if ($display_style) { ?> artblock-2<?php } ?>">
				<?php if ($articles['thumb']) { ?>
					<div class="image"><a href="<?php echo $articles['href']; ?>" class="image-link"><img alt="<?php echo $articles['name']; ?>" src="<?php echo $articles['thumb']; ?>"></a></div>
				<?php } ?>
					<?php if ($articles['date_added']) { ?>
						<div class="date"> <?php echo $articles['date_added']; ?></div>
					<?php } ?>
				<?php if ($articles['name']) { ?>
					<div class="name"><a href="<?php echo $articles['href']; ?>" class=""><?php echo $articles['name']; ?></a></div>
				<?php } ?>
				<div class="article-meta" style="display: none">
					<?php if ($articles['author']) { ?>
						<?php echo $text_posted_by; ?> <a href="<?php echo $articles['author_link']; ?>"><?php echo $articles['author']; ?></a> |
					<?php } ?>
			
					<?php if ($articles['du']) { ?>
						<?php echo $text_updated_on; ?> <?php echo $articles['du']; ?> |
					<?php } ?>
					<?php if ($articles['category']) { ?>
						<?php echo $text_posted_in; ?> <?php echo $articles['category']; ?> |
					<?php } ?>
				</div>
				<?php if ($articles['custom1']) { ?>
					<div class="custom1"><?php echo $articles['custom1']; ?></div>
				<?php } ?>
				<?php if ($articles['custom2']) { ?>
					<div class="custom2"><?php echo $articles['custom2']; ?></div>
				<?php } ?>
				<?php if ($articles['custom3']) { ?>
					<div class="custom3"><?php echo $articles['custom3']; ?></div>
				<?php } ?>
				<?php if ($articles['custom4']) { ?>
					<div class="custom4"><?php echo $articles['custom4']; ?></div>
				<?php } ?>
				<?php if ($articles['description']) { ?>
					<div class="description"><?php echo $articles['description']; ?></div>
				<?php } ?>
				<?php if ($articles['total_comments']) { ?>
				  <?php if (!$disqus_status && !$fbcom_status) { ?>
					<div class="total-comments"><?php echo $articles['total_comments']; ?> <?php echo $text_comments; ?> - <a href="<?php echo $articles['href']; ?>#comments"><?php echo $text_comments_v; ?></a></div>
				  <?php } elseif ($fbcom_status) { ?>
					<div class="total-comments"><fb:comments-count href="<?php echo $articles['canhref']; ?>"></fb:comments-count> <?php echo $text_comments; ?> - <a href="<?php echo $articles['href']; ?>#comments"><?php echo $text_comments_v; ?></a></div>
				  <?php } else { ?>
					<div class="total-comments"><a data-disqus-identifier="article_<?php echo $articles['article_id']; ?>" href="<?php echo $articles['href']; ?>#disqus_thread"><?php echo $text_comments_v; ?></a></div>
				  <?php } ?>
				<?php } ?>
				<?php if ($articles['button']) { ?>
					<div class="blog-button"><a class="button btn btn-green" href="<?php echo $articles['href']; ?>"><span><?php echo $button_detail; ?></span><i class="icon-rectangle-2-copy-3"></i></a></div>
				<?php } ?>
			</div>
			</div>

		<?php } ?>
  </div>
                <div class="row">
                    <div class="col-sm-12 text-center"><?php echo $pagination; ?></div>
                </div>

<?php } ?>
<?php if ($is_category) { ?>
  <?php if (!$ncategories && !$article) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
<?php } else { ?>
  <?php if (!$article) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
<?php } ?>

