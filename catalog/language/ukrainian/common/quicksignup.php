<?php
// Text
$_['text_signin_register']    = 'Аторизация/Регистрация';
$_['text_login']   			  = 'Аторизація';
$_['text_register']    		  = 'Регистрация';

$_['text_new_customer']       = 'Новий клієнт';
$_['text_returning']          = 'Авторизація';
$_['text_returning_customer'] = 'Я возвращающийся клиент';
$_['text_details']            = 'Ваші персональні дані';
$_['entry_email']             = 'Email';
$_['entry_name']              = 'І\'мя';
$_['entry_password']          = 'Пароль';
$_['entry_telephone']         = 'Телефон';
$_['text_forgotten']          = 'Забули пароль?';
$_['text_agree']              = 'Я принимаю  <a href="%s" class="agree"><b>%s</b></a>';



//Button
$_['button_login']            = 'Вхід';

//Error
$_['error_name']           = 'К-сть символів у полі повинно бути від 1 до 32!';
$_['error_lastname']       = 'К-сть символів у полі повинно бути від 1 до 32!';
$_['error_email']          = 'E-Mail вказан невірно';
$_['error_telephone']      = 'К-сть символів у полі повинно бути від 3 до 32!';
$_['error_password']       = 'К-сть символів у полі повинно бути від 4 до 20!';
$_['error_exists']         = 'E-Mail уже зареєстрований!';
$_['error_agree']          = 'Внимание: Вы должны принять %s!';
$_['error_warning']        = 'Увага: Перевірте коректність введених даних!';
$_['error_approved']       = 'Внимание: Ваша учетная запись требует одобрения, прежде чем вы сможете войти в систему.';
$_['error_login']          = 'Увага: Немає відповідності для E-Mail Address і \ або пароля.';
