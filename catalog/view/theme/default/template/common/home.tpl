<?php echo $header; ?>
<div class="curier-home-slider">
<div class="container-home-slider">
        <?php echo $content_top; ?>
    </div>
</div>

    <div class="container"  id="contents">
        <div class="row">
            <?php echo $column_left; ?>
            <?php echo $column_right; ?>
            <?php echo $home_category; ?>
        </div>
    </div>
<div class="container-home-gallery">
    <?php echo $home_gallery; ?>
</div>
<?php if (0) { ?>
    <div class="curier-instagram-home curier-grey">
            <div class="container container-home-instagram">
                <div class="row">
                        <div class="module_instagram_photos instagram_photos_second">
                            <div class="module_content">
                                <div class="block-title"><span>Instagram VSK</span><a href="https://www.instagram.com/vskplusshop/"><span>в instagram</span></a></div>
                                <?php
                                require_once 'inwidget/plugins/instagram-php-scraper/InstagramScraper.php';
                                require_once 'inwidget/plugins/unirest-php/Unirest.php';
                                require_once 'inwidget/inwidget.php';
                                $inWidget = new inWidget();
                                $inWidget->getData();
                                require_once 'inwidget/template.php';
                                ?>
                            </div>
                        </div>
                    </div>
            </div>
    </div>
<?php } ?>
    <div class="container container-bottom"><?php echo $content_bottom; ?></div>
<?php echo $footer; ?>