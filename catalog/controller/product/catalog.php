<?php
class ControllerProductCatalog extends Controller {
	public function index() {
		$this->load->language('product/category');
		$this->load->language('product/catalog');

		$this->load->model('catalog/category');

		$this->load->model('tool/image');


        $data['text_view_all'] = $this->language->get('text_view_all');


        $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);


        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_catalog'),
            'href' => $this->url->link('product/catalog')
        );


        $this->document->setTitle($this->language->get('meta_title'));

        $this->document->setDescription($this->language->get('catalog_description'));
        $this->document->setKeywords($this->language->get('catalog_keywords'));


        $categories = $this->model_catalog_category->getCategories(0);

        $this->load->model('tool/image');

        foreach ($categories as $category) {
            if ($category['catalog']) {
                $img_w = '270';
                $img_h = '470';

                if ($category['image']) {
                    $image = $this->model_tool_image->resize($category['image'], $img_w, $img_h);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $img_w, $img_h);
                }

                $data['categories'][] = array(
                    'description' => $category['description'],
                    'name' => $category['name'],
                    'image' => $image,
                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
        }

        $data['continue'] = $this->url->link('common/home');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/catalog.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/catalog.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/product/catalog.tpl', $data));
        }

	}
}
