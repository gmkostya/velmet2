<?php echo $header; ?>
<div class="container container-top">
  <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
    <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
      <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <?php if ($i + 1 < count($breadcrumbs)) { ?>
          <a itemscope itemtype="http://schema.org/Thing" itemprop="item"
             href="<?php echo $breadcrumb['href']; ?>">
            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
        <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
        <?php } ?>
      </li>
    <?php } ?>
  </ul>
  <?php echo $column_right; ?>
</div>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left) { ?>
      <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
      <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if (!$products) { ?>
        <h1><?php echo $heading_title; ?></h1>
      <?php }?>
      <?php if ($products) { ?>
        <div class="curier-h1-sort">
          <h1><?php echo $heading_title; ?></h1>
          <div class="row-sort">
            <div class="sort-label-select">
              <select id="input-sort" class="styling-select" onchange="location = this.value;">
                <?php foreach ($sorts as $sorts) { ?>
                  <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                    <option value="<?php echo $sorts['href']; ?>"
                            selected="selected"><?php echo $sorts['text']; ?></option>
                  <?php } else { ?>
                    <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </div>
            <div class="sort-label">
              <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
            </div>
          </div>
        </div>
        <div class="row">
          <?php foreach ($products as $product) { ?>
            <div class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <div class="product-thumb transition">
                <div class="image">
                  <?php if ($product['label']) { ?>
                    <div class="product-label"><?php echo $product['label']; ?></div>
                  <?php } ?>
                  <a href="<?php echo $product['href']; ?>">
                    <img src="<?php echo $product['thumb']; ?>"
                         alt="<?php echo $product['name']; ?>"
                         title="<?php echo $product['name']; ?>" class="img-responsive"/>
                  </a>
                  <div class="button-group">
                    <?php foreach ($product['options'] as $options) {
                      if ($options['option_id'] == '9') { ?>
                        <div class="option-sizes">
                          <?php foreach ($options['product_option_value'] as $value) { ?>
                            <span class="option-size"><?php echo $value['name']; ?></span>
                          <?php } ?>
                        </div>
                      <?php }
                    } ?>
                    <button type="button"
                            onclick="cart.add('<?php echo $product['product_id']; ?>');"
                            class="product-layout-button-cart"><i
                          class="icon-cart"></i><span><?php echo $button_cart; ?></span>
                    </button>
                      <?php if ($product['wishlist_status']){ ?>
                          <button type="button" data-toggle="tooltip"
                                  title="<?php echo $button_wishlist_is; ?>"
                                  onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                  class="product-layout-button-wishlist red js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-heart_2"></i>
                          </button>
                      <?php } else { ?>
                          <button type="button" data-toggle="tooltip"
                                  title="<?php echo $button_wishlist; ?>"
                                  onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                  class="product-layout-button-wishlist js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-uniF08B"></i>
                          </button>
                      <?php } ?>
                  </div>
                </div>
                <div class="caption">
                  <div class="name"><a
                        href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                  </div>
                  <?php if ($product['price']) { ?>
                    <div class="price">
                      <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                      <?php } else { ?>
                        <span class="price-new"><?php echo $product['special']; ?></span> <span
                            class="price-old"><?php echo $product['price']; ?></span>
                      <?php } ?>
                      <?php if ($product['tax']) { ?>
                        <span
                            class="price-tax"><?php echo $text_tax; ?><?php echo $product['tax']; ?></span>
                      <?php } ?>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
        <div class="row">
          <div class="col-sm-12 text-right"><?php echo $pagination; ?></div>
        </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
  </div>
</div>
<?php echo $footer; ?>
