<?php echo $header; ?>
<div class="container container-top">
  <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
    <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
      <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <?php if ($i + 1 < count($breadcrumbs)) { ?>
          <a itemscope itemtype="http://schema.org/Thing" itemprop="item"
             href="<?php echo $breadcrumb['href']; ?>">
            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
        <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
        <?php } ?>
      </li>
    <?php } ?>
  </ul>
  <?php echo $column_right; ?>
</div>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left) { ?>
      <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
      <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if (!$products) { ?>
        <h1><?php echo $heading_title; ?></h1>
      <?php }?>
        <?php if ($products) { ?>
            <div class="row">
                <div class="col-sm-12 ">
                    <div class="col-sort-limits">
                        <div class="col-sort-label">
                            <div class="col-label">
                                <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                            </div>
                            <div class="col-sort">
                                <select id="input-sort" class="form-control" onchange="location = this.value;">
                                    <?php foreach ($sorts as $sorts) { ?>
                                        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-list-grid">
                            <button type="button" id="list-view" class="btn btn-view" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button><button type="button" id="grid-view" class="btn btn-view" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
                        </div>
                        <div class="col-limits-label">
                            <div class="col-label">
                                <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                            </div>
                            <div class="col-limits">
                                <select id="input-limit" class="form-control" onchange="location = this.value;">
                                    <?php foreach ($limits as $limits) { ?>
                                        <?php if ($limits['value'] == $limit) { ?>
                                            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 product-box-curier">
                    <div class="product-box <?php if (count($products) < 4 ) { ?> product-box-limmit <?php } ?>">
                        <?php foreach ($products as $product) { ?>
                            <div class="product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="product-thumb transition">
                                    <div class="image">
                                        <?php if ($product['label']) { ?>
                                            <div class="product-label <?php echo  $product['label_type']; ?>"><?php echo $product['label']; ?></div>
                                            <?php if ($product['count_end_date']) { ?>
                                                <div class="timer-curier">
                                                    <div class="timer" data-timeend="<?php echo $product['count_end_date']; ?>" data-timetext="<?php echo $product['name']; ?>"></div>
                                                    <div class="timer-text" ><span class="t-day" >дней</span><span class="t-hour">часов</span><span class="t-min">мин</span><span class="t-sec">сек</span></div>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                        <a href="<?php echo $product['href']; ?>">
                                            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                                        </a>
                                    </div>
                                    <div class="caption">
                                        <div class="rating">
                                            <?php if ($product['rating']) { ?>
                                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                    <?php if ($product['rating'] < $i) { ?>

                                                    <?php } else { ?>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="name" ><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                                    </div>
                                    <div class="button-group">
                                        <?php if ($product['price']) { ?>
                                            <div class="price">
                                                <?php if ($product['special']) { ?>
                                                    <span class="price-old"><?php echo $product['price']; ?></span>
                                                    <span class="price-new"><?php echo $product['special']; ?></span>
                                                <?php } else { ?>
                                                    <?php echo $product['price']; ?>
                                                <?php } ?>
                                                <?php if ($product['tax']) { ?>
                                                    <span
                                                            class="price-tax"><?php echo $text_tax; ?><?php echo $product['tax']; ?></span>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>

                                        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="product-layout-button-cart"><i class="icon-cart"></i><span><?php echo $button_cart; ?></span></button>
                                        <?php if ($product['wishlist_status']){ ?>
                                            <button type="button" data-toggle="tooltip"
                                                    title="<?php echo $button_wishlist_is; ?>"
                                                    onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                                    class="product-layout-button-wishlist red js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-forma-1-copy-5"></i>
                                            </button>
                                        <?php } else { ?>
                                            <button type="button" data-toggle="tooltip"
                                                    title="<?php echo $button_wishlist; ?>"
                                                    onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                                    class="product-layout-button-wishlist js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-forma-1-copy-5"></i>
                                            </button>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center"><?php echo $pagination; ?></div>
            </div>
        <?php } ?>
      <?php echo $content_bottom; ?></div>
  </div>
</div>
<?php echo $footer; ?>
