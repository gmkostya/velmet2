<div class="product-box product-box-slider product-box-slider-bestseller">
  <div class="block-title"><span><?php echo $heading_title; ?></span><a href="<?php echo $bestseller; ?>"><span><?php echo $text_see_all; ?></span><i class="icon-rectangle-2-copy-3"></i></a></div>
  <div class="block-title-mob"><a href="<?php echo $bestseller; ?>"><span><?php echo $heading_title; ?></span><i class="icon-rectangle-2-copy-3"></i></a></div>
  <div class="js-bestseller-slider">
    <?php foreach ($products as $product) { ?>
      <div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="product-thumb transition">
          <div class="image">
            <?php if ($product['label']) { ?>
              <div class="product-label <?php echo  $product['label_type']; ?>"><?php echo $product['label']; ?></div>
              <?php if ($product['count_end_date']) { ?>
                <div class="timer-curier">
                  <div class="timer" data-timeend="<?php echo $product['count_end_date']; ?>" data-timetext="<?php echo $product['name']; ?>"></div>
                  <div class="timer-text" ><span class="t-day" ><?php echo $text_day; ?></span><span class="t-hour"><?php echo $text_hours; ?></span><span class="t-min"><?php echo $text_min; ?></span><span class="t-sec"><?php echo $text_sec; ?></span></div>
                </div>
              <?php } ?>
            <?php } ?>
            <a href="<?php echo $product['href']; ?>">
              <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
            </a>
          </div>
          <div class="caption">
            <div class="rating">
              <?php if ($product['rating']) { ?>
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>

                  <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
            </div>
            <div class="name" ><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
          </div>
          <div class="button-group">
            <?php if ($product['price']) { ?>
              <div class="price">
                <?php if ($product['special']) { ?>
                  <span class="price-old"><?php echo $product['price']; ?></span>
                  <span class="price-new"><?php echo $product['special']; ?></span>
                <?php } else { ?>
                  <?php echo $product['price']; ?>
                <?php } ?>
                <?php if ($product['tax']) { ?>
                  <span
                      class="price-tax"><?php echo $text_tax; ?><?php echo $product['tax']; ?></span>
                <?php } ?>
              </div>
            <?php } ?>

            <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="product-layout-button-cart"><i class="icon-cart"></i><span><?php echo $button_cart; ?></span></button>
            <?php if ($product['wishlist_status']){ ?>
              <button type="button" data-toggle="tooltip"
                      title="<?php echo $button_wishlist_is; ?>"
                      onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                      class="product-layout-button-wishlist red js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-forma-1-copy-5"></i>
              </button>
            <?php } else { ?>
              <button type="button" data-toggle="tooltip"
                      title="<?php echo $button_wishlist; ?>"
                      onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                      class="product-layout-button-wishlist js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-forma-1-copy-5"></i>
              </button>
            <?php } ?>
          </div>

        </div>
      </div>
    <?php } ?>
  </div>
</div>
