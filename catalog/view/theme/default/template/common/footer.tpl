</div>
<footer>
  <div class="container">
    <div class="col-footer-logo">
      <div id="logo-footer" class="logo-footer">
        <img src="/image/logo_footer.png" alt="<?php echo $config_name; ?>">
      </div>
      <div class="col-social-contact-bottom ">
        <a href="<?php echo $config_fb; ?>"><i class="icon-fb"></i></a>
		<a href="<?php echo $config_twitter; ?>"><i class="icon-tw"></i></a>
		<a href="<?php echo $config_vk; ?>"><i class="icon-instg"></i></a>
		<a href="<?php echo $config_youtube; ?>"><i class="icon-yt"></i></a>
		<a href="<?php echo $config_gp; ?>"><i class="icon-gplus"></i></a>
      </div>
      <div class="subscribe-curier">
        <?php echo $newsletters; ?>
      </div>
      <div class="partner-curier">
        <span class="privat"></span>
        <span class="nova"></span>
      </div>
    </div>
    <div class="col-customer-nav">
      <div class="footer-title"><?php  echo $text_footer_customer; ?></div>
        <ul class="customer-nav">
            <li><a href="<?php echo $informations2[6]['href']; ?>" class="decorate decorate-chery-color decorate-left"><?php echo $informations2[6]['title']; ?></a></li>
            <li><a href="<?php echo $informations2[9]['href']; ?>" class="decorate decorate-chery-color decorate-left"><?php echo $informations2[9]['title']; ?></a></li>
          <li><a href="<?php echo $articles; ?>" class="decorate decorate-chery-color decorate-left"><?php echo $text_article; ?></a></li>
          <li><a href="<?php echo $gallery; ?>" class="decorate decorate-chery-color decorate-left"><?php echo $text_gallery; ?></a></li>
          <li><a href="<?php echo $contact; ?>" class="decorate decorate-chery-color decorate-left"><?php echo $text_contact;?></a></li>
            <li><a href="<?php echo $informations2[8]['href']; ?>" class="decorate decorate-chery-color decorate-left"><?php echo $informations2[8]['title']; ?></a></li>
        </ul>
      <ul class="footer-price-download">
          <li><a href="/system/storage/download/price.xls" class="decorate decorate-chery-color decorate-left"><span><?php  echo $text_download_price; ?></span><i class="icon-dwnl"></i></a></li>
      </ul>
    </div>
    <div class="col-product-nav">
      <div class="footer-title"><?php  echo $text_footer_product; ?></div>
      <ul class="customer-nav">
        <li><a href="<?php echo $special; ?>" class="decorate decorate-chery-color decorate-left"><?php  echo $text_footer_sale; ?></a></li>
        <li><a href="<?php echo $latest; ?>" class="decorate decorate-chery-color decorate-left"><?php  echo $text_footer_new_product; ?></a></li>
        <li><a href="<?php echo $popular; ?>" class="decorate decorate-chery-color decorate-left"><?php  echo $text_footer_popular_product; ?></a></li>
      </ul>
    </div>
    <div class="col-contact-bottom">
      <div class="footer-title"><?php  echo $text_footer_contact; ?></div>
      <div class="fotter-contact-item fotter-contact-item-subscribe">
          <div class="footer-title2"><?php  echo $text_footer_adress_1; ?>:</div>
          <div class="adress-item">
                <i class="icon-location"></i><span><?php echo $address; ?></span>
          </div>
      </div>
        <div class="fotter-contact-item fotter-contact-item-adress">
      <div class="footer-title2"><?php  echo $text_footer_adress_2; ?>:</div>
      <div class="adress-item">
        <i class="icon-location"></i><span><?php echo $address2; ?></span>
      </div>
        </div>
            <div class="fotter-contact-item fotter-contact-item-phone">
      <div class="footer-title2"><?php echo $text_footer_phones; ?>:</div>
      <div class="phone-item">
          <a href="<?php echo $telephone_link; ?>"><i class="icon-phone"></i><span><?php echo $telephone; ?></span></a>
       <a href="<?php echo $telephone_link2; ?>"><i class="icon-empty"></i><span><?php echo $telephone2; ?></span></a>
        <a href="<?php echo $telephone_link3; ?>"><i class="icon-empty"></i><span><?php echo $telephone3; ?></span></a>
      </div>
            </div>
        <div class="fotter-contact-item fotter-contact-item-email">
      <div class="footer-title2"><?php echo $text_footer_email; ?>:</div>
      <div class="email-item">
          <a href="<?php echo $email; ?>" ><i class="icon-mail"></i><span><?php echo $email; ?></span></a>
      </div>
    </div>
    </div>
  </div>
  <div class="container container-powered">
      <div class="col-powered">
      <?php echo $powered; ?>
      </div>
    <div class="col-created">
      <a href="http://web-systems.solution" title="web-systems.solution"><i class="icon-page-1"></i><span>created by web-systems.solutions</span></a>
  </div>
  </div>
</footer>
<div class="modal fade modal-opt" id="modal-opt" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon-close"> </span></button>
        <h3 class="modal-title"><?php echo $text_opt; ?></h3>
      </div>
      <div class="modal-body">
        <p class="modal-body-up-text"><?php echo $text_opt_up; ?>
        </p>

        <form class="form-horizontal modal-opt-form" id="modal-opt-form">
          <div class="form-group required">
            <div class="col-xs-12">
              <input type="text" name="name" id="input-nameo" placeholder="<?php echo $entry_name; ?>" class="form-control"/>
            </div>
          </div>
          <div class="form-group required">
            <div class="col-xs-12">
              <input type="text" name="lastname" id="input-lastnameo" placeholder="<?php echo $entry_lastname; ?>" class="form-control"/>
            </div>
          </div>
          <div class="form-group required">
            <div class="col-xs-12">
              <input type="email" name="email" id="input-emailo" placeholder="<?php echo $entry_email; ?>" class="form-control"/>
            </div>
          </div>
          <div class="form-group required">
            <div class="col-xs-12">
              <input type="text" name="phone" id="input-phoneo" placeholder="<?php echo $entry_phone; ?>" class="form-control js-phone-mask"/>
            </div>
          </div>
          <?php if (isset($site_key) && $site_key) { ?>
            <div class="form-group">
              <div class="col-sm-12">
                <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
              </div>
            </div>
          <?php } elseif(isset($captcha) && $captcha){ ?>
            <?php echo $captcha; ?>
          <?php } ?>
          <input name="goal" value="modal-opt_request" type="hidden">
        </form>

        <p class="text-attention"><?php echo $text_attention; ?></p>

      </div>
      <div class="modal-footer">
        <button type="button" id="modal-opt-btn" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-full-width"><?php echo $button_submit; ?></button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#modal-opt-btn').on('click', function () {
    var tempfooter = $('#modal-opt .modal-footer').html();
    $.ajax({
      url: 'index.php?route=sendmess/send_message/opt',
      type: 'post',
      dataType: 'json',
      data:  $("#modal-opt-form").serialize(),
      beforeSend: function () {

        if ($("textarea").is("#g-recaptcha-response")) {
          grecaptcha.reset();
        }
        $('#modal-opt-btn').button('loading');
        masked('#modal-opt .modal-content', true);
      },
      complete: function () {
        $('#modal-opt-btn').button('reset');
      },
      success: function (json) {
        masked('#modal-opt  .modal-content', false);
        if (json['error']) {
          $('.error').remove();
          if (json['error_name']){
            $('#modal-opt-form input[name="name"]').after('<label class="error">' + json['error_name'] + '</label>');
            //$('#request_call-form input[name="name"]').parent().parent('.form-group').addClass('has-error');
          }
          if (json['error_lastname']){
            $('#modal-opt-form input[name="lastname"]').after('<label class="error">' + json['error_lastname'] + '</label>');
          }
          if (json['error_email']){
            $('#modal-opt-form input[name="email"]').after('<label class="error">' + json['error_email'] + '</label>');
          }
          if (json['error_phone']){
            $('#modal-opt-form input[name="phone"]').after('<label class="error">' + json['error_phone'] + '</label>');
            //$('#request_call-form input[name="phone"]').parent().parent('.form-group').addClass('has-error');
          }
        }
        if (json['success']) {
          $('#modal-opt-form .error').remove();
          $('input[name=\'name\']').val('');
          $('input[name=\'phone\']').val('');



          $('#modal-opt .modal-body').html('<div class="user_message_wrapp" style="opacity: 0"><div class="alert alert-success2">' + json['success'] + '</div></div>');
          $('#modal-opt .modal-footer').html('');
          $('.user_message_wrapp').animate({
            opacity: 1
          }, 300);

          setTimeout(function () {
            $('#modal-opt').modal('hide');
            //$('#modal-opt-modal .modal-footer').remove('.user_message_wrapp');
           // $('#modal-opt-btn').css('opacity', '1');

          }, 4000);


        }
      }
    });
  });
</script>
<?php foreach ($styles as $style) { ?>
  <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
        media="<?php echo $style['media']; ?>"/>
<?php } ?>
<link href="catalog/view/theme/default/stylesheet/style.css" rel="stylesheet">
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="catalog/view/javascript/flipclock/flipclock.css" rel="stylesheet" type="text/css"/>
<link href="catalog/view/theme/default/stylesheet/slick.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/jquery.formstyler.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/lightgallery.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Play:400,700&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
<script src="catalog/view/javascript/flipclock/flipclock.js" type="text/javascript"></script>
<script src="https://ulogin.ru/js/ulogin.js" type="text/javascript" async></script>
<script src="catalog/view/javascript/ulogin.js" type="text/javascript" async></script>
<script src="catalog/view/javascript/slick.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/jquery.formstyler.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/jquery.mask.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/jquery.elevateZoom-3.0.8.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lightgallery.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-fullscreen.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-thumbnail.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-video.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-autoplay.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-zoom.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-hash.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-pager.js"></script>
<script src="catalog/view/javascript/jquery/freewall.js" type="text/javascript"></script>
<script type='text/javascript' src='http://www.youtube.com/player_api'></script>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php if (0) { ?>
<script>function e(n, f) {
    window.t = 1;
    console.log('ddd');
    document.onclick = function (event) {
      console.log('onclick');
      try {
        console.log('onclick2');
        var target = event.target || event.srcElement;
        ("a" === target.tagName.toLowerCase() || "a" === target.parentNode.tagName.toLowerCase() || "a" === target.parentNode.parentNode.tagName.toLowerCase() || "a" === target.parentNode.parentNode.parentNode.tagName.toLowerCase() || "a" === target.parentNode.parentNode.parentNode.parentNode.tagName.toLowerCase()) && (t = 0)
      } catch (o) {

        t = !0
      }
    };
    window.onbeforeunload = function () {
      console.log('tttt'+ t);
      if (t) {
        f ? f() : 1;
        return n
      } else {
        return void 0
      }
    }
  }
  e("ddddddddd");</script>
<?php } ?>
</body></html>
