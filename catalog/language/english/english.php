<?php
// Locale
$_['code']                  = 'en';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ' ';

// Text
$_['text_home']	=	'Home';
$_['text_yes']              = 'Yes';
$_['text_no']               = 'No';
$_['text_none']             = ' --- None --- ';
$_['text_select']           = ' --- Please Select --- ';
$_['text_all_zones']        = 'All Zones';
$_['text_pagination']       = 'Showing %d to %d of %d (%d Pages)';
$_['text_loading']          = 'Loading...';

// Buttons
$_['button_address_add']    = 'Add Address';
$_['button_back']           = 'Back';
$_['button_continue']       = 'Continue';
$_['button_cart']           = 'Add to Cart';
$_['button_cancel']         = 'Cancel';
$_['button_compare']        = 'Compare this Product';
$_['button_wishlist']       = 'Add to Wish List';
$_['button_checkout']       = 'Checkout';
$_['button_confirm']        = 'Confirm Order';
$_['button_coupon']         = 'Apply Coupon';
$_['button_delete']         = 'Delete';
$_['button_download']       = 'Download';
$_['button_edit']           = 'Edit';
$_['button_filter']         = 'Refine Search';
$_['button_new_address']    = 'New Address';
$_['button_change_address'] = 'Change Address';
$_['button_reviews']        = 'Reviews';
$_['button_write']          = 'Write Review';
$_['button_login']          = 'Login';
$_['button_update']         = 'Update';
$_['button_remove']         = 'Remove';
$_['button_reorder']        = 'Reorder';
$_['button_return']         = 'Return';
$_['button_shopping']       = 'Continue Shopping';
$_['button_search']         = 'Search';
$_['button_shipping']       = 'Apply Shipping';
$_['button_submit']         = 'Submit';
$_['button_guest']          = 'Guest Checkout';
$_['button_view']           = 'View';
$_['button_voucher']        = 'Apply Voucher';
$_['button_upload']         = 'Upload File';
$_['button_reward']         = 'Apply Points';
$_['button_quote']          = 'Get Quotes';
$_['button_list']           = 'List';
$_['button_grid']           = 'Grid';
$_['button_map']            = 'View Google Map';

// Error
$_['error_exception']       = 'Error Code(%s): %s in %s on line %s';
$_['error_upload_1']        = 'Warning: The uploaded file exceeds the upload_max_filesize directive in php.ini!';
$_['error_upload_2']        = 'Warning: The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form!';
$_['error_upload_3']        = 'Warning: The uploaded file was only partially uploaded!';
$_['error_upload_4']        = 'Warning: No file was uploaded!';
$_['error_upload_6']        = 'Warning: Missing a temporary folder!';
$_['error_upload_7']        = 'Warning: Failed to write file to disk!';
$_['error_upload_8']        = 'Warning: File upload stopped by extension!';
$_['error_upload_999']      = 'Warning: No error code available!';


/**/
$_['text_catalog']	=	'Catalog';
$_['textDo']	=	'from';
$_['textOt']	=	'to';
$_['text_see_all_novelty']	=	'view all';
$_['text_see_all_special']	=	'view all';
$_['text_new']	=	'<span>NEW</span><i>new</i>';
$_['text_sale']	=	'<span>SALE</span><i></i>';
$_['text_price']	=	'Price';
$_['button_one_click']	=	'By 1 click';
$_['button_review_send_title']	=	'Send review';
$_['button_register']	=	'Sign Up';
$_['text_share']	=	'Share product';
$_['text_callback_up']	=	'Leave your phone number.<br/> We will contact you as soon as possible.';
$_['text_attention']	=	'* All fields are required.';
$_['button_zakazat']	=	'To order';
$_['entry_password2']	=	'Repeat password';
$_['entry_lastname']	=	'Surname';
$_['text_lastname']	=	'Surname';
$_['entry_telephone']	=	'Phone';
$_['text_customer_info']	=	'Fill out information about yourself';
$_['text_always_registered']	=	'I am already registered';
$_['text_info_login']	=	'Enter your e-mail and password to login ';
$_['error_password2']	=	'Password does not match';
$_['text_opt']	=	'I am a wholesale buyer';
$_['text_social_login']	=	'You can enter without registering through social networks: ';
$_['text_view_all']	=	'View All';
$_['button_submit2']	=	'send a message';
$_['entry_customer_opt']	=	'I am a wholesale buyer';
$_['text_title_cart']	=	'Cart';
$_['text_continue_shopping']	=	'Back to catalog';
$_['text_review_up']	=	'Leave feedback on this product.<br/>Your opinion is very important to us.';
$_['text_opt_up']	=	'Fill out information about yourself.<br/>We will contact you as soon as possible.';
$_['text_wishlist2']      = ' My Favorites';
$_['text_opt']      = ' Become a wholesaler';
$_['error_lastname']      = 'Obligatory field';
$_['text_email']      = 'Email';
$_['entry_email']      = 'Email';
$_['text_menu']      = 'Menu';
$_['banner_title']      = 'New Collection';
$_['camback_home']      = 'Go to home page';
$_['button_wishlist_is']      = 'Added';

$_['text_header_contact']      = 'Contacts';
