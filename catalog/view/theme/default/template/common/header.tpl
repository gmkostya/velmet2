<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;
        if (isset($_GET['page'])) {
            echo " - " . ((int)$_GET['page']) . " " . $text_page;
        } ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description;
        if (isset($_GET['page'])) {
            echo " - " . ((int)$_GET['page']) . " " . $text_page;
        } ?>"/>
    <?php } ?>
    <?php if ($keywords) { ?>
        <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <meta property="og:title" content="<?php echo $title;
    if (isset($_GET['page'])) {
        echo " - " . ((int)$_GET['page']) . " " . $text_page;
    } ?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo $og_url; ?>"/>
    <?php if ($og_image) { ?>
        <meta property="og:image" content="<?php echo $og_image; ?>"/>
    <?php } else { ?>
        <meta property="og:image" content="<?php echo $logo; ?>"/>
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>"/>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
    <?php } ?>
    <!--[if lt IE 10]>
    <link rel="stylesheet" href="https://rawgit.com/codefucker/finalReject/master/reject/reject.css" media="all" />
    <script type="text/javascript" src="https://rawgit.com/codefucker/finalReject/master/reject/reject.min.js" data-text="Unfortunately, the browser you use, outdated and can not properly display the site. Please download any of the following browsers:"></script>
    <![endif]-->
    <style>
        @media (min-width:1200px){.btn-close-head-menu, .menu-toggle, .list-inline > li.hidden-desctop{display:none!important;}
            #search:hover .input-curier, #search .input-curier.open{width:640px;}
        }
        .home-slider, .newcollection-slider {
            opacity: 0;
            transition:all 0.25s ease-in-out;
        }
    </style>
</head>
<body class="<?php echo $class; ?> lng-<?php echo $lang; ?>">
<div class="wrapper">
<div class="header-curier">
<div id="top">
    <div class="container">
        <div class="curier-contact-top pull-left">
        <div class="btn-group">
            <a href="tel:<?php echo $telephone_link; ?>" class="btn btn-link "><i class="icon-phone"></i><span><?php echo $telephone; ?></span></a><i class="icon-arrow-dawn dropdown-toggle"  data-toggle="dropdown"></i>
            <ul class="dropdown-menu">
                <?php if ($telephone2) { ?>
                <li>
                    <a href="tel:<?php echo $telephone_link2; ?>" class="decorate decorate-white-color decorate-left"><i>tel: </i><span><?php echo $telephone2; ?></span></a>
                </li><?php } ?>
                <?php if ($telephone3) { ?>
                <li>
                    <a href="tel:<?php echo $telephone_link3; ?>" class="decorate decorate-white-color decorate-left"><i>tel: </i><span><?php echo $telephone3; ?></span></a>
                </li><?php } ?>
            </ul>
        </div>
        </div>
       <div class="pull-right curier-lng-currency">
           <?php echo $currency; ?>
           <?php echo $language; ?>
       </div>
        <div class="col-user-curier">
            <ul class="row-user">
                <li class="dropdown col-welcom">
                    <div class="col-user-item welcom">
                        <?php if ($logged) { ?>
                        <button  title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-account"></i><span><?php echo $text_account; ?></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                        </ul>
                        <?php } else { ?>
                        <button type="button"  title="<?php echo $text_login; ?>" class="quick_login"><i class="icon-account"></i><span><?php echo $text_account; ?></span></button>
                        <?php } ?>
                    </div>
                </li>
                <li class="col-cart">
                    <?php echo $cart; ?>
                </li>
                <li class="col-wishlist">
                    <div class="col-user-item wishlist">
                        <a href="<?php echo $wishlist; ?>" id="wishlist-total" class="wishlist-total <?php  if((int)$text_wishlist>0) { ?> wishlist-full<?php } ?>" title="<?php echo $text_wishlist2; ?>"><i class="icon-forma-1-copy-5"></i><span><?php echo $text_wishlist; ?></span></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<header>
    <div class="container">
        <div class="col-logo">
            <div id="logo">
                <?php if ($logo) { ?>
                    <?php if ($home == $og_url) { ?>
                        <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="logo-full" />
                        <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="logo-small" />
                    <?php } else { ?>
                        <a href="<?php echo $home; ?>">
                            <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>"
                                                            alt="<?php echo $name; ?>" class="img-responsive logo-full"/>
                            <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive logo-small" />
                        </a>
                    <?php } ?>
                <?php } else { ?>
                    <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                <?php } ?>
            </div>
        </div>
        <div class="col-menu">
            <nav id="menu" class="navbar">
                <div class="navbar-header">
                    <button type="button" class="menu-toggle"><span class="icon-bar"></span> <span class="text-bar"><?php echo $text_menu; ?></span></button>
                </div>
                <div class="menu-container">
                    <div class="menu-popup-header">
                    <button class="menu-toggle">
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <ul class="nav navbar-nav list-unstyled">
                        <li ><a  href="<?php echo $informations[4]['href']; ?>" class="<?php echo $_SERVER['HTTP_HOST'] .''.$current_url; $active = strpos($informations[4]['href'], $current_url);  if ($active !== false AND $current_url!=$_SERVER['HTTP_HOST'].'/') {  echo ' active '; } ?> headermenu_id-aboute"><span><?php echo $informations[4]['title']; ?></span></a></li>
                        <?php if ($categories) { ?>
                            <li class="dropdown catalog-link"><a href="javascript:void()" class="<?php  if (!empty($ispath)) {  echo ' active '; } ?> dropdown-toggle"
                                                    data-toggle="dropdown"><span><?php echo $text_catalog; ?></span><i class="fa fa-caret-down"></i></a>
                                <div class="dropdown-menu">
                                    <div class="dropdown-inner">
                                <ul class="list-unstyled">
                                    <?php $k=0; foreach ($categories as $category) { $k++; ?>
                                        <?php if (($category['children']) and (1)) { ?>
                                            <li class="menu-category menu-category-count-<?php echo $k; ?> is-childe">
											<a href="<?php echo $category['href']; ?>"><span class="name"><?php echo $category['name']; ?></span><span class="image"><img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>"></span></a>
                                                <div class="childe-menu">
                                                    <div class="childe-menu-inner">
                                                            <ul class="list-unstyled">
                                                                <?php foreach ($category['children'] as $child) { ?>
                                                                    <li>
                                                                        <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } else { ?>
                                            <li class="menu-category menu-category-count-<?php echo $k; ?>">
											<a href="<?php echo $category['href']; ?>"><span class="name"><?php echo $category['name']; ?></span><span class="image"><img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>"></span></a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                                </div>
                                </div>
                            </li>
                        <?php } ?>
                        <?php if(0){ ?>
                        <?php foreach($headermenu as $header){ ?>
                                <?php if($header['sub_title']) { ?>
                                <li class="dropdown"><a class="dropdown-toggle dropdown-toggle-catalog headermenu_id-<?php echo $header['headermenu_id']; ?>" <?php if (!empty($header['link'])) { ?>href="<?php echo $header['link'] ?>" <?php } ?>data-toggle="dropdown"><span><?php echo $header['title']; ?></span><i class="fa fa-caret-down"></i></a>
                                    <div class="dropdown-menu">
                                        <div class="dropdown-inner">
                                            <ul class="list-unstyled">
                                                <?php foreach($header['sub_title'] as $subtitle){ ?>
                                                    <li>
                                                        <?php if((isset($subtitle['href'])) and (!empty($subtitle['href']))){?>
                                                            <a href="<?php echo $subtitle['href']; ?>"><?php echo $subtitle['title']; ?></a>
                                                        <?php }else{?>
                                                            <a href="<?php echo $subtitle['link']?>"><?php echo $subtitle['title']; ?></a>
                                                        <?php } ?>
                                                        <?php if($header['sub_title']){ ?>
                                                            <ul>
                                                                <?php foreach($subtitle['sub_title'] as $subtitle){ ?>
                                                                    <li>
                                                                        <?php if(isset($subtitle['href'])){?>
                                                                            <a href="<?php echo $subtitle['href']; ?>"><?php echo $subtitle['title']; ?></a>
                                                                        <?php }else{?>
                                                                            <a href="<?php echo $subtitle['link']?>"><?php echo $subtitle['title']; ?></a>
                                                                        <?php } ?>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php }?>
                                                    </li>
                                                <?php }?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php } else { ?>
                            <li ><a  href="<?php echo $header['link'] ?>" class="headermenu_id-<?php echo $header['headermenu_id']; ?>"><span><?php echo $header['title']; ?></span></a>
                            </li>
                            <?php  }?>
                        <?php  }?>
                        <?php  }?>
                        <li ><a  href="<?php echo $informations[6]['href']; ?>" class="<?php $active = strpos($informations[6]['href'], $current_url);  if ($active !== false AND $current_url!=$_SERVER['HTTP_HOST'].'/') {  echo ' active '; } ?> headermenu_id-6"><span><?php echo $informations[6]['title']; ?></span></a></li>
                        <li ><a  href="<?php echo $article_link; ?>" class="<?php $active = strpos($article_link, $current_url);  if ($active !== false AND $current_url!=$_SERVER['HTTP_HOST'].'/') {  echo ' active '; } ?> headermenu_id-article"><span><?php echo $article_title; ?></span></a></li>
                        <li ><a  href="<?php echo $gallery_link; ?>" class="<?php $active = strpos($gallery_link, $current_url);  if ($active !== false AND $current_url!=$_SERVER['HTTP_HOST'].'/') {  echo ' active '; } ?>  headermenu_id-article"><span><?php echo $gallery_title; ?></span></a></li>
                        <li ><a  href="<?php echo $contact; ?>" class="<?php $active = strpos($contact, $current_url);  if ($active !== false AND $current_url!=$_SERVER['HTTP_HOST'].'/') {  echo ' active '; } ?>headermenu_id-gallery"><span><?php echo $text_header_contact; ?></span></a></li>


                    </ul>
                </div>
            </nav>
        </div>
        <div class="col-serach">
            <?php echo $search; ?>
        </div>
    </div>
</header>
 </div>
<?php if ($categories) { ?>
    <div class="container container-after-head">
    </div>
<?php } ?>
