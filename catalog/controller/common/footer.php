<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

	//$data['scripts'] = $this->document->getScripts('footer');

        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts();

       // var_dump($data['scripts']);

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_gallery'] = $this->language->get('text_gallery');
		$data['text_article'] = $this->language->get('text_article');
		$data['text_footer_customer'] = $this->language->get('text_footer_customer');

		$data['text_footer_product'] = $this->language->get('text_footer_product');
		$data['text_footer_new_product'] = $this->language->get('text_footer_new_product');
		$data['text_footer_popular_product'] = $this->language->get('text_footer_popular_product');
		$data['text_footer_sale'] = $this->language->get('text_footer_sale');


		$data['text_footer_contact'] = $this->language->get('text_footer_contact');
		$data['text_footer_adress_1'] = $this->language->get('text_footer_adress_1');
		$data['text_footer_adress_2'] = $this->language->get('text_footer_adress_2');
		$data['text_footer_phones'] = $this->language->get('text_footer_phones');
		$data['text_footer_email'] = $this->language->get('text_footer_email');
		
		



		$data['text_phone'] = $this->language->get('text_phone');
        $data['text_call'] = $this->language->get('text_call');
        $data['entry_lastname'] = $this->language->get('entry_lastname');
        $data['text_lastname'] = $this->language->get('text_lastname');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['text_email'] = $this->language->get('text_email');

        $data['text_name'] = $this->language->get('text_name');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_phone'] = $this->language->get('entry_phone');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_callback_up'] = $this->language->get('text_callback_up');
		$data['text_attention'] = $this->language->get('text_attention');
		$data['button_zakazat'] = $this->language->get('button_zakazat');
		$data['text_opt_up'] = $this->language->get('text_opt_up');
		$data['text_opt'] = $this->language->get('text_opt');
		$data['button_submit'] = $this->language->get('button_submit');
		$data['text_download_price'] = $this->language->get('text_download_price');
		
		
		
		
		

		$this->load->model('catalog/information');

		$data['informations'] = array();


        $in_data = $this->model_catalog_information->getInformations();

		foreach ($in_data as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}
		foreach ($in_data as $result) {

				$data['informations2'][$result['information_id']] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);

		}



		$data['contact'] = $this->url->link('information/contact');
		$data['articles'] = $this->url->link('news/ncategory', '&ncat=59');
		//$data['gallery'] = $this->url->link('news/ncategory', '&ncat=60');

        $news_id = 22;
        $data['gallery'] = $this->url->link('news/article', '&news_id=' .  $news_id);

		$data['return'] = $this->url->link('account/return/add', '', 'SSL');
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
		$data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
		$data['special'] = $this->url->link('product/special', '', 'SSL');
		$data['latest'] =  $this->url->link('product/category', 'path=2');
		$data['popular'] =  $this->url->link('product/category', 'path=81');
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');

		$data['powered'] = sprintf($this->language->get('text_powered'), date('Y', time()), $this->config->get('config_name'));


        $data['telephone'] = $this->config->get('config_telephone');
        $data['telephone2'] = $this->config->get('config_telephone2');
        $data['telephone3'] = $this->config->get('config_telephone3');
        
		$data['config_fb'] = $this->config->get('config_fb');
        $data['config_vk'] = $this->config->get('config_vk');
        $data['config_gp'] = $this->config->get('config_gp');
        $data['config_youtube'] = $this->config->get('config_youtube');
        $data['config_twitter'] = $this->config->get('config_twitter');
		
		
		

   
		 $data['address'] =html_entity_decode($this->config->get('config_address'), ENT_QUOTES, 'UTF-8');  /*nl2br($this->config->get('config_address'));*/
		 $data['address2'] =html_entity_decode($this->config->get('config_address2'), ENT_QUOTES, 'UTF-8');  /*nl2br($this->config->get('config_address'));*/


        $data['email'] = $this->config->get('config_email');
        $data['config_name'] = $this->config->get('config_name');


        $data['telephone_link'] = preg_replace('![^0-9]+!', '', $this->config->get('config_telephone'));
        $data['telephone_link2'] = preg_replace('![^0-9]+!', '', $this->config->get('config_telephone2'));
        $data['telephone_link3'] = preg_replace('![^0-9]+!', '', $this->config->get('config_telephone3'));

        $data['newsletters'] = $this->load->controller('module/lt_newsletter');


        // Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/footer.tpl', $data);
		} else {
			return $this->load->view('default/template/common/footer.tpl', $data);
		}
	}
}
