<?php echo $header; ?>
    <div class="container container-top">
        <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <li> </li>
        </ul>
        <?php echo $content_top; ?>
    </div>
    <div class="container">
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-not-found">
                        <div class="image"><img src="/image/not_found.png" class="img-responsive"></div>
                        <div class="text"><?php echo $text_error; ?></div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-back-bt"><i class="icon-right-arrow-2"></i><a href="/" class="link-dashed"><span><?php echo  $camback_home; ?></span></a></div>
                </div>
      </div>
    <?php echo $column_right; ?></div>
</div>
<?php if ($content_bottom) { ?>
    <div class="curier-catalog-info curier-grey">
        <div class="container">
            <?php echo $content_bottom; ?>
        </div>
    </div>
<?php } ?>
<?php echo $footer; ?>