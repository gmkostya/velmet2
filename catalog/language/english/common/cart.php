<?php
// Text
$_['text_items']               = '%s';
$_['text_empty']     = 'Cart is empty!';
$_['text_cart']      = 'View Cart';
$_['text_checkout']  = 'Checkout';
$_['text_recurring'] = 'Payment Profile';