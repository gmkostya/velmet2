function get_timer2() {

    $('.timer').each(function() {
        timetext = $(this).data('timetext');
        timeend = $(this).data('timeend');

        //Объект даты для обратного отсчета
        var date_t =  new Date(timeend);
        //Объект текущей даты
        var date = new Date();
        //Вычисляем сколько миллисекунд пройдет
        //от текущей даты до даты отсчета времени
        var timer = date_t - date;

        //Проверяем не нужно ли закончить отсчет
        //если дата отсчета еще не истекла, то количество
        //миллисекунд в переменной date_t будет больше чем в переменной date
        if(date_t > date) {

            //Вычисляем сколько осталось дней до даты отсчета.
            //Для этого количество миллисекунд до даты отсчета делим
            //на количество миллисекунд в одном дне
            var day = parseInt(timer/(60*60*1000*24));
            //если полученное число меньше 10 прибавляем 0
            if(day < 10) {
                day = '0' + day;
            }
            //Приводим результат к строке
            day = day.toString();

            //Вычисляем сколько осталось часов до даты отсчета.
            //Для этого переменную timer делим на количество
            //миллисекунд в одном часе и отбрасываем дни
            var hour = parseInt(timer/(60*60*1000))%24;
            //если полученное число меньше 10 прибавляем 0
            if(hour < 10) {
                hour = '0' + hour;
            }
            //Приводим результат к строке
            hour = hour.toString();

            //Вычисляем сколько осталось минут до даты отсчета.
            //Для этого переменную timer делим на количество
            //миллисекунд в одной минуте и отбрасываем часы
            var min = parseInt(timer/(1000*60))%60;
            //если полученное число меньше 10 прибавляем 0
            if(min < 10) {
                min = '0' + min;
            }
            //Приводим результат к строке
            min = min.toString();

            //Вычисляем сколько осталось секунд до даты отсчета.
            //Для этого переменную timer делим на количество
            //миллисекунд в одной минуте и отбрасываем минуты
            var sec = parseInt(timer/1000)%60;
            //если полученное число меньше 10 прибавляем 0
            if(sec < 10) {
                sec = '0' + sec;
            }
            //Приводим результат к строке
            sec = sec.toString();
            if(day == 0){
                var timestr='<span class="t-day" >'+day +'</span><span class="t-hour">'+ hour+'</span><span class="t-min">'+min+'</span><span class="t-sec">'+sec+'</span>';
            } else {
                var timestr='<span class="t-day" >'+day +'</span><span class="t-hour">'+ hour+'</span><span class="t-min">'+min+'</span><span class="t-sec">'+sec+'</span>';
            }

            $(this).html('<span class="banner_timer"> '+timestr + '</span>');

        }
        else {
            //$(this).html("<span id='stop'>Отсчет закончен!!!</span>");
            $(this).html("");
        }

    });
    setTimeout(get_timer2,1000);
}
function getURLVar(key) {
    var value = [];
    var query = String(document.location).split('?');
    if (query[1]) {
        var part = query[1].split('&');
        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');
            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }
        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}
function subscribe() {
    var emailpattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var email = $('#txtemail').val();
    if (email != "") {
        $.ajax({
            url: 'index.php?route=module/newsletters/news',
            type: 'post',
            data: 'email=' + $('#txtemail').val(),
            dataType: 'json',
            success: function (json) {
                alert(json.message);
            }
        });
        return false;
    } else {
        $(email).focus();
        return false;
    }
}
function searchResize() {
    var searchWidth = $('#menu').width();
    if ($(window).width() > 1200) {
    }
    $('#search').hover(function () {
        $('.search-input').focus();
    });
};
function homeSlider() {
    if ($('*').is('.home-slider')) {

        if ($(window).width() > 1200 ) {
            $('.home-slider .item, .home-slider .item .slider-container').css('height', $(window).height()-147);
        }

        $('.home-slider').slick({
            dots: false,
            autoplay: true,
            infinite: true,
            arrows: false,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true
        });
       /* $('.home-slider').on('swipe', function (event, slick, direction) {
            console.log('swipe');
        });
        $('.home-slider').on('init', function (event, slick, direction) {
            console.log('init');
        });
        $('.home-slider').on('edge', function (event, slick, direction) {
            console.log('edge');
        });
        $('.home-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            console.log('beforeChange');
        });
        $('.home-slider').on('afterChange', function (event, slick, currentSlide) {
            console.log('afterChange');
        });*/




    }
}
function latestSlider() {
    if ($('*').is('.js-latest-slider')) {
        $('.js-latest-slider').slick({
            dots: true,
            infinite: true,
            autoplay: false,
            arrows: true,
            nextArrow: '<i class="icon-arrow-right arrow-next"></i>',
            prevArrow: '<i class="icon-arrow-left arrow-prev"></i>',
            speed: 300,
            slidesToShow: 4,
            adaptiveHeight: true,
            slidesToScroll: 4,
            responsive: [{
                breakpoint: 1200,
                settings: {slidesToShow: 3, slidesToScroll: 3, infinite: true,}
            }, {breakpoint: 767, settings: {slidesToShow: 2, slidesToScroll: 2}}, {
                breakpoint: 480,
                settings: {slidesToShow: 1, slidesToScroll: 1}
            }]
        });
    }
}
function BestSellerSlider() {
    if ($('*').is('.js-bestseller-slider')) {
        $('.js-bestseller-slider').slick({
            dots: true,
            infinite: true,
            autoplay: false,
            arrows: true,
            nextArrow: '<i class="icon-arrow-right arrow-next"></i>',
            prevArrow: '<i class="icon-arrow-left arrow-prev"></i>',
            speed: 300,
            slidesToShow: 4,
            adaptiveHeight: true,
            slidesToScroll: 4,
            responsive: [{
                breakpoint: 1200,
                settings: {slidesToShow: 3, slidesToScroll: 3, infinite: true,}
            }, {breakpoint: 767, settings: {slidesToShow: 2, slidesToScroll: 2}}, {
                breakpoint: 480,
                settings: {slidesToShow: 1, slidesToScroll: 1}
            }]
        });
    }
}
function RelatedSlider() {
    if ($('*').is('.js-relatet-product')) {
        $('.js-relatet-product').slick({
            dots: true,
            infinite: true,
            autoplay: false,
            arrows: true,
            nextArrow: '<i class="icon-arrow-right arrow-next"></i>',
            prevArrow: '<i class="icon-arrow-left arrow-prev"></i>',
            speed: 300,
            slidesToShow: 4,
            adaptiveHeight: true,
            slidesToScroll: 4,
            responsive: [{
                breakpoint: 1200,
                settings: {slidesToShow: 3, slidesToScroll: 3, infinite: true,}
            }, {breakpoint: 767, settings: {slidesToShow: 2, slidesToScroll: 2}}, {
                breakpoint: 480,
                settings: {slidesToShow: 1, slidesToScroll: 1}
            }]
        });
    }
}
function specialSlider() {
    if ($('*').is('.js-special-slider')) {
        $('.js-special-slider').slick({
            dots: false,
            infinite: true,
            autoplay: false,
            arrows: true,
            nextArrow: '<i class="icon-arrow-right arrow-next"></i>',
            prevArrow: '<i class="icon-arrow-left arrow-prev"></i>',
            speed: 300,
            slidesToShow: 3,
            adaptiveHeight: true,
            slidesToScroll: 3,
            responsive: [{
                breakpoint: 1200,
                settings: {slidesToShow: 3, slidesToScroll: 3, infinite: true,}
            }, {breakpoint: 767, settings: {slidesToShow: 2, slidesToScroll: 2}}, {
                breakpoint: 480,
                settings: {slidesToShow: 1, slidesToScroll: 1}
            }]
        });
    }
}
function CategoryHomeSlider() {
    if ($('*').is('.col-home-category-slider')) {
        $('.col-home-category-slider').slick({
            dots: true,
            infinite: true,
            autoplay: false,
            arrows: true,
            nextArrow: '<i class="icon-arrow-right arrow-next"></i>',
            prevArrow: '<i class="icon-arrow-left arrow-prev"></i>',
            speed: 300,
            slidesToShow: 3,
            adaptiveHeight: true,
            slidesToScroll: 3,
            responsive: [{
                breakpoint: 1200,
                settings: {slidesToShow: 3, slidesToScroll: 3, infinite: true,}
            }, {breakpoint: 767, settings: {slidesToShow: 2, slidesToScroll: 2}}, {
                breakpoint: 480,
                settings: {slidesToShow: 1, slidesToScroll: 1}
            }]
        });
    }
}
function productImageSlider() {
    if ($('*').is('.slider-image-big')) {
        $('.slider-image-big').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            nextArrow: '<i class="icon-arrow-right arrow-next"></i>',
            prevArrow: '<i class="icon-arrow-left arrow-prev"></i>',
            fade: true,
            asNavFor: '.slider-image-additional'
        });
        $('.slider-image-additional').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.slider-image-big',
            dots: false,
            arrows: false,
            centerMode: true,
            focusOnSelect: true,
            centerPadding: '0px',
        });
        if ($(window).width() > 767) {
        $('.slider-image-big').on('afterChange', function (event, slick, currentSlide) {
            $(".slider-image-big .slick-current img").elevateZoom({
                zoomType: "lens",
                lensShape: "square",
                lensSize: 160,
                borderSize: 0
            });
        });
       
            $(".slider-image-big .slick-current img").elevateZoom({
                zoomType: "lens",
                lensShape: "square",
                lensSize: 160,
                borderSize: 0
            });
        }
        $('#lightgallery').lightGallery({
            loop: true,
            fourceAutoply: true,
            thumbnail: true,
            hash: false,
            speed: 400,
            scale: 1,
            keypress: true,
            counter: false,
            download: false,
        });
        $('.click-triger-zoom').on('click', function (e) {
            $('.slider-image-big .slick-current img').data("link");
            $('#' + $('.slider-image-big .slick-current img').data("link")).trigger("click");
        })
    }
}
function instagramSlider() {
    if ($('*').is('.instagram_slider')) {
        $('.instagram_slider').slick({
            dots: false,
            infinite: false,
            speed: 300,
            autoplay: false,
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: true,
            nextArrow: '<i class="icon-right-arrow-1 arrow-next"></i>',
            prevArrow: '<i class="icon-right-arrow-2 arrow-prev"></i>',
            responsive: [{
                breakpoint: 1200,
                settings: {slidesToShow: 4, slidesToScroll: 4, dots: false}
            }, {breakpoint: 767, settings: {slidesToShow: 2, slidesToScroll: 2, dots: false}}, {
                breakpoint: 481,
                settings: {slidesToShow: 1, slidesToScroll: 1,}
            }]
        })
    }
}
function gallerySlider() {
    if ($('*').is('.gallery-slider')) {


        setTimeout(function () {
            var offset = $('.gallery-slider .slick-dots').offset();

            var qdots = $('.gallery-slider .slick-dots > li').length
            if ((window.innerWidth > 1199)) {
                $('.gallery-slider .arrow-prev').css('left', offset.left + 30);
                $('.gallery-slider .arrow-next').css('left', (offset.left + 35 * qdots + 55));
            }

        }, 300)


        $('.gallery-slider').slick({
            dots: true,
            infinite: true,
            autoplay: false,
            arrows: true,
            nextArrow: '<i class="icon-arrow-right arrow-next"></i>',
            prevArrow: '<i class="icon-arrow-left arrow-prev"></i>',
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 3,
            responsive: [{
                breakpoint: 1200,
                settings: {slidesToShow: 4, slidesToScroll: 3, dots: true}
            }, {breakpoint: 767, settings: {slidesToShow: 2, slidesToScroll: 1, dots: false}}, {
                breakpoint: 481,
                settings: {slidesToShow: 1, slidesToScroll: 1, centerMode: false, dots: false}
            }]
        });




    }
}


function newcollectionSlider() {
    if ($('*').is('.js-newcollection-slider')) {
        $('.js-newcollection-slider').on('init', function (event, slick) {
            $(".js-newcollection-slider .slick-active").each(function (index) {
                if (index == 0) {
                    $(this).addClass('big-slide');
                    $(this).prev().addClass('hidden-slide');
                }
                if (index == 2) {
                    $(this).next().addClass('blure-slide');
                }
            });
        });
        $('.js-newcollection-slider').slick({
            dots: false,
            infinite: true,
            autoplay: true,
            arrows: true,
            nextArrow: '<i class="icon-right-arrow-1 arrow-next"></i>',
            prevArrow: '<i class="icon-right-arrow-2 arrow-prev"></i>',
            speed: 300,
            slidesToShow: 4,
            adaptiveHeight: false,
            slidesToScroll: 1,
            centerMode: false,
            variableWidth: false,
            centerPadding: '85px',
            initialSlide: 0,
            responsive: [{
                breakpoint: 1200,
                settings: {slidesToShow: 2, slidesToScroll: 2, infinite: true,}
            }, {breakpoint: 749, settings: {slidesToShow: 1, slidesToScroll: 1}}]
        });
        $('.js-newcollection-slider').on('swipe', function (event, slick, direction) {
        });
        $('.js-newcollection-slider').on('edge', function (event, slick, direction) {
        });
        $('.js-newcollection-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            $('.js-newcollection-slider .slick-slide').removeClass('hidden-slide');
        });
        $('.js-newcollection-slider').on('afterChange', function (event, slick, currentSlide) {
            $(".js-newcollection-slider .slick-active").each(function (index) {
                if (index == 0) {
                    $(this).prev().addClass('hidden-slide');
                }
            });
        });
    }
}
function reviewSlider() {
    if ($('*').is('.js-review-slider')) {
        $('.js-review-slider').slick({
            dots: true,
            infinite: true,
            arrows: true,
            nextArrow: '<i class="icon-right-arrow-1 arrow-next"></i>',
            prevArrow: '<i class="icon-right-arrow-2 arrow-prev"></i>',
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true,
            responsive: [{breakpoint: 1200, settings: {slidesToShow: 1, slidesToScroll: 1, infinite: true,}}]
        });
    }
}
function BlogGallery() {
    if ($('*').is('.js-blog-gallery')) {
        $('.js-blog-gallery ').lightGallery({
            loop: true,
            fourceAutoply: true,
            thumbnail: true,
            hash: false,
            speed: 400,
            scale: 1,
            keypress: true,
            counter: false,
            download: false,
        });

        var wall = new Freewall( ".js-blog-gallery" );
        wall.reset({
            selector: '.brik',
            gutterX: 30,
            gutterY: 30,
            animate: true,
            cellW: 150,
            cellH: 'auto',
            onResize: function() {
                wall.fitWidth();
            }
        });


        wall.fitWidth();
    }
}
function EditMinQuant(product_id) {
    var qty = $('.item-' + product_id).val();
    if ((parseFloat(qty) != parseInt(qty)) || isNaN(qty)) {
        qty = 1;
    } else {
        qty = Number(qty) - 1;
        if (qty < 1) qty = '1';
    }
    $('.item-' + product_id).val(qty);
}
function EditMaxQuant(product_id) {
    var qty = $('.item-' + product_id).val();
    if ((parseFloat(qty) != parseInt(qty)) || isNaN(qty)) {
        qty = 1;
    } else {
        qty = Number(qty) + 1;
    }
    $('.item-' + product_id).val(qty);
}
function get_popup_purchase(product_id) {
    var url = 'index.php?route=module/popup_purchase&product_id=' + product_id;
    if (url.indexOf('#') == 0) {
        $(url).modal('open');
    } else {
        $.get(url, function (data) {
            $('<div class="modal fade modal-buy-one-click" id="buy-one-click" tabindex="-1" role="dialog">' + data + '</div>').modal();
        }).success(function () {
            $('input:text:visible:first').focus();
        });
    }
}
function scrollWidth() {
    var div = $('<div>').css({
        position: "absolute",
        top: "0px",
        left: "0px",
        width: "100px",
        height: "100px",
        visibility: "hidden",
        overflow: "scroll"
    });
    $('body').eq(0).append(div);
    var width = div.get(0).offsetWidth - div.get(0).clientWidth;
    div.remove();
    return width;
}
function masked(element, status) {
    if (status == true) {
        $(element).prepend('<div class="masked"></div><div class="masked_loading" ></div>');
    } else {
        $('.masked').remove();
        $('.masked_loading').remove();
    }
}
function headerResize() {
    var menu = $('.col-menu');
    var menucontainer = $('.menu-container');
    var navbarheader = $('.navbar-header');
    var wrapper = $('.wrapper');
    var usernav = $('.col-user-curier');
    var search = $('#search');
    var colSerach = $('.col-serach');
    var MenuPopupHeader = $('.menu-popup-header');
    var CurierLngCurrency = $('.curier-lng-currency');
    var CurierContactTop = $('.curier-contact-top');
    var ColWelcominner = $('.col-user-item.welcom');
    var ColWelcom = $('.col-welcom');






   /* if (window.innerWidth <= 1199) {
        usernav.after(menu);
        wrapper.after(menucontainer);
    } else {
        menu.after(usernav);
        navbarheader.after(menucontainer);
    }*/
    setTimeout(function () {
        var searchPosition = $('#search').offset().left;
        var logoPosition = $('#logo').offset().left;
        var logoWidth = $('#logo').outerWidth(true);
        if ((window.innerWidth > 767)) {
            widthSearchInput = (searchPosition - logoPosition - logoWidth);
            $('#search .input-curier').css('width', widthSearchInput);
            $('#search .input-curier').css('right', '-' + widthSearchInput);
            console.log(widthSearchInput);
        } else {
            widthSearchInput = (window.innerWidth-30);
            $('#search .input-curier').css('width', widthSearchInput);
            $('#search .input-curier').css('right', '-' + widthSearchInput);
        }
        if (window.innerWidth < 768) {
           // menu.after(colSerach);
            MenuPopupHeader.after(CurierLngCurrency);
            colSerach.after(menu);
            MenuPopupHeader.after(ColWelcominner);
            $('.header-curier').css('opacity',1);
        } else {
            colSerach.after(menu);
            CurierContactTop.after(CurierLngCurrency);
            ColWelcom.html(ColWelcominner);
            $('.header-curier').css('opacity',1);

        }
    }, 100);
}

function CheckouteResize() {
    var ccr = $('.col-customer-radio');
    var sp = $('#simplecheckout_payment');
    sp.before(ccr);

}
function FooterResize() {
    var fpnav = $('.col-product-nav .customer-nav');
    var partner = $('.partner-curier');
    var subscribe = $('.subscribe-curier');
    setTimeout(function () {

        if ((window.innerWidth <= 1199) && (window.innerWidth >= 768)) {
            fpnav.after(partner);
        } else {
            subscribe.after(partner);
        }
    }, 100);

}




function equalheight  (container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

function hasAttr (name) {
    return this.attr(name) !== undefined;
};

$.fn.hasAttr = function(name) {
    return this.attr(name) !== undefined;
};


function callPlayer(frame_id, func, args) {
    if (window.jQuery && frame_id instanceof jQuery) frame_id = frame_id.get(0).id;
    var iframe = document.getElementById(frame_id);
    if (iframe && iframe.tagName.toUpperCase() != 'IFRAME') {
        iframe = iframe.getElementsByTagName('iframe')[0];
    }
    if (iframe) {
		console.log(frame_id);
        // Frame exists, 
        iframe.contentWindow.postMessage(JSON.stringify({
            "event": "command",
            "func": func,
            "args": args || [],
            "id": frame_id
        }), "*");
    }
}





$(document).ready(function () {
    window.onresize = function () {
        headerResize();
        FooterResize();
    }
    get_timer2();

    headerResize();
    gallerySlider();
    searchResize();
    homeSlider();
    latestSlider();
    BestSellerSlider();
    specialSlider();
    newcollectionSlider();
    instagramSlider();
    productImageSlider();
    reviewSlider();
    CategoryHomeSlider();
    RelatedSlider();
    BlogGallery();
    CheckouteResize();
    FooterResize();

    $('.input-qty').on('keyup', function(){
        $(this).val($(this).val().replace (/\D/, ''));
    });



$("iframe").each(function () {
            $(this)[0].contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
        });


$(".slider-down-bt").click(function(){
	$("html,body").animate({scrollTop:$('.container-home-slider').height()+64},"slow")
	})

    setTimeout(function () {
        equalheight($('.col-news-layout .artblock'));
    },300)

    $('.menu-toggle, .btn-close-head-menu ').on('click', function (e) {
        e.preventDefault();
        $('body').toggleClass('menu-open');
        $('.menu-container').toggleClass('menu-container-open');
    });
    $('.ulogin-button-facebook').on('click', function (e) {
        setTimeout(function () {
            $('#modal-quick-login-register').modal('hide')
        },800)
    });
    $('a.dropdown-toggle').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasAttr('href')) {
        if ($(window).width() > 1200) {

            location = $(this).attr('href');
        } else if ($(this).parent('li').hasClass('open')) {
            location = $(this).attr('href');
        }
        }
    });
    $('.mfilter-box h3').on('click', function (e) {
        e.preventDefault();
        if ($(window).width() < 768) {
            $('.mfilter-content').toggle();
            $(this).toggleClass('open');
        }
    });
    $(document).delegate('.js-popup-link', 'click', function (e) {
        $('#' + $(this).data('link')).modal('show');
        console.log($(this).data('link'));
    });
    $('#customer_fax-1').click(function (e) {
        e.stopPropagation();
    });
    $('.item-video').click(function (e) {
		var action = 'playVideo';
		if ($(this).is('.open')) {
		action = 'stopVideo';
			
		} 
        $(this).toggleClass('open');
		var videoid =  $(this).data('videoid');
	
	  	setTimeout(function () {
           callPlayer(videoid,action)
        }, 100);
	
		console.log(action)
		
    });
/*    $('.label-customer_fax-1').on('click', function (e) {
        var thisRadio = $('#customer_fax-1');
        if (thisRadio.is(':checked')) {
            thisRadio.removeAttr('checked');
        } else {
            thisRadio.prop('checked', 'checked');
        }
    });*/
    $('.js-phone-mask').mask('+38 (000) 000-00-00',{placeholder: "+38 (0_ _) _ _ _ - _ _ - _ _"});
    if ($(window).width() > 768) {
        $('.header-curier').affix({offset: {top: 1}});
    } else {
        $('.header-curier').affix({offset: {top: 180}});
    }

    $('.header-curier').on('affix.bs.affix', function () {
        if ( (window.innerWidth >= 768)) {
            $('.container-after-head').css('padding-top', '147px');
        }
    });



    setTimeout(function () {
        $('select').styler();
        if ( (window.innerWidth <= 650)) {
            $('#grid-view').trigger('click');
        }
    }, 100);
    $('.text-danger').each(function () {
        var element = $(this).parent().parent();
        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });
    $('#currency .currency-select').on('click', function (e) {
        e.preventDefault();
        $('#currency input[name=\'code\']').attr('value', $(this).attr('name'));
        $('#currency').submit();
    });
    $('#language a').on('click', function (e) {
        e.preventDefault();
        $('#language input[name=\'code\']').attr('value', $(this).attr('href'));
        $('#language').submit();
    });
    $('#search .search-btn').on('click', function (e) {
        e.preventDefault();
        $('#search  .input-curier').toggleClass('open');
    });
    $(document).on('click', function (e) {

        if ($(e.target).closest("#search").length) return;
        $('#search  .input-curier').removeClass('open');
      //  e.stopPropagation();

    });


    $('#search input[name=\'search\']').parent().find('button').on('click', function () {
        url = $('base').attr('href') + 'index.php?route=product/search';
        var value = $('header input[name=\'search\']').val();
        if (value) {
            url += '&search=' + encodeURIComponent(value);
        }
        location = url;
    });
    $('#search input[name=\'search\']').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $('header input[name=\'search\']').parent().find('button').trigger('click');
        }
    });
    $('#menu .dropdown-menu').each(function () {
        var menu = $('#menu').offset();
        var container = $('header .container').offset();
        var dropdown = $(this).parent().offset();
       // var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());
        var i = (dropdown.left-container.left);
        if (i > 0) {
            $(this).css('margin-left', '-' + i  + 'px');
        }
    });

    $('#list-view').click(function () {

        $('.btn-view').removeClass('active');

        setTimeout(function () {
            $('#list-view').addClass('active');
        }, 100);

        $('.product-layout ').each(function (index) {
            var productlabel = $(this).find('.product-label');
            var caption =  $(this).find('.caption');
            caption.prepend(productlabel);
        });

        $('#content .product-grid > .clearfix').remove();
        $('#content .row .product-box > .product-grid').attr('class', 'product-layout product-list col-xs-12');
        localStorage.setItem('display', 'list');
    });
    $('#grid-view').click(function () {
        $('.btn-view').removeClass('active');

        setTimeout(function () {
            $('#grid-view').addClass('active');
            console.log($(this));
        }, 300);

        $('.product-layout ').each(function (index) {
            var productlabel = $(this).find('.product-label');
            var imagea =  $(this).find('.image a');
            imagea.before(productlabel);
        });


        $('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');

        localStorage.setItem('display', 'grid');
    });
    if (localStorage.getItem('display') == 'list') {
        $('#list-view').trigger('click');
    } else {
        $('#grid-view').trigger('click');
    }
    $(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function (e) {
        if (e.keyCode == 13) {
            $('#collapse-checkout-option #button-login').trigger('click');
        }
    });
    $('[data-toggle=\'tooltip\']').tooltip({container: 'body', trigger: 'hover'});
    $(document).ajaxStop(function () {
        $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
    });
});
var cart = {
    'add': function (product_id, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').addClass('loading');
            },
            complete: function () {
            },
            success: function (json) {
                $('#cart > button').removeClass('loading');
                $('.alert, .text-danger').remove();
                if (json['redirect']) {
                    location = json['redirect'];
                }
                if (json['success']) {
                    $('#cart > button').html('<i class="icon-shopping-purse-icon"></i><span id="cart-total"> ' + json['total'] + '</span>');
                    $('#cart').addClass('cart-full');
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                    $('#cart > button').trigger('click');
                    setTimeout(function () {
                        $('body').trigger('click');
                    }, 3000)
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'update': function (key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);
                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);
                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}
var voucher = {
    'add': function () {
    }, 'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);
                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}
var wishlist = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();
                if (json['redirect']) {
                    location = json['redirect'];
                }
                if (json['success']) {
                   // $('html, body').animate({scrollTop: 0}, 'slow');
                }
                $('#wishlist-total').html('<i class="icon-forma-1-copy-5"></i><span > ' + json['total'] + '</span>');
                $('#wishlist-total').addClass('wishlist-full');


                if($('*').is('#product')) {
                    $('.js-wishlist-'+product_id).addClass('red').removeAttr('title');
                }
                else {
                    $('.js-wishlist-'+product_id).addClass('red').removeAttr('title').html('<i class="icon-forma-1-copy-5"></i>');
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'remove': function () {
    }
}
var compare = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $('#compare-total').html(json['total']);
                    $('html, body').animate({scrollTop: 0}, 'slow');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'remove': function () {
    }
}
$(document).delegate('.agree', 'click', function (e) {
    e.preventDefault();
    $('#modal-agree').remove();
    var element = this;
    $.ajax({
        url: $(element).attr('href'), type: 'get', dataType: 'html', success: function (data) {
            html = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div';
            html += '  </div>';
            html += '</div>';
            $('body').append(html);
            $('#modal-agree').modal('show');
        }
    });
});
(function ($) {
    $.fn.autocomplete = function (option) {
        return this.each(function () {
            this.timer = null;
            this.items = new Array();
            $.extend(this, option);
            $(this).attr('autocomplete', 'off');
            $(this).on('focus', function () {
                this.request();
            });
            $(this).on('blur', function () {
                setTimeout(function (object) {
                    object.hide();
                }, 200, this);
            });
            $(this).on('keydown', function (event) {
                switch (event.keyCode) {
                    case 27:
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });
            this.click = function (event) {
                event.preventDefault();
                value = $(event.target).parent().attr('data-value');
                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }
            this.show = function () {
                var pos = $(this).position();
                $(this).siblings('ul.dropdown-menu').css({top: pos.top + $(this).outerHeight(), left: pos.left});
                $(this).siblings('ul.dropdown-menu').show();
            }
            this.hide = function () {
                $(this).siblings('ul.dropdown-menu').hide();
            }
            this.request = function () {
                clearTimeout(this.timer);
                this.timer = setTimeout(function (object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }
            this.response = function (json) {
                html = '';
                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }
                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }
                    var category = new Array();
                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }
                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }
                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';
                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }
                if (html) {
                    this.show();
                } else {
                    this.hide();
                }
                $(this).siblings('ul.dropdown-menu').html(html);
            }
            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));
        });
    }
})(window.jQuery);

  


