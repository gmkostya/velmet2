<table class="table">
    <tr>
		<td class="col-xs-3" style="vertical-align:top;">
        	<h5><strong>Picture Width & Height:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;In Pixels</span>
        </td>
		<td class="col-xs-9">
        	<div class="col-xs-4">
                <div class="input-group">
                  <span class="input-group-addon">Width:&nbsp;</span>
					<input type="text" name="<?php echo $moduleName; ?>[ViewWidth]" class="form-control" value="<?php echo (isset($moduleData['ViewWidth'])) ? $moduleData['ViewWidth'] : '80' ?>" />
				  <span class="input-group-addon">px</span>
                </div>
                <br />
                <div class="input-group">
                  <span class="input-group-addon">Height:</span>
					<input type="text" name="<?php echo $moduleName; ?>[ViewHeight]" class="form-control" value="<?php echo (isset($moduleData['ViewHeight'])) ? $moduleData['ViewHeight'] : '80' ?>" />
                  <span class="input-group-addon">px</span>
                </div>
            </div>
		</td>
  </tr>
  <tr>
       <td class="col-xs-3" style="vertical-align:top;">
			<h5><strong>Custom CSS:</strong></h5>
       		<span class="help"><i class="fa fa-info-circle"></i>&nbsp;Place your custom CSS here.</span> 
       </td>
       <td class="col-xs-9">
			<div class="col-xs-4">
				<textarea rows="5" name="<?php echo $moduleName; ?>[ViewCustomCSS]" placeholder="Custom CSS" class="form-control"><?php echo (isset($moduleData['ViewCustomCSS'])) ? $moduleData['ViewCustomCSS'] : '' ?></textarea>
            </div>
       </td>
  </tr>
</table>