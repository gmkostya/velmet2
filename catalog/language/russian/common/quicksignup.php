<?php
// Text
$_['text_signin_register']    = 'Аторизация/Регистрация';
$_['text_login']   			  = 'Аторизация';
$_['text_register']    		  = 'Регистрация';

$_['text_new_customer']       = 'Новый клиент';
$_['text_returning']          = 'Авторизация';
$_['text_returning_customer'] = 'Я возвращающийся клиент';
$_['text_details']            = 'Ваши персональные данные';
$_['entry_email']             = 'Email';
$_['entry_name']              = 'Имя';
$_['entry_password']          = 'Пароль';
$_['entry_telephone']         = 'Телефон';
$_['text_forgotten']          = 'Забыли пароль?';
$_['text_agree']              = 'Я принимаю  <a href="%s" class="agree"><b>%s</b></a>';



//Button
$_['button_login']            = 'Вход';

//Error
$_['error_name']           = 'Кол-во символов в  поле  должно быть от 1 до 32!';
$_['error_lastname']           = 'Кол-во  символов в  поле  должно быть от 1 до 32!';
$_['error_email']          = 'E-Mail указан некорректно';
$_['error_telephone']      = 'Кол-во  символов в  поле  должно быть от 3 до 32!';
$_['error_password']       = 'Кол-во  символов в  поле  должно быть от 4 до 20!';
$_['error_exists']         = 'Внимание: E-Mail уже зарегистрирован!';
$_['error_agree']          = 'Внимание: Вы должны принять %s!';
$_['error_warning']        = 'Внимание: Проверте корректность данных внесеных в форму!';
$_['error_approved']       = 'Внимание: Ваша учетная запись требует одобрения, прежде чем вы сможете войти в систему.';
$_['error_login']          = 'Внимание: Нет соответствия для E-Mail Address и / или пароля.';
