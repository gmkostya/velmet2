<?php echo $header; ?>
  <div class="container container-top">
    <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
      <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <?php if ($i + 1 < count($breadcrumbs)) { ?>
            <a itemscope itemtype="http://schema.org/Thing" itemprop="item"
               href="<?php echo $breadcrumb['href']; ?>">
              <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
          <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
          <?php } ?>
        </li>
      <?php } ?>
    </ul>
    <?php echo $content_top; ?>

  </div>

  <div class="container">
      <?php if ($success) { ?>
          <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
              <button type="button" class="close" data-dismiss="alert">&times;</button>
          </div>
      <?php } ?>

    <div class="row">
      <?php if ($column_left) { ?>
        <?php $class = 'col-sm-12'; ?>
      <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
      <?php } ?>
      <div id="content" class="<?php echo $class; ?>">
          <h1><?php echo $heading_title; ?></h1>
      <?php if ($products) { ?>


          <?php foreach ($products as $product) { ?>
              <div class="product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="product-thumb transition">
                      <div class="image">
                          <?php if ($product['label']) { ?>
                              <div class="product-label <?php echo  $product['label_type']; ?>"><?php echo $product['label']; ?></div>
                              <?php if ($product['count_end_date']) { ?>
                                  <div class="timer-curier">
                                      <div class="timer" data-timeend="<?php echo $product['count_end_date']; ?>" data-timetext="<?php echo $product['name']; ?>"></div>
                                      <div class="timer-text" ><span class="t-day" >дней</span><span class="t-hour">часов</span><span class="t-min">мин</span><span class="t-sec">сек</span></div>
                                  </div>
                              <?php } ?>
                          <?php } ?>
                          <a href="<?php echo $product['href']; ?>">
                              <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                          </a>
                      </div>
                      <div class="caption">
                          <div class="rating">
                              <?php if ($product['rating']) { ?>
                                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                                      <?php if ($product['rating'] < $i) { ?>

                                      <?php } else { ?>
                                          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                      <?php } ?>
                                  <?php } ?>
                              <?php } ?>
                          </div>
                          <div class="name" ><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                      </div>
                      <div class="button-group">
                          <?php if ($product['price']) { ?>
                              <div class="price">
                                  <?php if ($product['special']) { ?>
                                      <span class="price-old"><?php echo $product['price']; ?></span>
                                      <span class="price-new"><?php echo $product['special']; ?></span>
                                  <?php } else { ?>
                                      <?php echo $product['price']; ?>
                                  <?php } ?>
                                  <?php if ($product['tax']) { ?>
                                      <span
                                              class="price-tax"><?php echo $text_tax; ?><?php echo $product['tax']; ?></span>
                                  <?php } ?>
                              </div>
                          <?php } ?>

                          <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="product-layout-button-cart"><i class="icon-cart"></i><span><?php echo $button_cart; ?></span></button><br/>
                          <button type="button" data-toggle="tooltip"
                                  title="<?php echo $remove_wishlist; ?>"
                                  onclick="location.href='<?php echo $product['remove']; ?>'"
                                  class="product-layout-button-wishlist"><i class="icon-close"></i>
                          </button>
                      </div>

                  </div>
              </div>
          <?php } ?>



      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
          <div class="buttons clearfix">
              <div class="pull-right"><a href="/" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
          </div>
      <?php } ?>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
