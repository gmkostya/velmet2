<table class="table">
    <tr>
		<td class="col-xs-3" style="vertical-align:top;">
        	<h5><strong>Title:</strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;This will be the title of the widget.</span>
        </td>
        <td class="col-xs-9">
            <div class="col-xs-4">
                <?php foreach ($languages as $language) { ?>
                    <div class="input-group">
                      <span class="input-group-addon"><img src="<?php echo $language['flag_url']; ?>" style="margin-top:-3px;" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></span>
                      <input name="<?php echo $moduleName; ?>[WidgetTitle][<?php echo $language['language_id']; ?>]" class="form-control" type="text" value="<?php echo (isset($moduleData['WidgetTitle'][$language['language_id']])) ? $moduleData['WidgetTitle'][$language['language_id']] : 'Check out this bundle:' ?>" />
                    </div>
                    <br />
				<?php } ?>
			</div>
       </td>
    </tr>
    <tr>
		<td class="col-xs-3">
        	<h5><strong>Wrap in Widget:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Design may vary depending on the design on the template.</span>
        </td>
        <td class="col-xs-9">
			<div class="col-xs-4">
                <select name="<?php echo $moduleName; ?>[WrapInWidget]" class="form-control">
                    <option value="yes" <?php echo (isset($moduleData['WrapInWidget']) && ($moduleData['WrapInWidget'] == 'yes')) ? 'selected=selected' : '' ?>>Enabled</option>
					<option value="no" <?php echo (isset($moduleData['WrapInWidget']) && ($moduleData['WrapInWidget'] == 'no')) ? 'selected=selected' : '' ?>>Disabled</option>
                </select>
            </div>
       </td>
    </tr>
    <tr>
		<td class="col-xs-3">
        	<h5><strong>Bundles in Widget:</strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;Define how many bundles to be shown on the widget.</span>
        </td>
        <td class="col-xs-9">
            <div class="col-xs-4">
				<input type="number" name="<?php echo $moduleName; ?>[WidgetLimit]" class="form-control" value="<?php echo (isset($moduleData['WidgetLimit'])) ? $moduleData['WidgetLimit'] : '2' ?>" /> 
			</div>
       </td>
    </tr>
    <tr>
		<td class="col-xs-3">
        	<h5><strong>Display Type:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Choose how the bundles should be displayed.
            <br /><br />- Default: Shows the bundles chronologically. 
            <br />- Random: Shows the bundles randomly.            
            </span>
        </td>
        <td class="col-xs-9">
			<div class="col-xs-4">
                <select name="<?php echo $moduleName; ?>[DisplayType]" class="form-control">
                    <option value="default" <?php echo (isset($moduleData['DisplayType']) && ($moduleData['DisplayType'] == 'default')) ? 'selected=selected' : '' ?>>Default</option>
					<option value="random" <?php echo (isset($moduleData['DisplayType']) && ($moduleData['DisplayType'] == 'random')) ? 'selected=selected' : '' ?>>Random</option>
                </select>
            </div>
       </td>
    </tr>
    <tr>
		<td class="col-xs-3" style="vertical-align:top;">
        	<h5><strong>Picture Width & Height:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;In Pixels</span>
        </td>
		<td class="col-xs-9">
        	<div class="col-xs-4">
                <div class="input-group">
                  <span class="input-group-addon">Width:&nbsp;</span>
					<input type="text" name="<?php echo $moduleName; ?>[WidgetWidth]" class="form-control" value="<?php echo (isset($moduleData['WidgetWidth'])) ? $moduleData['WidgetWidth'] : '80' ?>" />
				  <span class="input-group-addon">px</span>
                </div>
                <br />
                <div class="input-group">
                  <span class="input-group-addon">Height:</span>
					<input type="text" name="<?php echo $moduleName; ?>[WidgetHeight]" class="form-control" value="<?php echo (isset($moduleData['WidgetHeight'])) ? $moduleData['WidgetHeight'] : '80' ?>" />
                  <span class="input-group-addon">px</span>
                </div>
            </div>
		</td>
  </tr>
  <tr>
       <td class="col-xs-3" style="vertical-align:top;">
			<h5><strong>Custom CSS:</strong></h5>
       		<span class="help"><i class="fa fa-info-circle"></i>&nbsp;Place your custom CSS here.</span> 
       </td>
       <td class="col-xs-9">
			<div class="col-xs-4">
				<textarea rows="5" name="<?php echo $moduleName; ?>[CustomCSS]" placeholder="Custom CSS" class="form-control"><?php echo (isset($moduleData['CustomCSS'])) ? $moduleData['CustomCSS'] : '' ?></textarea>
            </div>
       </td>
  </tr>
</table>