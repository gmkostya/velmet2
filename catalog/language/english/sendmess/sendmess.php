<?php
// Text
$_['text_name']  			     = 'Name:';
$_['text_email']  				 = 'E-mail:';
$_['text_message'] 				 = 'Message:';
$_['text_phone']				 = 'Phone:';

$_['error_name']   				 = 'The name must be between 3 and 25 characters!';
$_['error_email']   			 = 'E-mail is required!';
$_['error_message'] 			 = 'Message must be between 10 characters!';
$_['error_phone'] 			 	 = 'Phone must be from 5 characters!';
$_['success']      				 = 'Your request was successfully sent. We will contact you shortly!';

$_['email_subject_message'] 	 = 'A new message';
$_['email_subject_callback'] 	 = 'New call order';
$_['email_subject_opt'] 	 = 'Inquiry for wholesaler';
