<?php
class ControllerModuleRecentview extends Controller {
	public function index() {
		$this->load->language('module/recentview');
		$this->load->model("setting/setting");
		$this->load->model('catalog/product');
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
         $data['base'] = $this->config->get('config_ssl');
		} else {
         $data['base'] = $this->config->get('config_url');
		}
		$data['heading_title'] = $this->language->get('heading_title');
		
		$config = $this->model_setting_setting->getSetting('recentview');
		$data['number'] = $config['recentview_number'];
		if(isset($this->session->data['recentview'])){
			
			$recentview = array_reverse($this->session->data['recentview']);
			
			$this->load->model('tool/image');
			
			foreach($recentview as $product){
				$product_info = $this->model_catalog_product->getProduct($product);
				$data['products'][] = array('href'=>$this->url->link('product/product','&product_id=' . $product),'img'=> $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')), 'name'=>$product_info['name']);
			}
			
			
			if($config['recentview_status'] == 1){
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/recentview.tpl')) {
					return $this->load->view($this->config->get('config_template') . '/template/module/recentview.tpl', $data);
				} else {
					return $this->load->view('default/template/module/recentview.tpl', $data);
				}
			}
		}
	}
}