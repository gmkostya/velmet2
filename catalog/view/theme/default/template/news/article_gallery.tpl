<div class="article-content">
   <?php $clear =  strip_tags($description); if (!empty($clear)) { ?>
    <div class="description"><?php echo $description; ?></div>
    <?php } ?>
    <?php if ($gallery_array) { ?>
        <div class="content js-blog-gallery">
                     <?php foreach ($gallery_array as $item) { ?>
					<?php if ($item['type']=='v')  { ?>
                           <?php if (0) { ?>
                             <div class="brik brik-video" >
                             <a class="item-gallery item-gallery-v" data-src="<?php echo $item['code2']; ?>"   title="<?php echo $item['text']; ?>">
							<?php echo $item['code']; ?>
						</a>
                             </div>
                             <?php } ?>
                             <a data-src="<?php echo $item['code2']; ?>" class="item-gallery item-gallery-v" >
                                 <span class="brik brik-video">
                                 <?php echo $item['code']; ?>
                                     </span>
                             </a>
					<?php } else { ?>
						<a data-src="<?php echo $item['thumb']; ?>" class="item-gallery item-gallery-i"    title="<?php echo $item['text']; ?>">
							<img src="<?php echo $item['thumb']; ?>" alt="<?php echo $item['text']; ?>" class="brik" />
						</a>
					<?php } ?>
            <?php } ?>
        </div>
    <?php } ?>
	<?php if ($ntags && count($ntags) > 1) { ?>
		<div class="article-tags">
			<?php echo $text_tags; ?> 
			<?php foreach($ntags as $ntag) { ?>
				<a class="ntag" href="<?php echo $ntag['href']; ?>"><?php echo $ntag['ntag']; ?></a>
			<?php } ?>
		</div>
	<?php } ?>
</div>
