<?php echo $header; ?>
<div class="container container-top">
    <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <?php if ($i + 1 < count($breadcrumbs)) { ?>
                    <a itemscope itemtype="http://schema.org/Thing" itemprop="item"
                       href="<?php echo $breadcrumb['href']; ?>">
                        <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <?php echo $column_right; ?>
</div>

    <div class="container">

        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                    <h1><?php echo $heading_title; ?></h1>
      <label class="control-label" for="input-search"><?php echo $entry_search; ?></label>
      <div class="row">
        <div class="col-sm-4 search-page-fields">
          <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />
        </div>
        <div class="col-sm-3  search-page-fields">
          <select name="category_id" class="form-control select-default">
            <option value="0"><?php echo $text_category; ?></option>
            <?php foreach ($categories as $category_1) { ?>
            <?php if ($category_1['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <?php if ($category_2['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_2['children'] as $category_3) { ?>
            <?php if ($category_3['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="col-sm-12  search-page-fields">

            <?php if ($sub_category) { ?>
            <input type="checkbox" name="sub_category" id="subcategory"  class="styled" value="1" checked="checked" />
            <?php } else { ?>
            <input type="checkbox" name="sub_category" id="subcategory" class="styled" value="1" />
            <?php } ?>
            <label class="checkbox-inline" for="subcategory"><?php echo $text_sub_category; ?></label>
        </div>
      </div>
      <p class=" search-page-fields">

          <?php if ($description) { ?>
          <input type="checkbox" name="description" value="1" class="styled" id="description" checked="checked" />
          <?php } else { ?>
          <input type="checkbox" name="description" value="1" class="styled" id="description" />
          <?php } ?>
          <label class="checkbox-inline" for="description"><?php echo $entry_description; ?></label>
      </p>
      <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary" />
      <h2><?php echo $text_search; ?></h2>
                <?php if ($products) { ?>
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="col-sort-limits">
                                <div class="col-sort-label">
                                    <div class="col-label">
                                        <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                                    </div>
                                    <div class="col-sort">
                                        <select id="input-sort" class="form-control" onchange="location = this.value;">
                                            <?php foreach ($sorts as $sorts) { ?>
                                                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                                    <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-list-grid">
                                    <button type="button" id="list-view" class="btn btn-view" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button><button type="button" id="grid-view" class="btn btn-view" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
                                </div>
                                <div class="col-limits-label">
                                    <div class="col-label">
                                        <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                                    </div>
                                    <div class="col-limits">
                                        <select id="input-limit" class="form-control" onchange="location = this.value;">
                                            <?php foreach ($limits as $limits) { ?>
                                                <?php if ($limits['value'] == $limit) { ?>
                                                    <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 product-box-curier">
                            <div class="product-box <?php if (count($products) < 3 ) { ?> product-box-limmit <?php } ?>">
                                <?php foreach ($products as $product) { ?>
                                    <div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="product-thumb transition">
                                            <div class="image">
                                                <?php if ($product['label']) { ?>
                                                    <div class="product-label <?php echo  $product['label_type']; ?>"><?php echo $product['label']; ?></div>
                                                    <?php if ($product['count_end_date']) { ?>
                                                        <div class="timer-curier">
                                                            <div class="timer" data-timeend="<?php echo $product['count_end_date']; ?>" data-timetext="<?php echo $product['name']; ?>"></div>
                                                            <div class="timer-text" ><span class="t-day" >дней</span><span class="t-hour">часов</span><span class="t-min">мин</span><span class="t-sec">сек</span></div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                                <a href="<?php echo $product['href']; ?>">
                                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                                                </a>
                                            </div>
                                            <div class="caption">
                                                <div class="rating">
                                                    <?php if ($product['rating']) { ?>
                                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                            <?php if ($product['rating'] < $i) { ?>

                                                            <?php } else { ?>
                                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                                <div class="name" ><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                                            </div>
                                            <div class="button-group">
                                                <?php if ($product['price']) { ?>
                                                    <div class="price">
                                                        <?php if ($product['special']) { ?>
                                                            <span class="price-old"><?php echo $product['price']; ?></span>
                                                            <span class="price-new"><?php echo $product['special']; ?></span>
                                                        <?php } else { ?>
                                                            <?php echo $product['price']; ?>
                                                        <?php } ?>
                                                        <?php if ($product['tax']) { ?>
                                                            <span
                                                                class="price-tax"><?php echo $text_tax; ?><?php echo $product['tax']; ?></span>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>

                                                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="product-layout-button-cart"><i class="icon-cart"></i><span><?php echo $button_cart; ?></span></button>
                                                <?php if ($product['wishlist_status']){ ?>
                                                    <button type="button" data-toggle="tooltip"
                                                            title="<?php echo $button_wishlist_is; ?>"
                                                            onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                                            class="product-layout-button-wishlist red js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-forma-1-copy-5"></i>
                                                    </button>
                                                <?php } else { ?>
                                                    <button type="button" data-toggle="tooltip"
                                                            title="<?php echo $button_wishlist; ?>"
                                                            onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                                            class="product-layout-button-wishlist js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-forma-1-copy-5"></i>
                                                    </button>
                                                <?php } ?>
                                            </div>

                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center"><?php echo $pagination; ?></div>
                    </div>
                <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>
