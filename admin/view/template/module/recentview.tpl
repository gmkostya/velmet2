<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="recentview_status" id="input-status" class="form-control">
                <?php if ($recentview_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><span data-toggle="tooltip" data-html="true" data-trigger="click" title="" data-original-title="Number of Recent visited Products to show"><?php echo $recent_number; ?></span></label>
            <div class="col-sm-10">
              <input type="number" name="recentview_number" class="form-control" min="1" value="<?php if($recentview_number){ echo $recentview_number; }else{ echo "2"; } ?>">
            </div>
          </div>
        </form>
      </div>
    </div>
	<div class="panel panel-default">
		 <div class="panel-heading">
			<h3 class="panel-title">Buy Proversion </h3><a style="margin-top:-7px;" class="pull-right btn btn-primary" href="http://plugins.makeshop.in/opencart-plugins/Recently-Viewed-Product-For-Opencart2"><strong>Click to Buy</strong></a>
		 </div>
		 <div class="panel-body">
			<h2>Buy a pro version of this plugin for $9 and get more Features</h2>
			<ul style="margin-top:20px;">
				<li><h4>Show unlimited number of Recently Viewed Product</h4></li>
				<li><h4>More Position for the Recent View Icon</h4></li>
				<li><h4>and more..</h4></li>
			</ul>
			<a href="http://plugins.makeshop.in/opencart-plugins/Recently-Viewed-Product-For-Opencart2"><h4 style="width:100%" class="btn btn-primary">BUY NOW</h4></a>
		 </div>
	</div>
	
  </div>
</div>
<?php echo $footer; ?>