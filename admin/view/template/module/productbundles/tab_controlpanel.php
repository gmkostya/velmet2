<table class="table">
  <tr>
    <td class="col-xs-3">
    	<h5><span class="required">*</span> <strong>ProductBundles status:</strong></h5>
    	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;Enable or disable ProductBundles.</span>
    </td>
    <td class="col-xs-9">
		<div class="col-xs-4">
            <select id="Checker" name="<?php echo $moduleName; ?>[Enabled]" class="form-control">
                  <option value="yes" <?php echo (!empty($moduleData['Enabled']) && $moduleData['Enabled'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                  <option value="no"  <?php echo (empty($moduleData['Enabled']) || $moduleData['Enabled']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
            </select>
        </div>
   </td>
  </tr>
  <tr>
    <td class="col-xs-3">
        <h5><strong>If a given bundle is added more than once in the cart:</strong></h5>
        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;The discount can be added only once on every time depending on the setting.</span> 
    </td>
    <td class="col-xs-9">
        <div class="col-xs-4">
			<select name="<?php echo $moduleName; ?>[MultipleBundles]" class="form-control">
                  <option value="no"  <?php echo (empty($moduleData['MultipleBundles']) || $moduleData['MultipleBundles']== 'no') ? 'selected=selected' : '' ?>>Add the discount once</option>
                  <option value="yes" <?php echo (!empty($moduleData['MultipleBundles']) && $moduleData['MultipleBundles'] == 'yes') ? 'selected=selected' : '' ?>>Add the discount every time</option>
            </select>
        </div>
   </td>
 </tr>
 <tr>
    <td class="col-xs-3">
        <h5><span class="required">*</span> <strong>TAX the discounted value from the bundles:</strong></h5>
        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Enable if you want the Bundle Total to be shown with applied Taxes.</span> 
    </td>
    <td class="col-xs-9">
        <div class="col-xs-4">
			<select name="<?php echo $moduleName; ?>[DiscountTaxation]" class="form-control">
                  <option value="no"  <?php echo (empty($moduleData['DiscountTaxation']) || $moduleData['DiscountTaxation']== 'no') ? 'selected=selected' : '' ?>>Disabled</option>
                  <option value="yes" <?php echo (!empty($moduleData['DiscountTaxation']) && $moduleData['DiscountTaxation'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
            </select>
        </div>
   </td>
 </tr>
</table>