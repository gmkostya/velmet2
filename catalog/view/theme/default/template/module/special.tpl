<div class="product-box product-box-slider product-box-slider-special">
<div class="block-title"><span><?php echo $heading_title; ?></span><a href="<?php echo $special; ?>"><span><?php echo $text_see_all; ?></span></a></div>
    <div class="block-title-mob"><a href="<?php echo $special; ?>"><span><?php echo $heading_title; ?></span><i class="icon-right-arrow-1"></i></a></div>
    <div class="js-special-slider">
    <?php foreach ($products as $product) { ?>
        <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-thumb transition">
                <div class="image">
                    <?php if ($product['label']) { ?>
                        <div class="product-label"><?php echo $product['label']; ?></div>
                    <?php } ?>
                    <a href="<?php echo $product['href']; ?>">
                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                    </a>
                    <div class="button-group">
                        <?php foreach ($product['options'] as $options ) {  if ($options['option_id']=='9') { ?>
                            <div class="option-sizes">
                                <?php foreach ($options['product_option_value'] as $value ) { ?>
                                    <span class="option-size"><?php echo $value['name']; ?></span>
                                <?php }  ?>
                            </div>
                        <?php } } ?>
                        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="product-layout-button-cart"><i class="icon-cart"></i><span><?php echo $button_cart; ?></span></button>
                        <?php if ($product['wishlist_status']){ ?>
                            <button type="button" data-toggle="tooltip"
                                    title="<?php echo $button_wishlist_is; ?>"
                                    onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                    class="product-layout-button-wishlist red js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-heart_2"></i>
                            </button>
                        <?php } else { ?>
                            <button type="button" data-toggle="tooltip"
                                    title="<?php echo $button_wishlist; ?>"
                                    onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                    class="product-layout-button-wishlist js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-uniF08B"></i>
                            </button>
                        <?php } ?>
                    </div>
                </div>
                <div class="caption">
                    <div class="name" ><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                    <?php if ($product['price']) { ?>
                        <div class="price">
                            <?php if ($product['special']) { ?>
                                <span class="price-old"><?php echo $product['price']; ?></span>
                                <span class="price-new"><?php echo $product['special']; ?></span>
                            <?php } else { ?>
                                <?php echo $product['price']; ?>
                            <?php } ?>
                            <?php if ($product['tax']) { ?>
                                <span
                                        class="price-tax"><?php echo $text_tax; ?><?php echo $product['tax']; ?></span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
</div>
