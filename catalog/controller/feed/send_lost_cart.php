<?php

class ControllerFeedSendLostCart extends Controller
{
    private $error = array();

    public function sendfirst()
    {
         $this->load->language('feed/send_lost_cart');
         $this->load->model('feed/send_lost_cart');

        /**кількість записів за раз, умова по даті старше поточна  + interval(дні)*/
        $filter_data =  array(
            'limit' => '10',
            'interval' => '3'
        );

        $lostcart = array();
        $lostcustomers = $this->model_feed_send_lost_cart->getNotSendCustomers($filter_data);
        foreach ($lostcustomers as $customer_id => $customer_info) {
            //$lostcustomers = $this->model_feed_send_lost_cart->getNotSendCartsByCustomerId($lostcustomer['customer_id']);
            $lostproducts = $this->model_feed_send_lost_cart->getProducts($customer_id);
           // var_dump($customer_info);
           // var_dump($lostproducts);

            /**prepear mail data*/
            $data['customer_email'] = $customer_info['email'];
            $data['customer_name'] = $customer_info['firstname'].' '.$customer_info['lastname'];
            $data['store_url'] = $this->url->link('common/home', '', 'SSL');
            $data['login_url'] = $this->url->link('account/login', '', 'SSL');
            $data['store_name'] = $this->config->get('config_name');
            $data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');

            $data['text_product'] = $this->language->get('text_new_product');
            $data['text_model'] = $this->language->get('text_new_model');
            $data['text_quantity'] = $this->language->get('text_new_quantity');
            $data['text_price'] = $this->language->get('text_new_price');
            $data['text_total'] = $this->language->get('text_new_total');
            $data['text_footer'] = $this->language->get('text_new_footer');

            $subject = $this->language->get('text_subject_lost_cart');


            $data['text_lost_up'] = sprintf($this->language->get('text_lost_up'),$data['customer_name'],$data['store_url'],  $data['store_name'] );
            $data['text_lost_footer'] = sprintf($this->language->get('text_lost_footer'),$data['login_url']);

            foreach ($lostproducts as $product) {
                $option_data = array();


                foreach ($product['option'] as $option) {
                  //var_dump($option);
                    $option_data[] = array(
                        'name' => $option['name'],
                        'value' => $option['value']
                    );
                }

                $data['products'][] = array(
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'option' => $option_data,
                    'quantity' => $product['quantity'],
                    'price' => $this->currency->format($product['price'], $this->config->get('config_currency')),
                    'total' => $this->currency->format($product['total'], $this->config->get('config_currency'))
                );
            }


            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/lostcart1.tpl')) {
                $html = $this->load->view($this->config->get('config_template') . '/template/mail/lostcart1.tpl', $data);
            } else {
                $html = $this->load->view('default/template/mail/lostcart1.tpl', $data);
            }

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($data['customer_email']);
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender(html_entity_decode($data['store_name'], ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
            $mail->setHtml($html);
            //$mail->setText($text);
            $mail->send();


            $this->model_feed_send_lost_cart->SetFirstMailSend($customer_id);

        }

    }
    public function sendsecond()
    {
         $this->load->language('feed/send_lost_cart');
         $this->load->model('feed/send_lost_cart');

        /**кількість записів за раз, умова по даті старше поточна  + interval(дні)*/
        $filter_data =  array(
            'limit' => '10',
            'interval' => '7'
        );

        $lostcart = array();
        $lostcustomers = $this->model_feed_send_lost_cart->getNotSendCustomers2($filter_data);
        foreach ($lostcustomers as $customer_id => $customer_info) {
            //$lostcustomers = $this->model_feed_send_lost_cart->getNotSendCartsByCustomerId($lostcustomer['customer_id']);
            $lostproducts = $this->model_feed_send_lost_cart->getProducts($customer_id);
            //var_dump($customer_info);
           // var_dump($lostproducts);

            /**prepear mail data*/
            $data['customer_email'] = $customer_info['email'];
            $data['customer_name'] = $customer_info['firstname'].' '.$customer_info['lastname'];
            $data['store_url'] = $this->url->link('common/home', '', 'SSL');
            $data['login_url'] = $this->url->link('account/login', '', 'SSL');
            $data['store_name'] = $this->config->get('config_name');
            $data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');

            $data['text_product'] = $this->language->get('text_new_product');
            $data['text_model'] = $this->language->get('text_new_model');
            $data['text_quantity'] = $this->language->get('text_new_quantity');
            $data['text_price'] = $this->language->get('text_new_price');
            $data['text_total'] = $this->language->get('text_new_total');
            $data['text_footer'] = $this->language->get('text_new_footer');

            $subject = $this->language->get('text_subject_lost_cart');


            $data['text_lost_up'] = sprintf($this->language->get('text_lost_up'),$data['customer_name'],$data['store_url'],  $data['store_name'] );
            $data['text_lost_footer'] = sprintf($this->language->get('text_lost_footer'),$data['login_url']);

            foreach ($lostproducts as $product) {
                $option_data = array();


                foreach ($product['option'] as $option) {
                  //var_dump($option);
                    $option_data[] = array(
                        'name' => $option['name'],
                        'value' => $option['value']
                    );
                }

                $data['products'][] = array(
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'option' => $option_data,
                    'quantity' => $product['quantity'],
                    'price' => $this->currency->format($product['price'], $this->config->get('config_currency')),
                    'total' => $this->currency->format($product['total'], $this->config->get('config_currency'))
                );
            }


            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/lostcart1.tpl')) {
                $html = $this->load->view($this->config->get('config_template') . '/template/mail/lostcart1.tpl', $data);
            } else {
                $html = $this->load->view('default/template/mail/lostcart1.tpl', $data);
            }

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($data['customer_email']);
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender(html_entity_decode($data['store_name'], ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
            $mail->setHtml($html);
            //$mail->setText($text);
            $mail->send();


            $this->model_feed_send_lost_cart->SetSecondMailSend($customer_id);

        }

    }




    public function send()
    {
        $json = array();

        $this->load->language('sendmess/sendmess');

        $data['text_name'] = $this->language->get('text_name');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_message'] = $this->language->get('text_message');
        $data['text_phone'] = $this->language->get('text_phone');
        $data['text_type'] = $this->language->get('text_type');
        $data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $data['text_model'] = $this->language->get('text_model');

        $data['error_name'] = $this->language->get('error_name');
        $data['error_email'] = $this->language->get('error_email');
        $data['error_phone'] = $this->language->get('error_phone');
        $data['error_message'] = $this->language->get('error_message');
        $data['success'] = $this->language->get('success');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            if (isset($this->request->post['goal'])) {
                if ($this->request->post['goal'] == 'send_message'):
                    if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
                        $json['error'] = true;
                        $json['error_name'] = $data['error_name'];
                    }
                    if ((utf8_strlen($this->request->post['email']) < 5)) {
                        $json['error'] = true;
                        $json['error_email'] = $data['error_email'];
                    }
                    if ((utf8_strlen($this->request->post['message']) < 10)) {
                        $json['error'] = true;
                        $json['error_message'] = $data['error_message'];
                    }
                    if (!isset($json['error'])):
                        $mail = new Mail();
                        $mail->protocol = $this->config->get('config_mail_protocol');
                        $mail->parameter = $this->config->get('config_mail_parameter');
                        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
                        $mail->setTo($this->config->get('config_email'));
                        $mail->setFrom($this->config->get('config_name'));
                        $mail->setSender(html_entity_decode($this->request->post['name'], ENT_QUOTES, 'UTF-8'));
                        $mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject_message'), $this->request->post['name']), ENT_QUOTES, 'UTF-8'));
                        $html = '<table style="width: 100%; background-color: #f1f1f1;">
                                    <tbody>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">' . $data['text_name'] . '</td><td>' . $this->request->post['name'] . '</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">' . $data['text_email'] . '</td><td>' . $this->request->post['email'] . '</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">' . $data['text_message'] . '</td><td>' . $this->request->post['message'] . '</td>
                                        </tr>
                                    </tbody>
                                </table>';
                        $mail->setHtml($html);
                        $mail->send();
                        $json['success'] = $data['success'];
                    endif;
                endif;
                if ($this->request->post['goal'] == 'callback_request'):
                    if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
                        $json['error'] = true;
                        $json['error_name'] = $data['error_name'];
                    }
                    if ((utf8_strlen($this->request->post['phone']) < 5)) {
                        $json['error'] = true;
                        $json['error_phone'] = $data['error_phone'];
                    }
                    if (!isset($json['error'])):
                        $mail = new Mail();
                        $mail->protocol = $this->config->get('config_mail_protocol');
                        $mail->parameter = $this->config->get('config_mail_parameter');
                        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
                        $mail->setTo($this->config->get('config_email'));
                        $mail->setFrom($this->config->get('config_name'));
                        $mail->setSender(html_entity_decode($this->request->post['phone'], ENT_QUOTES, 'UTF-8'));
                        $mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject_callback'), ''), ENT_QUOTES, 'UTF-8'));
                        $html = '<table style="width: 100%; background-color: #f1f1f1;">
                                    <tbody>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">' . $data['text_name'] . '</td><td>' . $this->request->post['name'] . '</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">' . $data['text_phone'] . '</td><td>' . $this->request->post['phone'] . '</td>
                                        </tr>
                                    </tbody>
                                </table>';
                        $mail->setHtml($html);
                        $mail->send();
                        $json['success'] = $data['success'];
                    endif;
                endif;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function opt()
    {
        $json = array();

        $this->load->language('sendmess/sendmess');

        $data['text_name'] = $this->language->get('text_name');
        $data['text_lastname'] = $this->language->get('text_lastname');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_message'] = $this->language->get('text_message');
        $data['text_phone'] = $this->language->get('text_phone');
        $data['text_type'] = $this->language->get('text_type');
        $data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $data['text_model'] = $this->language->get('text_model');

        $data['error_name'] = $this->language->get('error_name');
        $data['error_lastname'] = $this->language->get('error_lastname');
        $data['error_email'] = $this->language->get('error_email');
        $data['error_phone'] = $this->language->get('error_phone');
        $data['error_message'] = $this->language->get('error_message');
        $data['success'] = $this->language->get('success');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

            if ($this->request->post['goal'] == 'modal-opt_request') {
                if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
                    $json['error'] = true;
                    $json['error_name'] = $data['error_name'];
                }
                if ((utf8_strlen($this->request->post['lastname']) < 3) || (utf8_strlen($this->request->post['lastname']) > 25)) {
                    $json['error'] = true;
                    $json['error_lastname'] = $data['error_lastname'];
                }

                if ((utf8_strlen($this->request->post['email']) < 5)) {
                    $json['error'] = true;
                    $json['error_email'] = $data['error_email'];
                }

                if ((utf8_strlen($this->request->post['phone']) < 5)) {
                    $json['error'] = true;
                    $json['error_phone'] = $data['error_phone'];
                }
                if (!isset($json['error'])) {
                    $mail = new Mail();
                    $mail->protocol = $this->config->get('config_mail_protocol');
                    $mail->parameter = $this->config->get('config_mail_parameter');
                    $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                    $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                    $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                    $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                    $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
                    $mail->setTo($this->config->get('config_email'));
                    $mail->setFrom($this->config->get('config_name'));
                    $mail->setSender(html_entity_decode($this->request->post['phone'], ENT_QUOTES, 'UTF-8'));
                    $mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject_opt'), ''), ENT_QUOTES, 'UTF-8'));
                    $html = '<table style="width: 100%; background-color: #f1f1f1;">
                                    <tbody>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">' . $data['text_name'] . '</td><td>' . $this->request->post['name'] . '</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">' . $data['text_lastname'] . '</td><td>' . $this->request->post['lastname'] . '</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">' . $data['text_phone'] . '</td><td>' . $this->request->post['phone'] . '</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">' . $data['text_email'] . '</td><td>' . $this->request->post['email'] . '</td>
                                        </tr>
                                    </tbody>
                                </table>';
                    $mail->setHtml($html);
                    $mail->send();
                    $json['success'] = $data['success'];
                }
            }
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
