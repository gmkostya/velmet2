<?php echo $header; ?>
<div class="container container-top">
    <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <?php if ($i + 1 < count($breadcrumbs)) { ?>
                    <a itemscope itemtype="http://schema.org/Thing" itemprop="item"
                       href="<?php echo $breadcrumb['href']; ?>">
                        <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <?php echo $content_top; ?>
</div>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <h1 ><?php echo $heading_title; ?></h1>
            <div class="row">
                <div class="col-md-4 col-sm-12 col-contact-left">
                     <ul class="list-inline col-contact-list">
                         <?php if ($telephone) { ?>
                            <li class="contact-list contact-phone-list"><i class="icon-phone"></i>
                                <?php if ($telephone) { ?>
                            <a href="tel:<?php echo $telephone_link; ?>"
                               class="decorate decorate-chery-color decorate-left"><span><?php echo $telephone; ?></span></a><br/>
                                <?php } ?>
                                <?php if ($telephone2) { ?>
                            <a href="tel:<?php echo $telephone_link2; ?>"
                               class="decorate decorate-chery-color decorate-left"><span><?php echo $telephone2; ?></span></a><br/>
                                <?php } ?>
                                <?php if ($telephone3) { ?>
                            <a href="tel:<?php echo $telephone_link3; ?>"
                               class="decorate decorate-chery-color decorate-left"><span><?php echo $telephone3; ?></span></a>
                                <?php } ?>
                            </li>
                         <?php } ?>
                        <?php if ($address) { ?>
                            <li class="contact-list contact-address-list"><i class="icon-location"></i><span><?php echo $address; ?></span></li>
                        <?php } ?>
                        <?php if ($skype) { ?>
                            <li class="contact-list contact-eskype-list"><i class="icon-skype"></i><a href="skype:<?php echo $skype; ?>"
                                   class="decorate decorate-chery-color decorate-left"><span><?php echo $skype; ?></span></a>
                            </li>
                        <?php } ?>
                        <?php if ($emails) { ?>
                            <li class="contact-list contact-email-list"><i class="icon-mail"></i><a href="mailto:<?php echo $emails; ?>"
                                   class="decorate decorate-chery-color decorate-left"><span><?php echo $emails; ?></span></a>
                            </li><?php } ?>
                    </ul>
                </div>
                <div class="col-md-8 col-sm-12 col-contact-right">
                    <h4 ><?php echo $heading_title2; ?></h4>
                    <div class="contact-form-text"><?php echo $text_before_contact; ?></div>
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"
                          class="form-vertical">
                        <div class="row row-contact-form">
                            <div class="col-md-4 col-sm-12">
                                <fieldset>
                                    <div class="form-group required">
                                        <label class="col-sm-12 control-label"
                                               for="input-name"><?php echo $entry_name; ?></label>
                                        <div class="col-sm-12">
                                            <input type="text" name="name" value="<?php echo $name; ?>" id="input-name"
                                                   class="form-control"/>
                                            <?php if ($error_name) { ?>
                                                <div class="text-danger"><?php echo $error_name; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                            </div>
                            <div class="col-md-4 col-sm-12">

                                <div class="form-group required">
                                    <label class="col-sm-12 control-label"
                                           for="input-email"><?php echo $entry_email; ?></label>
                                    <div class="col-sm-12">
                                        <input type="text" name="email" value="<?php echo $email; ?>" id="input-email"
                                               class="form-control"/>
                                        <?php if ($error_email) { ?>
                                            <div class="text-danger"><?php echo $error_email; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group required">
                                    <label class="col-sm-12 control-label"
                                           for="input-telephone"><?php echo $entry_telephone; ?></label>
                                    <div class="col-sm-12">
                                        <input type="text" name="telephone" value=""
                                               id="input-telephone" class="form-control js-phone-mask" placeholder="+38 (0__) ___ - __ - __"/>
                                        <?php if ($error_telephone) { ?>
                                            <div class="text-danger"><?php echo $error_telephone; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group required">
                                    <label class="col-sm-12 control-label"
                                           for="input-enquiry"><?php echo $entry_enquiry; ?></label>
                                    <div class="col-sm-12">
                                        <textarea name="enquiry" rows="3" id="input-enquiry"
                                                  class="form-control"><?php echo $enquiry; ?></textarea>
                                        <?php if ($error_enquiry) { ?>
                                            <div class="text-danger"><?php echo $error_enquiry; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php echo $captcha; ?>
                            <div class="col-md-12 col-sm-12 col-contact-bt">
                                <div class="buttons">
                                    <div class="pull-left">
                                        <input class="btn btn-primary" type="submit"
                                               value="<?php echo $button_submit2; ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php echo $column_right; ?></div>
</div>
<div class="curier-contact-map">
    <div id="map_canvas" style="width:auto; height:340px;position:relative;"></div>
</div>
<?php if ($content_bottom) { ?>
    <div class="curier-catalog-info curier-grey">
        <div class="container">
            <?php echo $content_bottom; ?>
        </div>
    </div>
<?php } ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3-C92-ELllZBvN03tmR6JKFmGJ_sv52I" type="text/javascript"></script>
<script type="text/javascript"><!--
    function initialize() {
        var latlng = new google.maps.LatLng(<?php echo $geocode; ?>);
        var settings = {
            zoom: 14,
            center: latlng,
            zoomControl: true,
            scrollwheel: false,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP};
        var map = new google.maps.Map(document.getElementById("map_canvas"), settings);
        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<div id="bodyContent">'+
            '<p><?php echo $store; ?></p>'+
            '</div>'+
            '</div>';
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        var companyImage = new google.maps.MarkerImage('/image/logo_map.png',
            new google.maps.Size(80,120),
            new google.maps.Point(0,0),
            new google.maps.Point(50,50)
        );
        var companyMarker = new google.maps.Marker({
            position: latlng,
            map: map,
           // icon: companyImage,
            zIndex: 3});
        google.maps.event.addListener(companyMarker, 'mouseover', function() {
            infowindow.open(map,companyMarker);
        });

    }
    google.maps.event.addDomListener(window, 'load', initialize);
    //--></script>


<?php echo $footer; ?>
