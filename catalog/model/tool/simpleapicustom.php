<?php
/*
@author Dmitriy Kubarev
@link   http://www.simpleopencart.com
@link   http://www.opencart.com/index.php?route=extension/extension/info&extension_id=4811
*/

class ModelToolSimpleApiCustom extends Model {
    public function example($filterFieldValue) {
        $values = array();

        $values[] = array(
            'id'   => 'my_id',
            'text' => 'my_text'
        );

        return $values;
    }
    public function ShopAdress($filterFieldValue) {
        $values = array();

        if($filterFieldValue=='Хмельницкий') {
            $values[] = array(
                'id' => '1',
                'text' => 'г. Хмельницкий, ул. Заречанская 3/1, 4-этаж'
            );
            $values[] = array(
                'id' => '2',
                'text' => 'г. Хмельницкий, ул. Н. Береговая 42/1, 2-й этаж'
            );
        } elseif ($filterFieldValue=='Khmelnitsky') {
            $values[] = array(
                'id' => '1',
                'text' => 'Khmelnitsky, st. Zarechanskaya 3/1, 4th floor'
            );
            $values[] = array(
                'id' => '2',
                'text' => 'Khmelnitsky, st. N. Beregovaya 42/1, 4th floor'
            );

        } else {
            $values[] = array(
                'id' => '1',
                'text' => 'Здесь магазина нет'
            );

        }

        return $values;
    }

    public function checkCaptcha($value, $filter) {
        if (isset($this->session->data['captcha']) && $this->session->data['captcha'] != $value) {
            return false;
        }

        return true;
    }

    public function getYesNo($filter = '') {
        return array(
            array(
                'id'   => '1',
                'text' => $this->language->get('text_yes')
            ),
            array(
                'id'   => '0',
                'text' => $this->language->get('text_no')
            )
        );
    }
    public function getNovapostOffices($city = '')
    {
        /*получаем данные об способах доставки*/
         echo $city;
        $this->load->model('extension/extension');
        $offices = array();

        if (isset($this->session->data['simple']['shipping_address'])) {

            $address = $this->session->data['simple']['shipping_address'];
            //$this->_templateData['address_empty'] = $this->simplecheckout->isShippingAddressEmpty();

            //  var_dump($this->session->data);


            $results = $this->model_extension_extension->getExtensions('shipping');


            foreach ($results as $result) {

                //var_dump($result['code'] );

                if ( $result['code'] == 'novaposhta') {
                    $this->load->model('shipping/' . $result['code']);
                    $quote = $this->{'model_shipping_' . $result['code']}->getQuote($address);
                  //  var_dump($quote);
                }



            }
            /**/

            if (isset($quote['offices']['data'])) {

                foreach ($quote['offices']['data'] as $key => $value) {
                    $offices[] = array(
                        'id' => $key,
                        'text' => $value['Description']
                    );
                }
            }
        }

        return $offices;
        /*  return array(
              array(
                  'id'   => '1',
                  'text' => $this->language->get('text_yes')
              ),
              array(
                  'id'   => '0',
                  'text' => $this->language->get('text_no')
              )
          );*/
    }
    public function getNovapostCities(){

        if (isset($this->request->get['filter'])) {
            $filter = $this->request->get['filter'];
        } else {
            $filter = '';
        }

        ///var_dump($filter) ;
        $city_response = $this->getResponse(
            $this->getRequest('Address', 'getCities', array('FindByString' => $filter))
        );
        //echo '<pre>';
        // var_dump($city_response['data']);
        // echo '</pre>';
        if($city_response['data']){

            $rows = array();
            foreach ($city_response['data'] as $city){
                // $city_array[] =$city['DescriptionRu'];
                $rows[] = $city['DescriptionRu'];

            }

        }
        //  jQuery211013737436736363928_1482354240259(["New Albany, IN, United States","New Albany, KS, United States","New Albany, MS, United States","New Albany, OH, United States","New Albany, PA, United States","New Albin, IA, United States","New Alexandria, PA, United States","New Almaden, CA, United States","NEW AMSTERDAM, EB, Guyana","New Athens, IL, United States","New Athens, OH, United States","New Auburn, MN, United States","New Auburn, WI, United States","New Augusta, MS, United States","New Baden, IL, United States","New Baden, TX, United States","New Baltimore, MI, United States","New Baltimore, NY, United States","New Baltimore, PA, United States","New Bavaria, OH, United States"]);
        return $rows;


    }




    private function getRequest($modelName, $calledMethod, $methodProperties) {
        $request = array(
            'modelName' => $modelName,
            'calledMethod' => $calledMethod,
            'methodProperties' => $methodProperties,
           // 'apiKey' => $this->config->get('novaposhta_api_key')
            'apiKey' => 'bb9fed013cb62e2b067b3547e930e730'
        );

        return json_encode($request);
    }




    private function getResponse($request) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.novaposhta.ua/v2.0/json/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        $json = json_decode($response, true);
        if ($json) {
            return $json;
        }

        return $response;
    }

}
