<?php
class ControllerModuleNewsGallery extends Controller {
	public function index($setting)
    {

		
		
        $this->language->load('module/news');

        $this->load->model('catalog/news');

        $this->load->model('catalog/ncomments');

        $this->load->model('catalog/ncategory');

        $this->load->model('tool/image');

        $this->document->addStyle('catalog/view/theme/default/stylesheet/blog-news.css');

        $data['text_headlines'] = $this->language->get('text_headlines');

        $data['text_gallery'] = $this->language->get('text_gallery');

        $data['display_style'] = $this->config->get('ncategory_bnews_display_style');
        $data['text_posted_by'] = $this->language->get('text_posted_by');
        $data['text_posted_on'] = $this->language->get('text_posted_on');
        $data['text_posted_pon'] = $this->language->get('text_posted_pon');
        $data['text_posted_in'] = $this->language->get('text_posted_in');
        $data['text_updated_on'] = $this->language->get('text_updated_on');
        $data['text_comments'] = $this->language->get('text_comments');
        $data['text_comments_v'] = $this->language->get('text_comments_v');
        $data['text_see_more'] = $this->language->get('text_see_more');


        $data['display_style'] = $this->config->get('ncategory_bnews_display_style');
        $data['button_more'] = $this->language->get('button_more');
        $data['disqus_sname'] = $this->config->get('ncategory_bnews_disqus_sname');
        $data['disqus_status'] = $this->config->get('ncategory_bnews_disqus_status');
        $data['fbcom_status'] = $this->config->get('ncategory_bnews_fbcom_status');
        $data['fbcom_appid'] = $this->config->get('ncategory_bnews_fbcom_appid');
        $data['fbcom_theme'] = $this->config->get('ncategory_bnews_fbcom_theme');
        $data['fbcom_posts'] = $this->config->get('ncategory_bnews_fbcom_posts');

        if (isset($setting['news_id'])) {
            $news_id = $setting['news_id'];
        } else {
            $news_id = 22;
        }


        $data['see_more'] = $this->url->link('news/article', '&news_id=' .  $news_id);

        $data['news_id'] = $news_id;
        $news_info = $this->model_catalog_news->getNewsStory($news_id);


		
		/*
		$datas = file_get_contents("https://www.googleapis.com/youtube/v3/videos?key=AIzaSyBN1MDjJXmySFNhLJdIye5pwIj6A_F2u4E&part=snippet&id=MlqPzisHZYk");
$json = json_decode($datas);
var_dump($json->items[0]->snippet->thumbnails);*/


        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_review'] = $this->language->get('entry_comment');
        $data['entry_captcha'] = $this->language->get('entry_captcha');
        $data['text_note'] = $this->language->get('text_note');
        $data['nocomment'] = $this->language->get('nocomment');
        $data['writec'] = $this->language->get('writec');
        $data['text_wait'] = $this->language->get('text_wait');
        $data['text_send'] = $this->language->get('bsend');
        $data['title_comments'] = sprintf($this->model_catalog_ncomments->getTotalNcommentsByNewsId($data['news_id']));
        $data['text_coms'] = $this->language->get('title_comments');
        $data['text_posted_pon'] = $this->language->get('text_posted_pon');
        $data['text_posted_in'] = $this->language->get('text_posted_in');
        $data['text_updated_on'] = $this->language->get('text_updated_on');
        $data['text_tags'] = $this->language->get('text_tags');
        $data['text_posted_by'] = $this->language->get('text_posted_by');
        $data['text_posted_on'] = $this->language->get('text_posted_on');
        $data['text_comments'] = $this->language->get('text_comments');
        $data['text_comments_v'] = $this->language->get('text_comments_v');
        $data['text_comments_to'] = $this->language->get('text_comments_to');
        $data['text_reply_to'] = $this->language->get('text_reply_to');
        $data['text_reply'] = $this->language->get('text_reply');
        $data['author_text'] = $this->language->get('author_text');
        $data['button_more'] = $this->language->get('button_more');
        $data['category'] = '';
        $cats = $this->model_catalog_news->getNcategoriesbyNewsId($data['news_id']);
        if ($cats) {
            $comma = 0;
            foreach ($cats as $catid) {
                $catinfo = $this->model_catalog_ncategory->getncategory($catid['ncategory_id']);
                if ($catinfo) {
                    if ($comma) {
                        $data['category'] .= ', <a href="' . $this->url->link('news/ncategory', 'ncat=' . $catinfo['ncategory_id']) . '">' . $catinfo['name'] . '</a>';
                    } else {
                        $data['category'] .= '<a href="' . $this->url->link('news/ncategory', 'ncat=' . $catinfo['ncategory_id']) . '">' . $catinfo['name'] . '</a>';
                    }
                    $comma++;
                }
            }
        }

        $data['gallery_type'] = isset($news_info['gal_slider_t']) ? $news_info['gal_slider_t'] : 1;
        if ($data['gallery_type'] != 1) {
            $this->document->addScript('catalog/view/theme/default/blog-mp/jssor.slider.mini.js');
        }
        $data['gallery_height'] = $news_info['gal_slider_h'];
        $data['gallery_width'] = $news_info['gal_slider_w'];
        $data['acom'] = $news_info['acom'];
        $data['heading_title'] = $news_info['title'];
        $data['description'] = html_entity_decode($news_info['description'], ENT_QUOTES, 'UTF-8');
        $data['description'] = str_replace("<video", "<iframe", $data['description']);
        $data['description'] = str_replace("</video>", "</iframe>", $data['description']);
        $data['custom1'] = html_entity_decode($news_info['cfield1'], ENT_QUOTES, 'UTF-8');
        $data['custom2'] = html_entity_decode($news_info['cfield2'], ENT_QUOTES, 'UTF-8');
        $data['custom3'] = html_entity_decode($news_info['cfield3'], ENT_QUOTES, 'UTF-8');
        $data['custom4'] = html_entity_decode($news_info['cfield4'], ENT_QUOTES, 'UTF-8');
        $data['date_added'] = date('d.m.Y', strtotime($news_info['date_added']));
        $data['date_updated'] = date('d.m.Y', strtotime($news_info['date_updated']));
        if ($data['date_added'] == $data['date_updated']) {
            $data['date_updated'] = '';
        }
        if ($news_info['nauthor_id']) {
            $data['author_link'] = $this->url->link('news/ncategory', 'author=' . $news_info['nauthor_id']);
            $data['author'] = $news_info['author'];
            if ($data['author']) {
                if (method_exists($this->document, 'addExtraTag')) {
                    $this->document->addExtraTag('noprop', $data['author'], 'author');
                }
            }
            $data['author_image'] = ($news_info['nimage']) ? $this->model_tool_image->resize($news_info['nimage'], 70, 70) : false;
            $authordesc = $this->model_catalog_news->getNauthorDescriptions($news_info['nauthor_id']);
            if (isset($authordesc[$this->config->get('config_language_id')])) {
                $data['author_desc'] = html_entity_decode($authordesc[$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
            } else {
                $data['author_desc'] = '';
            }
        } else {
            $data['author'] = '';
        }
        $data['ntags'] = array();
        if ($news_info['ntags']) {
            $ntags = explode(',', $news_info['ntags']);
            foreach ($ntags as $ntag) {
                $data['ntags'][] = array(
                    'ntag' => trim($ntag),
                    'href' => $this->url->link('news/search', 'article_tag=' . trim($ntag))
                );
            }
        }
        $data['button_news'] = $this->language->get('button_news');

        $data['button_cart'] = $this->language->get('button_cart');

        $data['button_wishlist'] = $this->language->get('button_wishlist');

        $data['button_compare'] = $this->language->get('button_compare');

        $data['news_prelated'] = $this->language->get('news_prelated');

        $data['news_related'] = $this->language->get('news_related');

        $bwidth = ($this->config->get('ncategory_bnews_thumb_width')) ? $this->config->get('ncategory_bnews_thumb_width') : 230;
        $bheight = ($this->config->get('ncategory_bnews_thumb_height')) ? $this->config->get('ncategory_bnews_thumb_height') : 230;
        if ($news_info['image']) {
            $data['thumb'] = $this->model_tool_image->myresize($news_info['image'], $bwidth, $bheight);
            $data['popup'] = $this->model_tool_image->resize($news_info['image'], 600, 600);
        } else {
            $data['thumb'] = '';
            $data['popup'] = '';
        }

        $data['article'] = array();

        $bbwidth = ($this->config->get('ncategory_bnews_image_width')) ? $this->config->get('ncategory_bnews_image_width') : 80;
        $bbheight = ($this->config->get('ncategory_bnews_image_height')) ? $this->config->get('ncategory_bnews_image_height') : 80;

        if ($this->config->get('ncategory_bnews_display_elements')) {
            $elements = $this->config->get('ncategory_bnews_display_elements');
        } else {
            $elements = array("name", "image", "da", "du", "author", "category", "desc", "button", "com", "custom1", "custom2", "custom3", "custom4");
        }

        $data['page_url'] = $this->url->link('news/article', '&news_id=' . $data['news_id']);
        $data['disqus_sname'] = $this->config->get('ncategory_bnews_disqus_sname');
        $data['disqus_id'] = 'article_' . $data['news_id'];
        $data['disqus_status'] = $this->config->get('ncategory_bnews_disqus_status');
        $data['fbcom_status'] = $this->config->get('ncategory_bnews_fbcom_status');
        $data['fbcom_appid'] = $this->config->get('ncategory_bnews_fbcom_appid');
        $data['fbcom_theme'] = $this->config->get('ncategory_bnews_fbcom_theme');
        $data['fbcom_posts'] = $this->config->get('ncategory_bnews_fbcom_posts');

        if (method_exists($this->document, 'addExtraTag')) {
            if (!$this->config->get('ncategory_bnews_facebook_tags')) {
                $this->document->addExtraTag('og:title', $data['heading_title']);
                if ($data['thumb']) {
                    $this->document->addExtraTag('og:image', $data['thumb']);
                }
                $this->document->addExtraTag('og:url', $data['page_url']);
                $this->document->addExtraTag('og:type', 'product');
                $this->document->addExtraTag('og:description', trim(utf8_substr(strip_tags(html_entity_decode($data['description'], ENT_QUOTES, 'UTF-8')), 0, 200) . '...'));
            }
            if (!$this->config->get('ncategory_bnews_twitter_tags')) {
                $this->document->addExtraTag('twitter:card', 'summary');
                $this->document->addExtraTag('twitter:url', $data['page_url']);
                $this->document->addExtraTag('twitter:title', $data['heading_title']);
                $this->document->addExtraTag('twitter:description', trim(utf8_substr(strip_tags(html_entity_decode($data['description'], ENT_QUOTES, 'UTF-8')), 0, 200) . '...'));
                if ($data['thumb']) {
                    $this->document->addExtraTag('twitter:image', $data['thumb']);
                }
            }
        }

        $data['article_videos'] = array();

        $vid_results = $this->model_catalog_news->getArticleVideos($data['news_id']);

        foreach ($vid_results as $result) {
			parse_str( parse_url( $result['video'], PHP_URL_QUERY ), $array_of_vars );
			
            $result['text'] = unserialize($result['text']);
            $result['text'] = isset($result['text'][$this->config->get('config_language_id')]) ? $result['text'][$this->config->get('config_language_id')] : '';
            //$code = '<div id="whateverID"><iframe  frameborder="0" allowfullscreen src="' . str_replace("watch?v=", "embed/", $result['video']) . '" height="' . $result['height'] . '"width="100%" style="max-width:' . $result['width'] . 'px"></iframe></div>';
			$code = '<iframe style="position: absolute; width:100%; height:  100%; border: none" frameborder="0" title="YouTube video player" type="text/html" src="' . str_replace("watch?v=", "embed/", $result['video']) . '?enablejsapi=1" ></iframe>';

			$code2 = $result['video'];
      
			$data['article_videos'][] = array(
					'text'  => $result['text'],
					'code' => $code,
					'code2' => $code2,
					'id_video' => $array_of_vars['v'],
					'popup' => 'https://i.ytimg.com/vi/' .$array_of_vars['v']. '/sddefault.jpg',
					'thumb' => 'https://i.ytimg.com/vi/' .$array_of_vars['v']. '/sddefault.jpg',
					'thumb2' => $this->model_tool_image->resize('logo.png', '470', '320'),
               		'type' => 'v',
                	'sort_order' => $result['sort_order']
			);
        }
		
		 

        $data['gallery_images'] = array();

        $gal_results = $this->model_catalog_news->getArticleGallery($data['news_id']);

        foreach ($gal_results as $result) {
            $result['text'] = unserialize($result['text']);
            $result['text'] = isset($result['text'][$this->config->get('config_language_id')]) ? $result['text'][$this->config->get('config_language_id')] : '';
           /* $data['gallery_images'][] = array(
                'text' => $result['text'],
                'popup' => $this->model_tool_image->resize($result['image'], $news_info['gal_popup_w'], $news_info['gal_popup_h']),
                'thumb' => $this->model_tool_image->myResize($result['image'], $news_info['gal_thumb_w'], $news_info['gal_thumb_h'])
            );*/
			
			$data['gallery_images'][] = array(
					'text'  => $result['text'],
					'popup' => $this->model_tool_image->resize($result['image'], $news_info['gal_popup_w'], $news_info['gal_popup_h']),
					'thumb' => '/image/'.$result['image'],
					'thumb2' => $this->model_tool_image->resize($result['image'], '470', '320'),
                	'code' => '',
					'code2' => '',
					'id_video' => '',
					'type' => 'i',
					'sort_order' => $result['sort_order']
			);
			
        }
		
		  $gallery_array =  array_merge($data['gallery_images'], $data['article_videos']);




        foreach ($gallery_array as $key => $row) {
            $sort_order[$key]  = $row['sort_order'];
        }

		array_multisort($sort_order, SORT_ASC,  $gallery_array);
		//var_dump($gallery_array);
        $data['gallery_array'] =  $gallery_array;

		
        $data['products'] = array();
        $data['text_tax'] = $this->language->get('text_tax');
        $results = $this->model_catalog_news->getProductRelated($data['news_id']);

        foreach ($results as $result) {
            $image = ($result['image']) ? $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height')) : false;

            $price = (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) ? $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'))) : false;

            $special = ((float)$result['special']) ? $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'))) : false;

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
            } else {
                $tax = false;
            }

            $rating = ($this->config->get('config_review_status')) ? (int)$result['rating'] : false;

            $data['products'][] = array(
                'product_id' => $result['product_id'],
                'thumb' => $image,
                'name' => $result['name'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                'price' => $price,
                'tax' => $tax,
                'special' => $special,
                'rating' => $rating,
                'reviews' => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
                'href' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
            );
        }
        $results = $this->model_catalog_news->getNewsRelated($data['news_id']);

        foreach ($results as $result) {
            if ($result['title']) {
                $name = (in_array("name", $elements) && $result['title']) ? $result['title'] : '';
                $da = (in_array("da", $elements)) ? date('d.m.Y', strtotime($result['date_added'])) : '';
                $du = (in_array("du", $elements) && $result['date_updated'] && $result['date_updated'] != $result['date_added']) ? date('d.m.Y', strtotime($result['date_updated'])) : '';
                $button = (in_array("button", $elements)) ? true : false;
                $custom1 = (in_array("custom1", $elements) && $result['cfield1']) ? html_entity_decode($result['cfield1'], ENT_QUOTES, 'UTF-8') : '';
                $custom2 = (in_array("custom2", $elements) && $result['cfield2']) ? html_entity_decode($result['cfield2'], ENT_QUOTES, 'UTF-8') : '';
                $custom3 = (in_array("custom3", $elements) && $result['cfield3']) ? html_entity_decode($result['cfield3'], ENT_QUOTES, 'UTF-8') : '';
                $custom4 = (in_array("custom4", $elements) && $result['cfield4']) ? html_entity_decode($result['cfield4'], ENT_QUOTES, 'UTF-8') : '';
                if (in_array("image", $elements) && ($result['image'] || $result['image2'])) {
                    if ($result['image2']) {
                        $image = 'image/' . $result['image2'];
                    } else {
                        $image = $this->model_tool_image->resize($result['image'], $bbwidth, $bbheight);
                    }
                } else {
                    $image = false;
                }
                if (in_array("author", $elements) && $result['author']) {
                    $author = $result['author'];
                    $author_id = $result['nauthor_id'];
                    $author_link = $this->url->link('news/ncategory', 'author=' . $result['nauthor_id']);
                } else {
                    $author = '';
                    $author_id = '';
                    $author_link = '';
                }
                if (in_array("desc", $elements) && ($result['description'] || $result['description2'])) {
                    if ($result['description2'] && (strlen(html_entity_decode($result['description2'], ENT_QUOTES, 'UTF-8')) > 20)) {
                        $desc = html_entity_decode($result['description2'], ENT_QUOTES, 'UTF-8');
                    } else {
                        $desc_limit = $this->config->get('ncategory_bnews_desc_length') ? $this->config->get('ncategory_bnews_desc_length') : 600;
                        $desc = utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $desc_limit) . '..';
                    }
                } else {
                    $desc = '';
                }
                if (in_array("com", $elements) && $result['acom']) {
                    $com = $this->model_catalog_ncomments->getTotalNcommentsByNewsId($result['news_id']);
                    if (!$com) {
                        $com = " 0 ";
                    }
                } else {
                    $com = '';
                }
                if (in_array("category", $elements)) {
                    $category = "";
                    $cats = $this->model_catalog_news->getNcategoriesbyNewsId($result['news_id']);
                    if ($cats) {
                        $comma = 0;
                        foreach ($cats as $catid) {
                            $catinfo = $this->model_catalog_ncategory->getncategory($catid['ncategory_id']);
                            if ($catinfo) {
                                if ($comma) {
                                    $category .= ', <a href="' . $this->url->link('news/ncategory', 'ncat=' . $catinfo['ncategory_id']) . '">' . $catinfo['name'] . '</a>';
                                } else {
                                    $category .= '<a href="' . $this->url->link('news/ncategory', 'ncat=' . $catinfo['ncategory_id']) . '">' . $catinfo['name'] . '</a>';
                                }
                                $comma++;
                            }
                        }
                    }
                } else {
                    $category = '';
                }

                $data['article'][] = array(
                    'article_id' => $result['news_id'],
                    'name' => $name,
                    'thumb' => $image,
                    'date_added' => $da,
                    'du' => $du,
                    'author' => $author,
                    'author_id' => $author_id,
                    'author_link' => $author_link,
                    'description' => $desc,
                    'button' => $button,
                    'custom1' => $custom1,
                    'custom2' => $custom2,
                    'custom3' => $custom3,
                    'custom4' => $custom4,
                    'category' => $category,
                    'href' => $this->url->link('news/article', '&news_id=' . $result['news_id']),
                    'total_comments' => $com
                );
            }
        }

        $data['news'] = $this->url->link('news/headlines');
        if (isset($this->request->get['page'])) {
            $page = (int)$this->request->get['page'];
        } else {
            $page = 1;
        }



        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/news_gallery.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/news_gallery.tpl', $data);
        } else {
            return $this->load->view('default/template/module/news_gallery.tpl', $data);
        }
    }
}
?>
