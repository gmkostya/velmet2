<div class="list-home-category">
  <?php $k=0; foreach ($categories as $category) { $k++;  ?>
    <?php if ($k==1) { ?>
      <div class="col-home-category  col-home-category-left">
        <div class="col-home-category-image" style="background: url('<?php echo $category['image']; ?>')">
          <img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>">
          <div class="name"><?php echo $category['name']; ?></div>
          <a class="btn btn-green" href="<?php echo $category['href']; ?>"><span><?php echo $text_see_all; ?></span><i class="icon-rectangle-2-copy-3"></i></a>
        </div>
        <div class="col-home-category-slider">
        <?php foreach ($category['products'] as $product) { ?>
            <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="product-thumb transition">
                    <div class="image">
                        <?php if ($product['label']) { ?>
                            <div class="product-label <?php echo  $product['label_type']; ?>"><?php echo $product['label']; ?></div>
                            <?php if ($product['count_end_date']) { ?>
                                <div class="timer-curier">
                                    <div class="timer" data-timeend="<?php echo $product['count_end_date']; ?>" data-timetext="<?php echo $product['name']; ?>"></div>
                                    <div class="timer-text" ><span class="t-day" >дней</span><span class="t-hour">часов</span><span class="t-min">мин</span><span class="t-sec">сек</span></div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        <a href="<?php echo $product['href']; ?>">
                            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                        </a>
                    </div>
                    <div class="caption">
                        <div class="name" ><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                        <?php if ($product['price']) { ?>
                            <div class="price">
                                <?php if ($product['special']) { ?>
                                    <span class="price-old"><?php echo $product['price']; ?></span>
                                    <span class="price-new"><?php echo $product['special']; ?></span>
                                <?php } else { ?>
                                    <?php echo $product['price']; ?>
                                <?php } ?>
                                <?php if ($product['tax']) { ?>
                                    <span
                                        class="price-tax"><?php echo $text_tax; ?><?php echo $product['tax']; ?></span>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="button-group">
                        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="product-layout-button-cart"><i class="icon-cart"></i><span><?php echo $button_cart; ?></span></button>
                        <?php if ($product['wishlist_status']){ ?>
                            <button type="button" data-toggle="tooltip"
                                    title="<?php echo $button_wishlist_is; ?>"
                                    onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                    class="product-layout-button-wishlist red js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-uniF08B2"></i>
                            </button>
                        <?php } else { ?>
                            <button type="button" data-toggle="tooltip"
                                    title="<?php echo $button_wishlist; ?>"
                                    onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                    class="product-layout-button-wishlist js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-forma-1-copy-5"></i>
                            </button>
                        <?php } ?>
                    </div>

                </div>
            </div>
        <?php } ?>
        </div>
        </div>
    <?php } ?>
    <?php if ($k==2) { ?>
      <div class="col-home-category  col-home-category-right">
        <div class="col-home-category-slider">
            <?php foreach ($category['products'] as $product) { ?>
                <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product-thumb transition">
                        <div class="image">
                            <?php if ($product['label']) { ?>
                                <div class="product-label <?php echo  $product['label_type']; ?>"><?php echo $product['label']; ?></div>
                                <?php if ($product['count_end_date']) { ?>
                                    <div class="timer-curier">
                                        <div class="timer" data-timeend="<?php echo $product['count_end_date']; ?>" data-timetext="<?php echo $product['name']; ?>"></div>
                                        <div class="timer-text" ><span class="t-day" >дней</span><span class="t-hour">часов</span><span class="t-min">мин</span><span class="t-sec">сек</span></div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <a href="<?php echo $product['href']; ?>">
                                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                            </a>
                        </div>
                        <div class="caption">
                            <div class="name" ><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                            <?php if ($product['price']) { ?>
                                <div class="price">
                                    <?php if ($product['special']) { ?>
                                        <span class="price-old"><?php echo $product['price']; ?></span>
                                        <span class="price-new"><?php echo $product['special']; ?></span>
                                    <?php } else { ?>
                                        <?php echo $product['price']; ?>
                                    <?php } ?>
                                    <?php if ($product['tax']) { ?>
                                        <span
                                            class="price-tax"><?php echo $text_tax; ?><?php echo $product['tax']; ?></span>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="button-group">
                            <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="product-layout-button-cart"><i class="icon-cart"></i><span><?php echo $button_cart; ?></span></button>
                            <?php if ($product['wishlist_status']){ ?>
                                <button type="button" data-toggle="tooltip"
                                        title="<?php echo $button_wishlist_is; ?>"
                                        onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                        class="product-layout-button-wishlist red js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-uniF08B2"></i>
                                </button>
                            <?php } else { ?>
                                <button type="button" data-toggle="tooltip"
                                        title="<?php echo $button_wishlist; ?>"
                                        onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                        class="product-layout-button-wishlist js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-forma-1-copy-5"></i>
                                </button>
                            <?php } ?>
                        </div>

                    </div>
                </div>
          <?php } ?>
        </div>
        <div class="col-home-category-image"  style="background: url('<?php echo $category['image']; ?>')">
          <img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>">
          <div class="name"><?php echo $category['name']; ?></div>
          <a class="btn btn-green" href="<?php echo $category['name']; ?>"><span><?php echo $text_see_all; ?></span><i class="icon-rectangle-2-copy-3"></i></a>
        </div>

      </div>
      <?php  $k=0;} ?>

  <?php } ?>

</div>
