<?php
/**
 * Project:     inWidget: show pictures from instagram.com on your site!
 * File:        template.php
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of MIT license
 * http://inwidget.ru/MIT-license.txt
 *
 * @link http://inwidget.ru
 * @copyright 2014 Alexandr Kazarmshchikov
 * @author Alexandr Kazarmshchikov
 * @version 1.0.6
 * @package inWidget
 *
 */

if(!$inWidget) die('inWidget object is not init.');
if(!is_object($inWidget->data)) die('<b style="color:red;">Cache file contains plain text:</b><br />'.$inWidget->data);
?>
	<div class="instagram_slider">
		<?php
		if(!empty($inWidget->data->images)){
		    //var_dump($inWidget->preview);
			$size = 250 ;
			if($inWidget->config['imgRandom'] === true) shuffle($inWidget->data->images);
			$inWidget->data->images = array_slice($inWidget->data->images,0,$inWidget->view);
			$iic = 0;
			foreach ($inWidget->data->images as $key=>$item){
				//print_r($item);
				switch ($inWidget->preview){
					case 'large':
						$thumbnail = $item->large;
						break;
					case 'fullsize':
						$thumbnail = $item->fullsize;
						break;
					default:
						$thumbnail = $item->small;
				}
				echo '<div class="item item-instagram"><div class="insta-from_top_and_bottom from_top_and_bottom"><a href="' . $item->link . '" target="_blank">';
				echo '<img class="grayscale img-responsive  " src="'. $thumbnail . '" alt="instagram_image_w2_'.$iic.'" />';
				echo '</a></div></div>';
				$iic++;

			}

		}


		?>
</div>
