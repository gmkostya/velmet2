<?php
// Heading
$_['heading_title']      			= 'ProductBundles';
// Text
$_['text_module']         			= 'Modules';
$_['text_success']        			= 'Success: You have modified module ProductBundles!';
$_['text_success_activation']       = 'ACTIVATED: You have successfully activated ProductBundles!';
$_['entry_yes']						= 'Yes';
$_['entry_no']						= 'No';
$_['error_permission']    			= 'Warning: You do not have permission to modify module ProductBundles!';
?>