<?php
$_['heading_title'] = 'Фильтер';
$_['name_price'] = 'Цена';
$_['name_manufacturers'] = 'Производители';
$_['name_rating'] = 'Рейтинг';
$_['name_search'] = 'Поиск';
$_['name_stock_status'] = 'Наличие';
$_['name_location'] = 'Location';
$_['name_length'] = 'Length';
$_['name_width'] = 'Width';
$_['name_height'] = 'Height';
$_['name_weight'] = 'Weight';
$_['name_mpn'] = 'MPN';
$_['name_isbn'] = 'ISBN';
$_['name_sku'] = 'SKU';
$_['name_upc'] = 'UPC';
$_['name_ean'] = 'EAN';
$_['name_jan'] = 'JAN';
$_['name_model'] = 'Модель';
$_['name_tags'] = 'Tags';
$_['text_button_apply'] = 'Применить';
$_['text_reset_all'] = 'Сбросить';
$_['text_show_more'] = 'Show more (%s)';
$_['text_show_less'] = 'Show less';
$_['text_display'] = 'Показать';
$_['text_grid'] = 'Grid';
$_['text_list'] = 'List';
$_['text_loading'] = 'Loading...';
$_['text_select'] = 'Select...';
$_['text_go_to_top'] = 'Go to top';
$_['text_init_filter'] = 'Initialize the filter';
$_['text_initializing'] = 'Initializing...';
?>
