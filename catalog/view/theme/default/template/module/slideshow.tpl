<div id="slideshow<?php echo $module; ?>" class="home-slider">
  <?php foreach ($banners as $banner) { ?>
  <div class="item" style="background: url('<?php echo $banner['image']; ?>'); background-size: cover">
    <div class="slider-container container">
    <?php if ($banner['link']) { ?>
    <a   href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
      <?php } ?>
      <div class="slider-down-bt-curier"><span class="slider-down-bt"><i class=""><i class="path1 icon-down-arrow-2"></i><i class="path2 icon-down-arrow-2"></i><i class="path2 icon-down-arrow-2"></i></i></span></div>
  </div>
  </div>
  <?php } ?>
</div>
