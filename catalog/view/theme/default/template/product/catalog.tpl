<?php echo $header; ?>
    <div class="container container-top">
        <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <?php if ($i + 1 < count($breadcrumbs)) { ?>
                        <a itemscope itemtype="http://schema.org/Thing" itemprop="item"
                           href="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                    <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
        <?php echo $content_top; ?>
    </div>
    <div class="container">
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>">
                <?php if ($categories) { ?>
                    <div class="row row-catalog-categories">
                        <div class="curier-catalog-header-line">
                            <div class="catalog-header-line"></div>
                        </div>
                        <?php foreach ($categories as $category) { ?>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-catalog">
                                <div class="name"><a style="text-decoration: none"
                                                     href="<?php echo $category['href']; ?>"><span><?php echo $category['name']; ?></span></a>
                                </div>
                                <div class="category-thumb transition">
                                    <div class="insta-from_top_and_bottom from_top_and_bottom">
                                        <div class="image"><a href="<?php echo $category['href']; ?>"><img
                                                        src="<?php echo $category['image']; ?>"
                                                        alt="<?php echo $category['name']; ?>"
                                                        title="<?php echo $category['name']; ?>"
                                                        class="img-responsive"/></a></div>
                                    </div>
                                    <div class="caption" style="min-height: 50px">
                                        <a href="<?php echo $category['href']; ?>"><span><?php echo $text_view_all; ?></span><i
                                                    class="icon-right-arrow-1"></i></a>
                                    </div>
                                </div>
                            </div>


                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if (!$categories) { ?>
                    <p><?php echo $text_empty; ?></p>
                <?php } ?>
            </div>
        </div>
    </div>
<?php if ($content_bottom) { ?>
    <div class="curier-catalog-info curier-grey">
        <div class="container">
            <?php echo $content_bottom; ?>
        </div>
    </div>
<?php } ?>
<?php echo $footer; ?>