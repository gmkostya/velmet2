<?php
// Heading
$_['heading_title']     = 'Товары со скидкой';

// Text
$_['text_empty']        = 'В данный момент товары со скидкой недоступны';
$_['text_quantity']     = 'Кол-во:';
$_['text_manufacturer'] = 'Производитель:';
$_['text_model']        = 'Код товара:';
$_['text_points']       = 'Бонусные Баллы:';
$_['text_price']        = 'Цена:';
$_['text_tax']          = 'Без налога:';
$_['text_compare']      = 'Сравнение товаров (%s)';
$_['text_sort']         = 'Сортировать по ';
$_['text_default']      = 'По умолчанию';
$_['text_name_asc']     = ' имени - от A до Я';
$_['text_name_desc']    = ' имени  - от Я до A';
$_['text_price_asc']    = ' цене - от меньшей';
$_['text_price_desc']   = ' цене - от большей';
$_['text_rating_asc']   = ' рейтингу  - от меньшего';
$_['text_rating_desc']  = ' рейтингу  - от большего';
$_['text_model_asc']    = ' Модели (A - Я)';
$_['text_model_desc']   = ' Модели (Я - A)';
$_['text_limit']        = 'Показывать:';
