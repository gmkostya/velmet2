<?php
class ControllerToolImport extends Controller {
	private $error = array();
	private $category_id = 0;
	private $path = array();

	public function index() {
		$this->language->load('tool/import');

		echo $this->document->setTitle($this->language->get('heading_title'));


/*
 import product for lng
  /admin/index.php?route=tool/import/importproduct&lng=1&token=m7BouLJcGhQS0vyPtqWMFNwozDl3gxXR
/admin/index.php?route=tool/import/importproduct&lng=3&token=m7BouLJcGhQS0vyPtqWMFNwozDl3gxXR
/admin/index.php?route=tool/import/importproduct&lng=4&token=m7BouLJcGhQS0vyPtqWMFNwozDl3gxXR



category

/admin/index.php?route=tool/import/importcategory&target=importcategory&lng=1


 */


	}

    public function importproduct() {

        if (isset($this->request->get['target']) && $this->request->get['target']=='importproduct') {

            if (isset($this->request->get['lng'])) {
                $lng = $this->request->get['lng'];


                if ($lng==1) $lngopen=2;
                if ($lng==4) $lngopen=4;
                if ($lng==3) $lngopen=1;


                /* add colum description_short,  */


                /* real 1 -en 3 ru 4-ua */

                /* new 4-ua en-2  ru-1*/



                $sql = "INSERT INTO `oc_product_description` (`product_id`, `name`, description, meta_title,   meta_description, meta_keyword, language_id, tag)
SELECT
pp.id_product, pl.name, pl.description, pl.meta_title, pl.meta_description, pl.meta_keywords, ".$lngopen.", ''
FROM
ps_product pp
JOIN ps_product_lang pl ON pp.id_product = pl.id_product AND id_lang = ".$lng."";

                $this->db->query($sql);

                echo $lng;

            }



        }

    }



    public function importproduct_other_data() {

        if (isset($this->request->get['target']) && $this->request->get['target']=='importproduct_other_data') {


                $sql = "INSERT INTO `oc_product` (`product_id`, `price`, `minimum`)
SELECT
id_product,`price`, `minimal_quantity` FROM ps_product_shop";

                $this->db->query($sql);
                /*foto*/

                $sql = "update oc_product dd
left join ps_image ps on
dd.product_id=ps.id_product

set image=CONCAT('catalog/photo/',id_image,'.jpg') where position=1";

                echo 'foto good';

                $this->db->query($sql);


                $sql = "INSERT INTO `oc_product_to_category` (`product_id`, `category_id`)
SELECT
id_product,id_category
FROM ps_category_product";

                $this->db->query($sql);


                $sql = "INSERT INTO `oc_product_to_store` (`product_id`, `store_id`)
SELECT
id_product,id_shop
FROM ps_product_shop";

                $this->db->query($sql);


                $sql = "UPDATE `oc_product` SET STATUS =1";

                $this->db->query($sql);


                $sql = "UPDATE `oc_product_to_store` SET `store_id` =0";

                $this->db->query($sql);



                $sql = "INSERT INTO `oc_product_image` (`product_id`, `image`,sort_order )
SELECT
id_product ,CONCAT('catalog/photo/',id_image,'.jpg'),0
FROM ps_image where position>1";

                $this->db->query($sql);

            echo 'fotos good';


        }

   }

    public function importproduct_url_alias() {


        $query = $this->db->query("SELECT id_product,link_rewrite FROM ps_product_lang WHERE id_lang = '3'");

        foreach ($query->rows as $result) {
            $this->db->query("REPLACE INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$result['id_product'] . "', keyword = '" . $result['link_rewrite'] . "'");

            echo $result['link_rewrite'];

         };
    }
    public function importproduct_qty() {

            $query = $this->db->query("SELECT * FROM ps_product_sale");

            foreach ($query->rows as $result) {
                $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '". (int)$result['quantity'] . "' WHERE product_id = '".(int)$result['id_product']."'");

            };
        }

    public function importproduct_comment() {

            $query = $this->db->query("SELECT * FROM ps_product_comment");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT " . DB_PREFIX . "review SET  product_id = '".(int)$result['id_product']."',  customer_id = '". (int)$result['id_customer'] . "', author = '". $result['customer_name'] . "', text = '". $result['content'] . "', rating = '". (int)$result['grade'] . "', status = '1', date_added = '". $result['date_add'] . "'");

            };
        }


    public function importproduct_default_category() {


        $query = $this->db->query("SELECT id_product,id_category_default, id_manufacturer, price, wholesale_price, active FROM ps_product");

        foreach ($query->rows as $result) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET manufacturer_id = '". (int)$result['id_manufacturer'] . "', price = '" . $result['price'] . "', status = '" . $result['active'] . "' WHERE product_id = '".(int)$result['id_product']."'");

            $this->db->query("UPDATE " . DB_PREFIX . "product_to_category SET main_category = '1' WHERE product_id = '".(int)$result['id_product']."' AND  category_id = '".$result['id_category_default']."'");



        };
    }


    public function importcategory() {

        if (isset($this->request->get['target']) && $this->request->get['target']=='importcategory') {

            if (isset($this->request->get['lng'])) {
                $lng = $this->request->get['lng'];

                if ($lng==1) $lngopen=2;
                if ($lng==4) $lngopen=4;
                if ($lng==3) $lngopen=1;


                /* add colum description_short,  */


                /* real 1 -en 3 ru 4-ua */

                /* new 4-ua en-2  ru-1*/


                $sql =  "INSERT INTO `oc_category_description` ( `category_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `language_id`, `link_rewrite`)

SELECT
cl.id_category,cl.name,cl.description, cl.meta_title,  cl.meta_description, cl.meta_keywords,".$lngopen.", link_rewrite
FROM
ps_category_lang cl
JOIN ps_category cat ON cat.id_category = cl.id_category AND id_lang = ".$lng."";


                $this->db->query($sql);
                echo $lng;

            }



        }

    }

    public function importcategory_other_data() {

        if (isset($this->request->get['target']) && $this->request->get['target']=='importcategory_other_data') {


            $sql = "INSERT INTO `oc_category` (`parent_id`, `category_id`, status)

SELECT
cat.id_parent, cl.id_category,1
FROM
ps_category_lang cl
JOIN ps_category cat ON cat.id_category = cl.id_category AND id_lang = 1";

            $this->db->query($sql);
            /*foto*/



        }

    }
    public function importcategory_other_data2() {

        if (isset($this->request->get['target']) && $this->request->get['target']=='importcategory_other_data2') {

            $sql = "INSERT INTO `oc_category_to_store` (`category_id`, `store_id`) SELECT id_category ,0 FROM ps_category ";

            $this->db->query($sql);
            /*foto*/



        }

    }

    public function importcategory_url_alias() {


        $query = $this->db->query("SELECT id_category,link_rewrite FROM ps_category_lang WHERE id_lang = '3'");

        foreach ($query->rows as $result) {
            $this->db->query("REPLACE INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$result['id_category'] . "', keyword = '" . $result['link_rewrite'] . "'");

            echo $result['link_rewrite'];

        };
    }
/**customer info*/
    public function importcustomer() {

        $query = $this->db->query("SELECT * FROM ps_customer");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT " . DB_PREFIX . "customer SET  customer_id = '".(int)$result['id_customer']."',  customer_group_id = '1', firstname = '". $result['firstname'] . "', lastname = '". $result['lastname'] . "', email = '". $result['email'] . "', newsletter = '". (int)$result['newsletter'] . "', status = '". $result['active'] . "', date_added = '". $result['date_add'] . "'");

        };
    }

    /***/








}
