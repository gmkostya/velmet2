<div class="gallery-slider-curier">
    <div class="container"><div class="block-title"><span><?php  echo  $text_gallery; ?></span></div></div>
<div class="gallery-slider">
	<?php if ($gallery_array) { ?>
		<?php foreach ($gallery_array as $item) { ?>
		<?php if ($item['type']=='v')  { ?>
               <div class="item item-video" data-videoid="<?php echo $item['id_video']; ?>">
			    		<div >
							<span class="video-blur" style="background:url(<?php echo $item['thumb']; ?>); background-size:cover"></span>
                                 <img src="<?php echo $item['thumb2']; ?>" alt="<?php echo $item['text']; ?>" class="brik" />
								 <span class="slider-brik-video">
                                 	 <div id="<?php echo $item['id_video']; ?>"><?php echo $item['code']; ?></div>
                                 </span>
						</div>
				</div>
		<?php } else { ?>
				<div class="item item-gallery">
                		<div style="background:url(<?php echo $item['thumb']; ?>); background-size:cover">
							<img src="<?php echo $item['thumb2']; ?>" alt="<?php echo $item['text']; ?>" class="brik" />
						</div>
            	</div>
		<?php } ?>
		<?php } ?>
	<?php } ?>
</div>
    <div class="container container-see-more"><a class="btn btn-green btn-see-more" href="<?php echo $see_more; ?>"><span><?php echo $text_see_more; ?></span><i class="icon-rectangle-2-copy-3"></i></a></div>
</div>
