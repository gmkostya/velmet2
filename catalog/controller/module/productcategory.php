<?php
class ControllerModuleProductCategory extends Controller {
	public function index() {
		$this->load->language('module/category');

		$data['text_see_all'] = $this->language->get('text_see_all');
        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');

        $data['text_see_all'] = $this->language->get('text_see_all_novelty');

        $this->load->model('catalog/category');

		$this->load->model('catalog/product');

        $categories = $this->model_catalog_category->getCategories(2);

        $this->load->model('tool/image');
        $data['categories'] = array();
        foreach ($categories as $category) {
            if ($category['catalog']) {
                $img_w = '370';
                $img_h = '530';

                if ($category['image']) {
                    $imagecategory = $this->model_tool_image->resize($category['image'], $img_w, $img_h);
                } else {
                    $imagecategory = $this->model_tool_image->resize('placeholder.png', $img_w, $img_h);
                }

                $products = array();
                $filter_data = array(
                    'filter_category_id' => $category['category_id'],
                    'filter_filter'      => '',
                    'sort'               => '',
                    'order'              => '',
                    'start'              => 0,
                    'limit'              => 24
                );


                $results = $this->model_catalog_product->getProducts($filter_data);



                foreach ($results as $result) {
                    if ($result['image']) {
                        $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                    }

                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $price = false;
                    }

                    if ((float)$result['special']) {
                        $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = (int)$result['rating'];
                    } else {
                        $rating = false;
                    }

                    $data['options'] = array();

                    foreach ($this->model_catalog_product->getProductOptions( $result['product_id']) as $option) {
                        $product_option_value_data = array();

                        foreach ($option['product_option_value'] as $option_value) {
                            if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {

                                $product_option_value_data[] = array(
                                    'product_option_value_id' => $option_value['product_option_value_id'],
                                    'option_value_id'         => $option_value['option_value_id'],
                                    'name'                    => $option_value['name'],
                                    'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                    'price_prefix'            => $option_value['price_prefix']
                                );
                            }
                        }

                        $data['options'][] = array(
                            'product_option_id'    => $option['product_option_id'],
                            'product_option_value' => $product_option_value_data,
                            'option_id'            => $option['option_id'],
                            'name'                 => $option['name'],
                            'type'                 => $option['type'],
                            'value'                => $option['value'],
                            'required'             => $option['required']
                        );
                    }

                    $label=false;
                    $label_type =false;

                    if ($result['upc']=='new') {
                        $label = $this->language->get('text_new');
                        $label_type ='new';
                    }
                    if ($special) {
                        $label = $this->language->get('text_sale');
                        $label_type ='sale';
                    }
                    if (!empty($result['special_end_date'])) {
                        $special_end_date = explode('-', $result['special_end_date']);
                        $count_end_date =$special_end_date[1].', '.$special_end_date[2].', '.$special_end_date[0];
                    } else
                        $count_end_date = false;


                    if (isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist'])) {
                        $wishlist_status = '1';
                    } else {
                        $wishlist_status = false;
                    }



                    $products[] = array(
                        'product_id'  => $result['product_id'],
                        'thumb'       => $image,
                        'name'        => $result['name'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                        'price'       => $price,
                        'special'     => $special,
                        'count_end_date'     => $result['special_end_date'],
                        'options'     => $data['options'],
                        'label'         => $label,
                        'label_type'         => $label_type,
                        'wishlist_status' => $wishlist_status,
                        'tax'         => $tax,
                        'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
                        'rating'      => $result['rating'],
                        'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                    );
                }



                $data['categories'][] = array(
                    'description' => $category['description'],
                    'name' => $category['name'],
                    'image' => $imagecategory,
                    'products' => $products,
                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
        }

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/productcategory.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/productcategory.tpl', $data);
		} else {
			return $this->load->view('default/template/module/productcategory.tpl', $data);
		}
	}
}
