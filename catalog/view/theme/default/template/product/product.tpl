<?php echo $header; ?>
<div class="container container-top">
    <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <?php if ($i + 1 < count($breadcrumbs)) { ?>
                    <a itemscope itemtype="http://schema.org/Thing" itemprop="item"
                       href="<?php echo $breadcrumb['href']; ?>">
                        <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <?php echo $column_right; ?>
</div>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <div class="row">
                    <?php $class = 'col-sm-5'; ?>
                <div class="col-sm-5 col-product-left">
                    <?php if ($images) { ?>
                        <div class="row-images row">
                            <div class="slider-image-big-curier">
                                <div class="slider-image-big">
                                    <?php $k = '';
                                    foreach ($images as $image) {
                                        $k++; ?>
                                        <div class="item "><img src="<?php echo $image['thumb']; ?>"
                                                                data-link="image-<?php echo $k; ?>"
                                                                class="img-responsive"
                                                                title="<?php echo $heading_title; ?>"
                                                                alt="<?php echo $heading_title; ?>"/></div>
                                    <?php } ?>
                                </div>
                                <button class="click-triger-zoom btn-zoom"><i class="icon-zoom-in"></i></button>
                            </div>

                            <div class="slider-image-additional">
                                <?php foreach ($images as $image) { ?>
                                    <div class="item"><img src="<?php echo $image['thumb']; ?>" class="img-responsive"
                                                           title="<?php echo $heading_title; ?>"
                                                           alt="<?php echo $heading_title; ?>"/></div>
                                <?php } ?>
                            </div>
                            <ul id="lightgallery" style="display: none">
                                <li data-src="<?php echo $popup; ?>" id="image-0">
                                    <a href="">
                                        <img src="<?php echo $popup; ?>" alt="<?php echo $heading_title; ?>">
                                    </a>
                                </li>
                                <?php $k = '';
                                foreach ($images as $image) {
                                    $k++; ?>
                                    <li data-src="<?php echo $image['popup']; ?>" id="image-<?php echo $k; ?>">
                                        <a href="">
                                            <img src="<?php echo $image['popup']; ?>" alt="<?php echo $heading_title; ?>">
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-sm-6 col-sm-offset-1 col-product-right">
                    <div class="curier-product-title row">
                        <div class="col-md-12 col-md-model"><span><?php echo $model; ?></span></div>
                        <div class="col-md-12">
                            <?php if ($review_status) { ?>
                                <div class="rating">

                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <?php if ($rating < $i) { ?>
                                            <span class=""><i class="icon-star"></i></span>
                                        <?php } else { ?>
                                            <span class=""><i class="icon-star_red"></i></span>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="col-md-12"><h1><?php echo $heading_title; ?></h1></div>

                    <?php if ($count_end_date) { ?>
                        <div class="col-md-12 col-timer-curier">
                        <div class="timer-curier ">
                            <div class="timer" data-timeend="<?php echo $count_end_date; ?>" data-timetext="<?php echo $heading_title; ?>"></div>
                            <div class="timer-text" ><span class="t-day" >дней</span><span class="t-hour">часов</span><span class="t-min">мин</span><span class="t-sec">сек</span></div>
                        </div>
                        </div>
                    <?php } ?>
                    <?php if ($price) { ?>
                        <div class="col-price col-md-12">
                            <div class="col-price-value">
                                <?php if (!$special) { ?>
                                    <span class="price"><?php echo $price; ?></span>
                                <?php } else { ?>
                                    <span class="price-new"><?php echo $special; ?></span>
                                    <span class="price-old"><?php echo $price; ?></span>
                                <?php } ?>
                            </div>
                            <?php if ($tax || $points || $discounts) { ?>
                                <ul class="list-unstyled price-info">
                                    <?php if ($tax) { ?>
                                        <li><?php echo $text_tax; ?><?php echo $tax; ?></li>
                                    <?php } ?>
                                    <?php if ($points) { ?>
                                        <li><?php echo $text_points; ?><?php echo $points; ?></li>
                                    <?php } ?>
                                    <?php if ($discounts) { ?>
                                        <li>
                                            <hr>
                                        </li>
                                        <?php foreach ($discounts as $discount) { ?>
                                            <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    </div>

                    <div id="product">
                        <?php if ($quantity > 0) { ?>
                        <?php if ($options) { ?>
                            <div class="options">
                                <?php foreach ($options as $option) { ?>
                                    <?php if ($option['type'] == 'select') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"
                                                   for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <select name="option[<?php echo $option['product_option_id']; ?>]"
                                                    id="input-option<?php echo $option['product_option_id']; ?>"
                                                    class="form-control select-default">
                                                <option value=""><?php echo $text_select; ?></option>
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <option
                                                        value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                    <?php } ?>
                                    <?php if ($option['type'] == 'radio') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?> </label>
                                            <?php if ($option['option_id']==9) { ?>
                                                <div class="col-size-link"><a class="js-popup-link" data-link="size-table-modal"><i
                                                            class="icon-combined-shape"></i><span><?php echo $text_size_table; ?></span></a></div>
                                            <?php } ?>

                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class="col-option option-id-<?php echo $option['option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="radio">
                                                        <input type="radio" id="<?php echo $option_value['product_option_value_id']; ?>"
                                                               name="option[<?php echo $option['product_option_id']; ?>]"
                                                               value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                        <label for="<?php echo $option_value['product_option_value_id']; ?>">
                                                            <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>

                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'checkbox') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"
                                                                   name="option[<?php echo $option['product_option_id']; ?>][]"
                                                                   value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                            <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'image') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class=" col-option option-id-<?php echo $option['option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="radio">
                                                        <input type="radio"
                                                               name="option[<?php echo $option['product_option_id']; ?>]"
                                                               value="<?php echo $option_value['product_option_value_id']; ?>" id="option_value-<?php echo $option_value['product_option_value_id']; ?>"/>
                                                        <label for="option_value-<?php echo $option_value['product_option_value_id']; ?>">
                                                            <img src="<?php echo $option_value['image']; ?>"
                                                                 alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                                                                 class="img-radio"/>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'text') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"
                                                   for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <input type="text"
                                                   name="option[<?php echo $option['product_option_id']; ?>]"
                                                   value="<?php echo $option['value']; ?>"
                                                   placeholder="<?php echo $option['name']; ?>"
                                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                                   class="form-control"/>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'textarea') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"
                                                   for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <textarea name="option[<?php echo $option['product_option_id']; ?>]"
                                                      rows="5" placeholder="<?php echo $option['name']; ?>"
                                                      id="input-option<?php echo $option['product_option_id']; ?>"
                                                      class="form-control"><?php echo $option['value']; ?></textarea>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'file') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <button type="button"
                                                    id="button-upload<?php echo $option['product_option_id']; ?>"
                                                    data-loading-text="<?php echo $text_loading; ?>"
                                                    class="btn btn-default btn-block"><i
                                                    class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                            <input type="hidden"
                                                   name="option[<?php echo $option['product_option_id']; ?>]" value=""
                                                   id="input-option<?php echo $option['product_option_id']; ?>"/>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'date') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"
                                                   for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group date">
                                                <input type="text"
                                                       name="option[<?php echo $option['product_option_id']; ?>]"
                                                       value="<?php echo $option['value']; ?>"
                                                       data-date-format="YYYY-MM-DD"
                                                       id="input-option<?php echo $option['product_option_id']; ?>"
                                                       class="form-control"/>
                                                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'datetime') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"
                                                   for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group datetime">
                                                <input type="text"
                                                       name="option[<?php echo $option['product_option_id']; ?>]"
                                                       value="<?php echo $option['value']; ?>"
                                                       data-date-format="YYYY-MM-DD HH:mm"
                                                       id="input-option<?php echo $option['product_option_id']; ?>"
                                                       class="form-control"/>
                                                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'time') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"
                                                   for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group time">
                                                <input type="text"
                                                       name="option[<?php echo $option['product_option_id']; ?>]"
                                                       value="<?php echo $option['value']; ?>" data-date-format="HH:mm"
                                                       id="input-option<?php echo $option['product_option_id']; ?>"
                                                       class="form-control"/>
                                                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <?php } ?>
                        <?php if ($attribute_groups) { ?>
                            <div class="col-sm-12 col-attr">
                            <?php foreach ($attribute_groups as $attribute_group) { ?>
                                <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                    <div class="attribute"><span class="attribute-name"><?php echo $attribute['name']; ?>:</span>
                                        <span><?php echo $attribute['text']; ?></span></div>
                                <?php } ?>
                            <?php } ?>
                                </div>
                        <?php } ?>


                        <?php if ($recurrings) { ?>
                            <hr>
                            <h3><?php echo $text_payment_recurring ?></h3>
                            <div class="form-group required">
                                <select name="recurring_id" class="form-control">
                                    <option value=""><?php echo $text_select; ?></option>
                                    <?php foreach ($recurrings as $recurring) { ?>
                                        <option
                                            value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block" id="recurring-description"></div>
                            </div>
                        <?php } ?>
                        <div class="col-qty  <?php if ($quantity < 1) { ?> no-avaible <?php } ?>">
                            <?php if ($quantity > 0) { ?>
                            <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
                            <div class="qty-input">
                                <button class="btn-qty qty-m" onclick="EditMinQuant('<?php echo $product_id; ?>');">-
                                </button>
                                <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2"
                                       id="input-quantity"
                                       class="form-control input-qty item-<?php echo $product_id; ?>"/>
                                <button class="btn-qty qty-p" onclick="EditMaxQuant('<?php echo $product_id; ?>');">+
                                </button>
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                            </div>
                            <?php } ?>
                            <div
                                class="col-stock <?php if ($quantity < 1) { ?> text-red <?php } ?>" ><?php echo $stock; ?></div>
                        </div>
                        <div class="col-product-bts">
                            <?php if ($quantity > 0) { ?>
                                <div class="col-product-bt">
                                    <button type="button" class="btn  btn-green  btn-one-click"
                                            onclick="get_popup_purchase('<?php echo $product_id; ?>');"><span><?php echo $button_one_click; ?></span>
                                    </button>
                                </div>

                                <div class="col-product-bt">
                                <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>"
                                        class="btn btn-primary btn-to-cart"><i
                                        class="icon-cart"></i><span><?php echo $button_cart; ?></span>
                                </button>
                            </div>
                            <?php } ?>
                            <div class="col-product-bt">
                                                         <?php if ($wishlist_status){ ?>
                                    <button type="button" data-toggle="tooltip"
                                            title="<?php echo $button_wishlist_is; ?>"
                                            onclick="wishlist.add('<?php echo $product_id; ?>');"
                                            class="btn btn-to-wishlist btn-primary red js-wishlist-<?php echo $product_id; ?>"><i
                                            class="icon-forma-1-copy-5"></i></button>
                                    </button>
                                <?php } else { ?>
                                    <button type="button" class="btn btn-primary btn-to-wishlist js-wishlist-<?php echo $product_id; ?>"
                                            title="<?php echo $button_wishlist; ?>"
                                            onclick="wishlist.add('<?php echo $product_id; ?>');"><i
                                            class="icon-forma-1-copy-5"></i></button>
                                <?php } ?>
                            </div>
                        </div>
                        <?php if ($minimum > 1) { ?>
                            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?>
                            </div>
                        <?php } ?>
                        <div class="col-sm-12 col-share">
                            <span class="/"><?php echo $text_share; ?></span>
                            <div class="pluso" data-background="transparent"
                                 data-options="small,square,line,horizontal,nocounter,theme=02"
                                 data-services="facebook,twitter,google"></div>
                        </div>

                    </div>
                </div>
               <?php if($content_bottom) { ?><div class="col-bottom col-sm-12"><?php echo $content_bottom; ?></div><?php } ?>
               <div class="col-sm-12 col-tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
                    <?php if ($attribute_groups) { ?>
                        <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                    <?php } ?>
                    <?php if ($review_status) { ?>
                        <li><a href="#tab-review" class="tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
                    <?php } ?>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-description"><?php echo $description; ?></div>
                    <?php if ($attribute_groups) { ?>
                        <div class="tab-pane" id="tab-specification">
                            <table class="table table-bordered">
                                <?php foreach ($attribute_groups as $attribute_group) { ?>
                                    <thead>
                                    <tr>
                                        <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                        <tr>
                                            <td><?php echo $attribute['name']; ?></td>
                                            <td><?php echo $attribute['text']; ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } ?>
                    <?php if ($review_status) { ?>
                        <div class="tab-pane" id="tab-review">
                            <div id="review"></div>
                            <div class="col-sm-8 col-review-form">
                                <form class="form-horizontal" id="form-review">
                                <h2><?php echo $text_write; ?></h2>
                                <?php if ($review_guest) { ?>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                            <input type="text" name="name" value="" id="input-name" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                                            <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                                                                                   </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="col-sm-12 col-rating">
                                            <label class="control-label"><?php echo $entry_rating; ?></label>
                                            &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                                            <input type="radio" name="rating" value="1" />
                                            &nbsp;
                                            <input type="radio" name="rating" value="2" />
                                            &nbsp;
                                            <input type="radio" name="rating" value="3" />
                                            &nbsp;
                                            <input type="radio" name="rating" value="4" />
                                            &nbsp;
                                            <input type="radio" checked name="rating" value="5" />
                                            &nbsp;<?php echo $entry_good; ?></div>
                                    </div>

                                    <div class="buttons clearfix">
                                        <div class="pull-left pull-button-send">
                                            <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><span><?php echo $button_submit; ?></span></button>
                                        </div>
                                        <?php echo $captcha; ?>
                                    </div>

                                <?php } else { ?>
                                    <?php echo $text_login; ?>
                                <?php } ?>
                            </form>
                        </div>
                        </div>
                    <?php } ?>
                </div>
                </div>

            </div>
            <?php if ($products) { ?>
                    <div class="row row-relatet-product">
                        <?php $i = 0; ?>
                        <div class="col-relatet-product col-sm-12 product-box-slider product-box">
                            <div class="block-title-2"><span><?php echo $text_related; ?></span></div>
                            <div class="js-relatet-product">
                        <?php foreach ($products as $product) { ?>
                            <div class="product-layout">
                                <div class="product-thumb transition">
                                    <div class="image">
                                        <?php if ($product['label']) { ?>
                                            <div class="product-label <?php echo  $product['label_type']; ?>"><?php echo $product['label']; ?></div>
                                            <?php if ($product['count_end_date']) { ?>
                                                <div class="timer-curier">
                                                    <div class="timer" data-timeend="<?php echo $product['count_end_date']; ?>" data-timetext="<?php echo $product['name']; ?>"></div>
                                                    <div class="timer-text" ><span class="t-day" >дней</span><span class="t-hour">часов</span><span class="t-min">мин</span><span class="t-sec">сек</span></div>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                        <a href="<?php echo $product['href']; ?>">
                                            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                                        </a>
                                    </div>
                                    <div class="caption">
                                        <div class="rating">
                                            <?php if ($product['rating']) { ?>
                                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                    <?php if ($product['rating'] < $i) { ?>

                                                    <?php } else { ?>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="name" ><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                                    </div>
                                    <div class="button-group">
                                        <?php if ($product['price']) { ?>
                                            <div class="price">
                                                <?php if ($product['special']) { ?>
                                                    <span class="price-old"><?php echo $product['price']; ?></span>
                                                    <span class="price-new"><?php echo $product['special']; ?></span>
                                                <?php } else { ?>
                                                    <?php echo $product['price']; ?>
                                                <?php } ?>
                                                <?php if ($product['tax']) { ?>
                                                    <span
                                                            class="price-tax"><?php echo $text_tax; ?><?php echo $product['tax']; ?></span>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>

                                        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="product-layout-button-cart"><i class="icon-cart"></i><span><?php echo $button_cart; ?></span></button>
                                        <?php if ($product['wishlist_status']){ ?>
                                            <button type="button" data-toggle="tooltip"
                                                    title="<?php echo $button_wishlist_is; ?>"
                                                    onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                                    class="product-layout-button-wishlist red js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-forma-1-copy-5"></i>
                                            </button>
                                        <?php } else { ?>
                                            <button type="button" data-toggle="tooltip"
                                                    title="<?php echo $button_wishlist; ?>"
                                                    onclick="wishlist.add('<?php echo $product['product_id']; ?>');"
                                                    class="product-layout-button-wishlist js-wishlist-<?php echo $product['product_id']; ?>"><i class="icon-forma-1-copy-5"></i>
                                            </button>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    </div>
                </div>
            <?php } ?>
            <?php echo $content_top; ?>
            <?php if ($tags) { ?>
                <p><?php echo $text_tags; ?>
                    <?php for ($i = 0; $i < count($tags); $i++) { ?>
                        <?php if ($i < (count($tags) - 1)) { ?>
                            <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
                        <?php } else { ?>
                            <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
                        <?php } ?>
                    <?php } ?>
                </p>
            <?php } ?>
            </div>
    </div>
</div>
<?php if ($size_popup_info) { ?>
    <div class="modal fade size-table-modal" id="size-table-modal"  tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close"><span aria-hidden="true"
                                                     class="icon-close"> </span>
                    </button>
                    <h3 class="modal-title"><?php echo $size_popup_heading_title; ?></h3>
                </div>
                <div class="modal-body">
                   <?php echo $size_popup_info; ?>
                </div>
              </div>
        </div>
    </div>
<?php } ?>
<script type="text/javascript">(function () {
        if (window.pluso)if (typeof window.pluso.start == "function") return;
        if (window.ifpluso == undefined) {
            window.ifpluso = 1;
            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
            s.type = 'text/javascript';
            s.charset = 'UTF-8';
            s.async = true;
            s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
            var h = d[g]('body')[0];
            h.appendChild(s);
        }
    })();</script>
<script type="text/javascript"><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function () {
                $('#recurring-description').html('');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('#button-cart').on('click', function () {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-cart').button('loading');
            },
            complete: function () {
                $('#button-cart').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    // $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#cart-total').html(json['total']);

                    $('#cart').addClass('cart-full');
                    //$('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');

                    $('#cart > button').trigger('click');
                    setTimeout(function () {
                        $('body').trigger('click');
                    }, 3000)

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('button[id^=\'button-upload\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script type="text/javascript"><!--

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function () {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function () {
                $('#button-review').button('loading');
                masked('#send_review-modal .modal-content', true);
            },
            complete: function () {
                $('#button-review').button('reset');
            },
            success: function (json) {
                masked('#send_review-modal .modal-content', false);
                $('.alert-success, .alert-danger').remove();

                if (json['error']) {
                    $('#form-review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#form-review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);

                    setTimeout(function () {
                        $('#send_review-modal').modal('hide');
                    }, 4000);

                }
            }
        });
    });

/*    $(document).ready(function () {
        $('.thumbnails').magnificPopup({
            type: 'image',
            delegate: 'a',
            gallery: {
                enabled: true
            }
        });
    });*/
    //--></script>

<?php echo $footer; ?>
