<?php
// Text
$_['text_refine']       = 'Уточнити Пошук';
$_['text_product']      = 'Товари';
$_['text_error']        = 'Категорія не знайдена!';
$_['text_empty']        = 'У цій категорії немає товарів.';
$_['text_quantity']     = 'Кількість:';
$_['text_manufacturer'] = 'Виробник:';
$_['text_model']        = 'Модель:';
$_['text_points']       = 'Бонусні Бали:';
$_['text_price']        = 'Ціна:';
$_['text_tax']          = 'Без податку:';
$_['text_compare']      = 'Порівняння товарів (%s)';
$_['text_sort']         = 'Сортувати за ';
$_['text_default']      = ' замовчуванням';
$_['text_name_asc']     = ' iм’ям (A - Я)';
$_['text_name_desc']    = ' iм’ям (Я - A)';
$_['text_price_asc']    = ' ціною (зростання)';
$_['text_price_desc']   = ' ціною (зменшення)';
$_['text_rating_asc']   = ' рейтингом (зростання)';
$_['text_rating_desc']  = ' рейтингом (зменшення)';
$_['text_model_asc']    = ' Моделлю (A - Я)';
$_['text_model_desc']   = ' Моделлю (Я - A)';
$_['text_limit']        = 'На сторінці:';
