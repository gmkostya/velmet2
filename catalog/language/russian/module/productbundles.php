<?php
// Heading 
$_['heading_title']  = 'Вместе дешевле';

$_['ProductBundles_Title'] = $_['heading_title'];
$_['ProductBundles_BundlePrice'] = "Цена комплекта:";
$_['ProductBundles_YouSave'] = "Економия";
$_['ProductBundles_AddBundleToCart'] = "В корзину";

$_['text_breadcrumb']  = 'Продукты комплекты';
$_['listing_heading_title']  = 'Новый комплект';


$_['text_option']  = 'Доступные опции';
$_['text_option_heading'] = 'Некоторіе товары имеют обязательные опции';
$_['error_required']  = 'Обязательное поле!';
$_['AdditionalFees']  = 'There may be some additional fees depending on the options that you choose.';
$_['Continue']  = 'Продолжить';

$_['view_bundle'] = 'Просмотреть комплект';
?>