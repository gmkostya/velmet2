<style>
	#recent-view{
		position:fixed;
		bottom:30px;
		left:30px;
		cursor:pointer;
	}
	
	.my-hr{
		margin-top:3px;
		margin-bottom:5px;
	}
</style>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Recent View</h4>
		</div>
		<div class="modal-body">
	  <?php 
			$count = 0;
			foreach($products as $product){
				if($count < $number && $product['name'] !=""){
					echo '<div class="row">';
					echo '<div class="col-xs-3"><img src="'.$product['img'].'" class="img-responsive thumbnail" style="margin-bottom:2px;"></div>';
					echo '<div class="col-xs-9"><h4><a href="'.$product['href'].'">'.$product['name'].'</a></h4></div>';
					echo '</div><hr class="my-hr">';
				}
				$count++;
			} 
	  ?>
		</div>
    </div>
  </div>
</div>

<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD4AAAA9CAYAAAD1VdrqAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4BpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDoyM0FERDgwNTc3ODNFNDExQTZCN0Y0QjcwRDFGMTZBNyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo0N0I1MEE5ODkzNDkxMUU0QUI3N0I2OUE0OUE0QkVDNiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo0N0I1MEE5NzkzNDkxMUU0QUI3N0I2OUE0OUE0QkVDNiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNCAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo1NmYyMTYzZC0xYjQ1LTllNDctOTc1OC04ZjZiYzEyNTNmZjEiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDpkZDliMGJjZS04OTllLTExZTQtYjQyOC1mYzcyYjUyOGViMGYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6mPCukAAAI2UlEQVR42uxbXUxURxSeu7tg+bH8Kz/+gFWQpuVBtFIBrfbnQWlNTRpI9LW+KdI2qZL0RTRtH1SiqSnYBxE1WmMTW4p/AZNajVatxlJca4MWAZPSponIYmF3p+fcnCHjZe7uvbsLrLGTfLl79965c747Z845M3OuxiamaDbv59EmUCjP1Wy0xy38jjrimuKomfwfiLR85Cb/h11cESZsFXI9FdFAiMiQ0CJM2KE4OgKQZwFI+wlcceThaoAWQcKCoFM6d9C5pngBZsQFOZ9E3k/nxhcS8gtwhUnaSFCG+G8aoBSwADAXMAOQBoijZw0B/gb0AH4H/Ay4APhTIu8zQJNegCCt2SGv2SSt6mVB1CUdMwDvAVYDXqqsrNRKSkrY/PnzWV5eHsvIyGAJCQn6QwcHB1l/fz+7e/cuc7vd7NKlS+zo0aNIoANwAnAM0A/wEmmv9AJUvc8jbf0FyRjAFEA8YCoghYhmAYoBXwE8NTU1/MqVK9zv93O7BetgXXwGPgvQBFhIbWRQm1NJhikkk1MaTuNC+jlq8HlAKmA6IBfwKXbg1q1b+aNHj3ikCmgEr6urwxcwSG3kUpupJEM8yRRR8irSqKNJgHTqgdcAv27YsIF3dXXx8Sr47I0bN+IL6AS8QW2nkywJkSSvIp0ISCZ1ywG8DxjYuXMnn6iyZ88e0fvrSYYMkikxEuQ1yTXJPS2TrkVDc+rUKT7R5fTp05yMXK2BvLHnNbvkRW+7JEOWJJH+ZP369fz69et8sgq2jTKgLBL5JMnguez2uqzisZIhS6cGUMW8N2/e5JNdUAbqeaH26ZLBi7Wj8ioVn0oWNIuMyuDJkyd5tBSUhca8MHipJLMtlTeqeAL5THQfLwBu7d69m0dbQZlQNpJxOsmcYEflNUnF40htMijM/AzdSbQWcnWfk6wZJHucpPKald4WrkuoeAmq03j66XALhLxC5UsklRcubkyvO02MmoPGRwy9sVqInopXr17NorUkJyczl8sVc+7cOTRs7YapLDPG8XL3OwwBSyyNkWycLXk8nilxcXEsmsvQ0BCLj4//l2aDfQD8PQwYMUxqRqelmqHXHdJMa82mTZvCJj0yMsKuXr3KLly4wG7dusUePHigC4rPzcrKYoWFhay0tJQtWrQIey6kNvBZKGt9ff0aOP2SXJ3g45c4cpVRi6coSEw+OkDgkMceEOYHDhzg5eXlHHoj4LISXl+6dClvbm7W64VScFZHU1oxmUk2+HXNaNiECxNGDVX8FXxToUwtsXR2dvKVK1eaEo2NjTW9tmrVKr1+KFNa6t1XiYMwcsK1PUHcIQUswoXNBNRUVVWFRLqlpYVnZ2ePIZSTk8Nra2s5GCHe0dGhH/Hc7N7W1lbbba9duxbrf0gchGsTAY3DaM1jyO8l0XLRbFwA2LVrl+2Gz5w5w1NSUsYQQTW+ffu2so7b7daHg7FOamoqP3v2rK32UWZavJhNXJKIW4zs1gRxeXxnAvIA5+2Gp0hg5syZYwgUFBTw+/fv6/c8fPiQ79+/n2/ZsoU3NTXp51jwen5+/pi6s2bNMn1hqoJaAvV+IA6ZhnH+BHFh2ESIigHAHMAfd+7csdzg8PCwckxrmsb37dun3wPWnC9fvvyJ63iO/2NpbGzU71eNeasGD2VG2YlDlhTCPmHgBHGjYcMV0UeiN6yUY8eOKQ0Vjt++vj79nurqauU94Ib06z09PRzcm/Ke48ePW5IDZUbZKXY3GjhT4mk0xUPittS8rKxMKfDChQv16wMDA7yoqEh5D/6P17Hg/ap70AZYLVRnLnFJMxJ3WNzse6oKDDlG0ZopH0eQZwxCL1huENRV+T+ouR6pJSYmMhjPyntWrFihX8d7e3t7bT3fWEhmj5XFBzNV77Zj3ND4BDNuONaNxg1Ijxq3hoYGpXGrqKiwa9y6A6l6MOP2Y6TcGbqp7u7uUeODYSwGLngUBhSvz5s3L1Lu7LxV46ZyZ82hBDAYcGDgoTJO+GJUBSYuygAmLS2Nt7W1hRLANFtxZ2YBzEehhqyoKTNmzFC6Ngxc2tvb9ZAVj5s3b1a6MKwfyvL1unXrsP7HVgIYs5C1LJxJCvYijs1QJilYD+uHMUlZFixkDTRJycctm3CmpV6vlx88eFCP061MS5ctW8YPHTqk1wuloKy0zZQfaJLiMtmYl/eivwPBC4uLi0PyqU6nk8FsiVVWVjKYK7OLFy+OLkR4PB5cMRldiFiyZElYCxFYDh8+jIfvDRzGJBJoivU2l7TsFEfuoO0pW3p6HdBLSQdi+ckrvwRHgHQM0fO4KX9ix44dUR+xkYwnSGZfgLQRS4uNcWQkWrq6uuIxoyEay7179zDbAqO1CpqZDQVabDTrcZ+UdjFCatNQX18ftb0NvhsPjSTriCFlJGCaSKANBezmIoB7IvfCbQYsbpIxz8qGgp0tJAxhV+E8N0o3DSukrCpbW0jBNg1xnBcAqlF9omib2EcyFZCMljYNnUHyUVX/oeEY8Pl85TAZYZmZmZMypm/cuMH27t3Lrl27hglB30iGTLgur8Gih50YgIF/IaAO3zamZUx0wVVc6uk6kmVOuIkBVlNBBPkaHPOTkPyDbusDA+mwU0GsJv9gg/MBb6NFxUVEsXw8HgUXIWmh8jfAu9T2nEgn/1hN98olo4J5ql8AHm/bti3iCX7bt29Hwo+pjQXUZu54pHvZSfCbTW7kRcCbgEMipRNnSqGmdGJdSunECOxrwFvUxlxqM6wEP82G2hsTB1yEGAVQoHcAK7F3qqqqtMWLF+szMJHEiwuLWEA7RpN4cdZ2+fJlduTIESR8G9AK+BbwF0VjRniDzcLCzV4OlrXsMrwIl5TNPI12XV+mqCqb/Kyctv0PbeTfBfwC+ElK2/ZKobPXAF+wychEp227THLXHSxwor7fJD/da0jb9gfoZUs+286MnyuS4o0COyXhHAbiqk80GDP/JMMXIFHfr+hlNl6J+qpI7qn9NOP/j3HCJM/YxH1+FZEP8Z7ZD+6e2U8s/xNgAJVPfeLpfzzVAAAAAElFTkSuQmCC" id="recent-view" data-toggle="modal" data-target=".bs-example-modal-sm">