<table class="table">
    <tr>
		<td class="col-xs-3">
        	<h5><strong>Listing URL:</strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;This is the URL of the page.</span>
        </td>
        <td class="col-xs-9">
            <div class="col-xs-12">
                <a href="<?php echo $store['url']."index.php?route=module/productbundles/listing"; ?>" target="_blank"><?php echo $store['url']."index.php?route=module/productbundles/listing"; ?></a>
			</div>
       </td>
    </tr>
	<tr>
		<td class="col-xs-3">
        	<h5><strong>Add link to the listing in the main menu:</strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;It may require some additional work to make it work on a highly customized themes.</span>
        </td>
        <td class="col-xs-9">
            <div class="col-xs-4">
                <select id="LinkChecker" name="<?php echo $moduleName; ?>[MainLinkEnabled]" class="form-control">
                    <option value="yes" <?php echo (!empty($moduleData['MainLinkEnabled']) && $moduleData['MainLinkEnabled'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
                    <option value="no"  <?php echo (empty($moduleData['MainLinkEnabled']) || $moduleData['MainLinkEnabled']== 'no') ? 'selected=selected' : '' ?>>Disabled</option>
                </select>
			</div>
       </td>
    </tr>
    <tbody id="MainLinkOptions">
	<tr>
		<td class="col-xs-3">
        	<h5><strong>Menu Link Title:</strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;Specify the link title.</span>
        </td>
        <td class="col-xs-9">
            <div class="col-xs-4">
                <?php foreach ($languages as $language) { ?>
                    <div class="input-group">
                        <span class="input-group-addon"><?php echo $language['name']; ?>:</span>
                        <input type="text" class="form-control" name="<?php echo $moduleName; ?>[LinkTitle][<?php echo $language['language_id']; ?>]" value="<?php if(isset($moduleData['LinkTitle'][$language['language_id']])) { echo $moduleData['LinkTitle'][$language['language_id']]; } else { echo "Bundles"; }?>" />
                    </div>
                    <br />
                <?php } ?>
           </div>
       </td>
    </tr>
    <tr>
		<td class="col-xs-3">
        	<h5><strong>Menu Link Sort Order:</strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;Specify the sort order for the link.</span>
        </td>
        <td class="col-xs-9">
            <div class="col-xs-4">
				<input type="number" class="form-control" name="<?php echo $moduleName; ?>[LinkSortOrder]" value="<?php if(isset($moduleData['LinkSortOrder'])) { echo $moduleData['LinkSortOrder']; } else { echo "7"; }?>" />
           </div>
       </td>
    </tr>
    </tbody>
    <tr>
		<td class="col-xs-3">
        	<h5><strong>Bundles per page:</strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;Define how many bundles to be shown on first page.</span>
        </td>
        <td class="col-xs-9">
            <div class="col-xs-4">
				<input type="number" name="<?php echo $moduleName; ?>[ListingLimit]" class="form-control" value="<?php echo (isset($moduleData['ListingLimit'])) ? $moduleData['ListingLimit'] : '10' ?>" /> 
			</div>
       </td>
    </tr>
	<tr>
		<td class="col-xs-3" style="vertical-align:top;">
        	<h5><strong>SEO Options:</strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;Here you will find SEO options which (if used correctly) will boost your SEO rankings
            	<br />
                <br />
				<i class="fa fa-info-circle"></i>&nbsp;<strong>Important</strong>: The SEO title will also act as a heading title to the listing page.
            </span>
        </td>
        <td class="col-xs-9">
            <div class="col-xs-4">
                <ul class="nav nav-tabs" id="langtabs" role="tablist">
                  <?php foreach ($languages as $language) { ?>
                    <li><a href="#lang-<?php echo $language['language_id']; ?>" role="tab" data-toggle="tab"><img src="<?php echo $language['flag_url']; ?>" title="<?php echo $language['name']; ?>"/></a></li>
                  <?php } ?>
                </ul>
                <div class="tab-content">
                  <?php foreach ($languages as $language) { ?>
                  	<div class="tab-pane" id="lang-<?php echo $language['language_id']; ?>">
                   		Page Title:<br />
						<input name="<?php echo $moduleName; ?>[PageTitle][<?php echo $language['language_id']; ?>]" class="form-control" type="text" value="<?php echo (isset($moduleData['PageTitle'][$language['language_id']])) ? $moduleData['PageTitle'][$language['language_id']] : 'Product Bundles'; ?>" />
                        <br />
                        Meta Description:<br />
						<textarea name="<?php echo $moduleName; ?>[MetaDescription][<?php echo $language['language_id']; ?>]" class="form-control" rows="4"><?php echo (isset($moduleData['MetaDescription'][$language['language_id']])) ? $moduleData['MetaDescription'][$language['language_id']] : 'Bundles with great discount! Only in example.com.'; ?></textarea>
                        <br />
                        Meta Keywords:<br />
						<input name="<?php echo $moduleName; ?>[MetaKeywords][<?php echo $language['language_id']; ?>]" class="form-control" type="text" value="<?php echo (isset($moduleData['MetaKeywords'][$language['language_id']])) ? $moduleData['MetaKeywords'][$language['language_id']] : 'product bundles, discount, products, get discount'; ?>" />
                        <br/>
                        SEO Slug:<br />
                        <input name="<?php echo $moduleName; ?>[SeoURL][<?php echo $language['language_id']; ?>]" class="form-control" type="text" value="<?php echo (isset($moduleData['SeoURL'][$language['language_id']])) ? $moduleData['SeoURL'][$language['language_id']] : 'bundles'; ?>" />
                        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Type only the slug that you want to use for SEO URL. For example, you want the bundle listing to be accessible from <em><?php echo HTTP_CATALOG."<strong>bundles</strong>"; ?></em>, type only <strong>bundles</strong> in the field above.</span>
                    </div>
                  <?php } ?>
                </div>
			</div>
       </td>
    </tr>
    <tr>
		<td class="col-xs-3">
        	<h5><strong>Picture Width & Height:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;In Pixels</span>
        </td>
		<td class="col-xs-9">
        	<div class="col-xs-4">
                <div class="input-group">
                  <span class="input-group-addon">Width:&nbsp;</span>
					<input type="text" name="<?php echo $moduleName; ?>[ListingPictureWidth]" class="form-control" value="<?php echo (isset($moduleData['ListingPictureWidth'])) ? $moduleData['ListingPictureWidth'] : '120' ?>" />
				  <span class="input-group-addon">px</span>
                </div>
                <br />
                <div class="input-group">
                  <span class="input-group-addon">Height:</span>
					<input type="text" name="<?php echo $moduleName; ?>[ListingPictureHeight]" class="form-control" value="<?php echo (isset($moduleData['ListingPictureHeight'])) ? $moduleData['ListingPictureHeight'] : '120' ?>" />
                  <span class="input-group-addon">px</span>
                </div>
            </div>
		</td>
  </tr>
  <tr>
       <td class="col-xs-3" style="vertical-align:top;">
			<h5><strong>Custom CSS:</strong></h5>
       		<span class="help"><i class="fa fa-info-circle"></i>&nbsp;Place your custom CSS for the bundles listing here.</span> 
       </td>
       <td class="col-xs-9">
			<div class="col-xs-4">
				<textarea rows="5" name="<?php echo $moduleName; ?>[ListingCustomCSS]" placeholder="Custom CSS..." class="form-control"><?php echo (isset($moduleData['ListingCustomCSS'])) ? $moduleData['ListingCustomCSS'] : '' ?></textarea>
            </div>
       </td>
  </tr>
</table>